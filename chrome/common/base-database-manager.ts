/// <reference path='common.ts'/>

module Common {
    export class DatabaseIndexDescriptor {
        constructor(public indexName: string, public indexKeyPath: string, public indexProperties: any) {}
    }

    export abstract class BaseDatabaseManager {
        private logger: Logger;
        private database: IDBDatabase;
        private onReadyPendingCallbacks: Common.VoidCallback[];

        constructor(private dbName: string, private indices: DatabaseIndexDescriptor[]) {
            this.onReadyPendingCallbacks = [];
            this.initDatabase();
        }

        private onReady(callback: ()=> void) {
            if (!!this.database) {
                callback();
                return;
            }
            this.onReadyPendingCallbacks.push(callback);
        }

        private initDatabase() {
            var self = this,
                openRequest = indexedDB.open(this.dbName, 1);

            openRequest.onupgradeneeded = function(e: IDBVersionChangeEvent) {
                var db = (<any>(e.target)).result;

                if(!db.objectStoreNames.contains(self.dbName)) {
                    var objectStore = db.createObjectStore(
                        self.dbName,
                        {
                            keyPath: 'auto_id',
                            autoIncrement: true
                        }
                    );

                    self.indices.forEach(function(index){
                        objectStore.createIndex(index.indexName, index.indexKeyPath, index.indexProperties);
                    });
                }
            };

            openRequest.onsuccess = function(e) {
                self.database = (<any>(e.target)).result;
                while (self.onReadyPendingCallbacks.length) {
                    var onReadyCallback = self.onReadyPendingCallbacks.shift();
                    try {
                        onReadyCallback();
                    } catch (e) {
                        self.logger.error('error in calling db open callback', e);
                    }
                }

                self.database.onerror = function(e) {
                    self.logger.error(e);
                };
            };
        }

        private getDBTransaction(readWrite: boolean): IDBTransaction {
            var self = this,
                transaction: IDBTransaction =
                    this.database.transaction([this.dbName], 'readwrite');

            transaction.onerror = function (event) {
                self.logger.error('error in transaction', event);
            };

            return transaction;
        }

        protected getStore(): IDBObjectStore {
            var transaction: IDBTransaction = this.getDBTransaction(true);
            return transaction.objectStore(this.dbName);
        }

        protected addOrUpdateObject(indexName: string, objectToAdd: any,
                                    callback?: Common.BooleanCallback, indexFieldName?: string) {

            var previewDBManager = this;

            indexFieldName = indexFieldName === void 0 ? indexName : indexFieldName;

            this.onReady(function () {
                var store:IDBObjectStore = previewDBManager.getStore();
                var index:IDBIndex = store.index(indexName);
                var getRequest:IDBRequest = index.get(objectToAdd[indexFieldName]);

                getRequest.onsuccess = function (event) {
                    var savedRecordInDB:any = (<any>(event.target)).result;
                    var setRequest:IDBRequest;

                    if (!!savedRecordInDB) {
                        //screenShotRecord.updatePersistedRecordScreenShot(savedRecordInDB);
                        setRequest = store.put(savedRecordInDB);
                    } else {
                        //setRequest = store.put(screenShotRecord);
                    }

                    setRequest.onsuccess = function () {
                        if (!!callback) {
                            callback(true);
                        }
                    };
                    setRequest.onerror = function (event) {
                        previewDBManager.logger.error('error in addScreenShotURL request', setRequest.error, event);
                        if (callback) {
                            callback(false);
                        }
                    };
                };
                getRequest.onerror = function (event) {
                    previewDBManager.logger.error('error in getting any existing screen shot', getRequest.error, event);
                    if (callback) {
                        callback(false);
                    }
                };
            });
        }

    }
}