/// <reference path='../lib/ts/chrome.d.ts'/>
/// <reference path='../lib/ts/URIjs.d.ts'/>
'use strict';
var Common;
(function (Common) {
    Common.Constants = {
        EXTENSION_ID: 'obklodkfejkbibjdphelfpphlcbdgnik',
        APP_ID: 'efhimmplcdcbipghpfjiflahmechmlnm',
        NAVIGATOR_UI_CONNECTION_ID_TAB: 'NAVIGATOR_UI_CONNECTION_ID_TAB',
        NAVIGATOR_UI_CONNECTION_ID_APP: 'NAVIGATOR_UI_CONNECTION_ID_APP',
        NAVIGATOR_UI_APP_EMBED_URL_HASH: '5c84a345-2ea6-4916-9fee-fbb65d5f1c32'
    };
    Common.Commands = {
        EXTENSION_APP_PING: 'EXTENSION_APP_PING',
        NAVIGATE_FORWARD: 'NAVIGATE_FORWARD',
        NAVIGATE_BACKWARDS: 'NAVIGATE_BACKWARDS',
        HIDE_NAVIGATOR: 'HIDE_NAVIGATOR',
        TOGGLE_NAVIGATOR: 'TOGGLE_NAVIGATOR',
        GET_SEARCH_RESULTS: 'GET_SEARCH_RESULTS',
        GET_PREVIEW_RECORD: 'GET_PREVIEW_RECORD',
        GET_PAGE_PREVIEW: 'GET_PAGE_PREVIEW',
        GET_PAGE_SEARCHABLE_DATA: 'GET_PAGE_SEARCHABLE_DATA',
        NAVIGATE_TO_ITEM: 'NAVIGATE_TO_ITEM',
        CLOSE_TAB: 'CLOSE_TAB',
        OPEN_GOOGLE_SEARCH: 'OPEN_GOOGLE_SEARCH',
        IS_SHOWING_NAVIGATOR: 'IS_SHOWING_NAVIGATOR',
        IS_TAB_SUSPENDED: 'IS_TAB_SUSPENDED',
        CAN_SUSPEND_TAB: 'CAN_SUSPEND_TAB',
        CAN_TAKE_SCREEN_SHOT: 'CAN_TAKE_SCREEN_SHOT',
        HANDLE_TAB_SWITCH_OUT: 'HANDLE_TAB_SWITCH_OUT',
        HANDLE_TAB_SWITCH_IN: 'HANDLE_TAB_SWITCH_IN',
        SUSPEND_TAB: 'SUSPEND_TAB',
        UNSUSPEND_TAB: 'UNSUSPEND_TAB',
        LOAD_SUSPENDED_TAB_CONTENT: 'LOAD_SUSPENDED_TAB_CONTENT',
        GET_IMAGE_DATA_URL: 'GET_IMAGE_DATA_URL',
        lucenejs: {
            GET_OR_CREATE_INDEX: 'GET_OR_CREATE_INDEX',
            INDEX_DOCUMENT: 'INDEX_DOCUMENT',
            SEARCH: 'SEARCH',
            DELETE: 'DELETE',
            ADD_OR_UPDATE_DOCUMENTS: 'ADD_OR_UPDATE_DOCUMENTS',
            OPTIMIZE_INDEX: 'OPTIMIZE_INDEX',
            GET_INDEX_STATISTICS: 'GET_INDEX_STATISTICS'
        }
    };
    Common.KeyboardCommands = {
        NAVIGATE_FORWARD: 'navigate_forward',
        NAVIGATE_BACKWARDS: 'navigate_backwards'
    };
    Common.TabState = {
        LOADING: 'loading',
        COMPLETE: 'complete'
    };
    Common.DocumentState = {
        LOADING: 'loading',
        INTERACTIVE: 'interactive',
        COMPLETE: 'complete'
    };
    var PostMessage = (function () {
        function PostMessage(command, data, conversationId) {
            if (data === void 0) { data = null; }
            if (conversationId === void 0) { conversationId = null; }
            this.command = command;
            this.data = data;
            this.typeId = PostMessage.POST_MESSAGE_GUID;
            this.uniqueId = new Date().getTime();
            this.tab = null;
            if (conversationId === null) {
                this.conversationId = Util.generateUUID();
            }
        }
        PostMessage.POST_MESSAGE_GUID = '7907e372-0f4d-40e9-8e0c-4367bf499fa0';
        return PostMessage;
    }());
    Common.PostMessage = PostMessage;
    var SearchRequest = (function () {
        function SearchRequest(query, itemType, doFuzzyMatch) {
            this.query = query;
            this.itemType = itemType;
            this.doFuzzyMatch = doFuzzyMatch;
            this.timestamp = new Date().getTime();
            if (doFuzzyMatch === void 0) {
                this.doFuzzyMatch = false;
            }
        }
        return SearchRequest;
    }());
    Common.SearchRequest = SearchRequest;
    var PreviewRecordRequest = (function () {
        function PreviewRecordRequest(url) {
            this.url = url;
        }
        return PreviewRecordRequest;
    }());
    Common.PreviewRecordRequest = PreviewRecordRequest;
    (function (NavigatorItemType) {
        NavigatorItemType[NavigatorItemType["TAB"] = 0] = "TAB";
        NavigatorItemType[NavigatorItemType["BROWSING_HISTORY"] = 1] = "BROWSING_HISTORY";
        NavigatorItemType[NavigatorItemType["WEB_SEARCH"] = 2] = "WEB_SEARCH";
        NavigatorItemType[NavigatorItemType["ADVERTISEMENT"] = 3] = "ADVERTISEMENT";
    })(Common.NavigatorItemType || (Common.NavigatorItemType = {}));
    var NavigatorItemType = Common.NavigatorItemType;
    (function (TernaryBoolean) {
        TernaryBoolean[TernaryBoolean["UNKNOWN"] = 0] = "UNKNOWN";
        TernaryBoolean[TernaryBoolean["TRUE"] = 1] = "TRUE";
        TernaryBoolean[TernaryBoolean["FALSE"] = 2] = "FALSE";
    })(Common.TernaryBoolean || (Common.TernaryBoolean = {}));
    var TernaryBoolean = Common.TernaryBoolean;
    var KeyboardCommand = (function () {
        function KeyboardCommand(name, shortcut) {
            this.name = name;
            this.shortcut = shortcut;
        }
        return KeyboardCommand;
    }());
    Common.KeyboardCommand = KeyboardCommand;
    var UserPreferences = (function () {
        function UserPreferences() {
        }
        return UserPreferences;
    }());
    Common.UserPreferences = UserPreferences;
    var PreviewRecord = (function () {
        function PreviewRecord(url, screenShotURL, thumbnailURL, faviconURL) {
            this.url = url;
            this.screenShotURL = screenShotURL;
            this.thumbnailURL = thumbnailURL;
            this.faviconURL = faviconURL;
            this.encodedURL = PreviewRecord.getEncodedUrl(url);
            this.screenShotTimestamp = this.faviconTimestamp = new Date().getTime();
            this.domain = PreviewRecord.getDomainForUrl(url);
        }
        PreviewRecord.fromPersistedRecord = function (persistedRecord) {
            var previewRecord = new Common.PreviewRecord(persistedRecord.url, persistedRecord.screenShotURL, persistedRecord.thumbnailURL, persistedRecord.faviconURL);
            previewRecord.encodedURL = PreviewRecord.getEncodedUrl(persistedRecord.url);
            previewRecord.faviconTimestamp = persistedRecord.faviconTimestamp;
            previewRecord.screenShotTimestamp = persistedRecord.screenShotTimestamp;
            return previewRecord;
        };
        PreviewRecord.getEncodedUrl = function (url) {
            return encodeURIComponent(url);
        };
        PreviewRecord.getDecodedUrl = function (url) {
            return decodeURIComponent(url);
        };
        PreviewRecord.getDomainForUrl = function (url) {
            var parsedURI = new URI(url);
            var host = parsedURI.host();
            if (!host) {
                return '';
            }
            return host.toLowerCase();
        };
        PreviewRecord.isRecordForURL = function (record, url) {
            return url === PreviewRecord.getDecodedUrl(record.encodedURL);
        };
        PreviewRecord.prototype.updatePersistedRecordScreenShot = function (persistedRecord) {
            persistedRecord.screenShotURL = this.screenShotURL;
            persistedRecord.thumbnailURL = this.thumbnailURL;
            persistedRecord.screenShotTimestamp = this.screenShotTimestamp;
        };
        PreviewRecord.prototype.setScreenShotURL = function (screenShotURL, thumbnailURL) {
            this.screenShotURL = screenShotURL;
            this.thumbnailURL = thumbnailURL;
            this.screenShotTimestamp = new Date().getTime();
        };
        PreviewRecord.prototype.setFaviconURL = function (faviconURL) {
            this.faviconURL = faviconURL;
            this.faviconTimestamp = new Date().getTime();
        };
        PreviewRecord.prototype.update = function (sourceRecord) {
            this.screenShotURL = sourceRecord.screenShotURL;
            this.thumbnailURL = sourceRecord.thumbnailURL;
            this.screenShotTimestamp = sourceRecord.screenShotTimestamp;
            this.faviconURL = sourceRecord.faviconURL;
            this.faviconTimestamp = sourceRecord.faviconTimestamp;
        };
        PreviewRecord.prototype.makeShortLived = function () {
            this.screenShotTimestamp = 0;
        };
        return PreviewRecord;
    }());
    Common.PreviewRecord = PreviewRecord;
    var NavigatorItem = (function () {
        function NavigatorItem() {
            this.itemGuid = Common.Util.generateUUID();
        }
        return NavigatorItem;
    }());
    Common.NavigatorItem = NavigatorItem;
    var NavigationRequestData = (function () {
        function NavigationRequestData(navigatorItems, userPreferences) {
            this.navigatorItems = navigatorItems;
            this.userPreferences = userPreferences;
            this.id = new Date().getTime();
        }
        return NavigationRequestData;
    }());
    Common.NavigationRequestData = NavigationRequestData;
    var NavigateToItemRequestData = (function () {
        function NavigateToItemRequestData(navigatorItem, inNewTab) {
            if (inNewTab === void 0) { inNewTab = false; }
            this.navigatorItem = navigatorItem;
            this.inNewTab = inNewTab;
        }
        NavigateToItemRequestData.prototype.stringify = function () {
            return JSON.stringify({
                navigatorItem: this.navigatorItem,
                inNewTab: this.inNewTab
            });
        };
        return NavigateToItemRequestData;
    }());
    Common.NavigateToItemRequestData = NavigateToItemRequestData;
    var TabSearchableData = (function () {
        function TabSearchableData(url, title, description, text) {
            this.url = url;
            this.title = title;
            this.description = description;
            this.text = text;
        }
        return TabSearchableData;
    }());
    Common.TabSearchableData = TabSearchableData;
    var Size = (function () {
        function Size(width, height) {
            this.width = width;
            this.height = height;
        }
        return Size;
    }());
    Common.Size = Size;
    var Logger = (function () {
        function Logger(category) {
            this.category = category;
        }
        Logger.augmentArguments = function (args) {
            Array.prototype.unshift.call(args, new Date().toString());
        };
        Logger.prototype.info = function () {
            var any = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                any[_i - 0] = arguments[_i];
            }
            Logger.augmentArguments(arguments);
            console.info.apply(console, arguments);
        };
        Logger.prototype.debug = function () {
            var any = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                any[_i - 0] = arguments[_i];
            }
            Logger.augmentArguments(arguments);
            console.debug.apply(console, arguments);
        };
        Logger.prototype.log = function () {
            var any = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                any[_i - 0] = arguments[_i];
            }
            Logger.augmentArguments(arguments);
            console.log.apply(console, arguments);
        };
        Logger.prototype.warn = function () {
            var any = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                any[_i - 0] = arguments[_i];
            }
            Logger.augmentArguments(arguments);
            console.warn.apply(console, arguments);
        };
        Logger.prototype.error = function () {
            var any = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                any[_i - 0] = arguments[_i];
            }
            Logger.augmentArguments(arguments);
            console.error.apply(console, arguments);
        };
        return Logger;
    }());
    Common.Logger = Logger;
    var SpatioTemporalBoundedMapEntry = (function () {
        function SpatioTemporalBoundedMapEntry(value) {
            this.value = value;
            this.touch();
        }
        SpatioTemporalBoundedMapEntry.prototype.touch = function () {
            this.lastAccessTime = new Date().getTime();
        };
        return SpatioTemporalBoundedMapEntry;
    }());
    var SpatioTemporalBoundedMap = (function () {
        function SpatioTemporalBoundedMap(maxNumEntries, maxLifeInMillis, purgeCallback) {
            this.maxNumEntries = maxNumEntries;
            this.maxLifeInMillis = maxLifeInMillis;
            this.underlyingMap = {};
            this.purgeTimer = null;
            this.logger = new Common.Logger('spatio-temporal-bounded-map');
            this.purgeCallback = purgeCallback || null;
        }
        SpatioTemporalBoundedMap.prototype.destroy = function () {
            clearTimeout(this.purgeTimer);
            this.underlyingMap = null;
        };
        SpatioTemporalBoundedMap.prototype.has = function (key) {
            return !!this.underlyingMap.hasOwnProperty(key);
        };
        SpatioTemporalBoundedMap.prototype.get = function (key) {
            if (!this.underlyingMap.hasOwnProperty(key)) {
                return void 0;
            }
            var entry = this.underlyingMap[key];
            entry.touch();
            return entry.value;
        };
        SpatioTemporalBoundedMap.prototype.set = function (key, value) {
            var entry = this.underlyingMap[key];
            if (!entry) {
                entry = new SpatioTemporalBoundedMapEntry(value);
                this.underlyingMap[key] = entry;
            }
            else {
                entry.value = value;
                entry.touch();
            }
            this.purge();
        };
        SpatioTemporalBoundedMap.prototype.getNumEntries = function () {
            return Object.keys(this.underlyingMap).length;
        };
        SpatioTemporalBoundedMap.prototype.getKeysSortedByLastAccessTime = function () {
            var map = this.underlyingMap;
            var allKeys = Object.keys(this.underlyingMap);
            allKeys.sort(function (keyA, keyB) {
                var entryA = map[keyA];
                var entryB = map[keyB];
                return entryA.lastAccessTime - entryB.lastAccessTime;
            });
            return allKeys;
        };
        SpatioTemporalBoundedMap.prototype.dropEntry = function (key) {
            var entry = this.underlyingMap[key];
            delete this.underlyingMap[key];
            if (!!this.purgeCallback) {
                try {
                    this.purgeCallback(key, entry.value);
                }
                catch (e) {
                    this.logger.error('Error in purge callback, ignored', e);
                }
            }
        };
        SpatioTemporalBoundedMap.prototype.purge = function () {
            var now = new Date().getTime();
            var sortedKeys = this.getKeysSortedByLastAccessTime();
            var oldestAllowedEntryTime = now - this.maxLifeInMillis;
            var i = 0;
            for (; i < sortedKeys.length; ++i) {
                var key = sortedKeys[i];
                if (sortedKeys.length - i > this.maxNumEntries) {
                    this.dropEntry(key);
                    continue;
                }
                var entry = this.underlyingMap[key];
                if (entry.lastAccessTime < oldestAllowedEntryTime) {
                    this.dropEntry(key);
                    continue;
                }
                break;
            }
            if (this.purgeTimer) {
                clearTimeout(this.purgeTimer);
                this.purgeTimer = null;
            }
            // if we removed all entries no need to set a timer, wait until another
            // entry is added
            if (i >= sortedKeys.length) {
                return;
            }
            var oldestEntryKey = sortedKeys[i];
            var oldestEntry = this.underlyingMap[oldestEntryKey];
            var oldestEntryExpirationTime = oldestEntry.lastAccessTime + this.maxLifeInMillis;
            var timeUntilNextPurge = oldestEntryExpirationTime - now;
            if (timeUntilNextPurge < 0) {
                this.logger.error('timeUntilNextPurge may not be negative', timeUntilNextPurge, this.underlyingMap);
                return;
            }
            if (!isFinite(timeUntilNextPurge) || timeUntilNextPurge >= Number.MAX_VALUE) {
                return;
            }
            var boundedMap = this;
            this.purgeTimer = setTimeout(function () {
                boundedMap.purge();
            }, timeUntilNextPurge);
        };
        return SpatioTemporalBoundedMap;
    }());
    Common.SpatioTemporalBoundedMap = SpatioTemporalBoundedMap;
    var TelemetryMeasurement = (function () {
        function TelemetryMeasurement() {
            this.id = Util.generateUUID();
        }
        return TelemetryMeasurement;
    }());
    var Telemeter = (function () {
        function Telemeter() {
            this.logger = new Logger('telemeter');
            if (Telemeter.instance) {
                throw new Error('use Telemeter.getInstance()');
            }
            this.measurementIdToMeasurement = new SpatioTemporalBoundedMap(Telemeter.BUFFER_MAX_ITEM_COUNT, Telemeter.BUFFER_TTL);
        }
        Telemeter.getInstance = function () {
            if (!Telemeter.instance) {
                Telemeter.instance = new Telemeter();
            }
            return Telemeter.instance;
        };
        Telemeter.prototype.start = function (label, data) {
            var measurement = new TelemetryMeasurement();
            measurement.label = label;
            measurement.data = data;
            measurement.startTime = new Date().getTime();
            this.measurementIdToMeasurement.set(measurement.id, measurement);
            return measurement.id;
        };
        Telemeter.prototype.end = function (id) {
            var measurement = this.measurementIdToMeasurement.get(id);
            if (!measurement) {
                this.logger.warn('no running measurement found with id', id, ' probably purged already');
                return Number.NEGATIVE_INFINITY;
            }
            measurement.endTime = new Date().getTime();
            var timeTaken = measurement.endTime - measurement.startTime;
            this.logger.debug('[', new Date().toString(), ']', 'time taken', measurement.label, timeTaken, measurement.data);
            return timeTaken;
        };
        Telemeter.BUFFER_MAX_ITEM_COUNT = 500;
        Telemeter.BUFFER_TTL = Number.POSITIVE_INFINITY;
        Telemeter.instance = null;
        return Telemeter;
    }());
    Common.Telemeter = Telemeter;
    var Util = (function () {
        function Util() {
        }
        Util.getCurrentTab = function (callback) {
            var currentTabQueryParams = {
                active: true,
                currentWindow: true
            };
            chrome.tabs.query(currentTabQueryParams, function (tabs) {
                if (!tabs || !tabs.length) {
                    callback(null);
                    return;
                }
                callback(tabs[0]);
            });
        };
        Util.getTemporallyAutoCallback = function (callback, timeout) {
            var called = false;
            var timer = setTimeout(function () {
                called = true;
                callback();
            }, timeout);
            return function () {
                if (called) {
                    return;
                }
                clearTimeout(timer);
                callback();
            };
        };
        Util.generateUUID = function () {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
        };
        Util.debounce = function (func, minExecutionInterval, context) {
            if (!context) {
                context = {};
            }
            if (!context.hasOwnProperty('timer')) {
                context.timer = null;
            }
            return function () {
                if (context.timer) {
                    clearTimeout(context.timer);
                    context.timer = null;
                }
                var args = Array.prototype.slice.call(arguments), self = this;
                context.timer = setTimeout(function () {
                    func.apply(self, args);
                }, minExecutionInterval, false);
            };
        };
        Util.printf = function (template, args) {
            for (var key in args) {
                var pattern = "\\{" + key + "\\}";
                var re = new RegExp(pattern, "g");
                template = template.replace(re, args[key]);
            }
            return template;
        };
        Util.findInArray = function (arr, predicate) {
            if (!!Array.prototype['find']) {
                return arr.find(predicate);
            }
            for (var i = 0; i < arr.length; ++i) {
                if (predicate(arr[i])) {
                    return arr[i];
                }
            }
            return void 0;
        };
        Util.isMac = function () {
            return navigator.userAgent.indexOf('Macintosh') >= 0;
        };
        Util.getObjectValues = function (object) {
            return Object.keys(object).map(function (key) {
                return object[key];
            });
        };
        Util.removeDuplicates = function (arr, keyGetter) {
            var elementHash = {};
            var filtered = [];
            arr.forEach(function (element) {
                var elementKey = !!keyGetter ? keyGetter(element) : element.toString();
                if (elementHash.hasOwnProperty(elementKey)) {
                    return;
                }
                elementHash[elementKey] = true;
                filtered.push(element);
            });
            return filtered;
        };
        Util.setPeriodicAlarm = function (alarmName, periodInMinutes, alarmHandler) {
            chrome.alarms.create(alarmName, { periodInMinutes: periodInMinutes });
            chrome.alarms.onAlarm.addListener(function (alarm) {
                if (alarm.name === alarmName) {
                    alarmHandler(alarm);
                }
            });
        };
        Util.isGoogleSearchURL = function (url) {
            return /^https?:\/\/(?:www\.)google\.(.+?)/.test(url) || /^https?:\/\/(?:www\.)g\.cn/.test(url);
        };
        Util.executeInNextEventLoop = function (func) {
            setTimeout(func, 0);
        };
        Util.removeItemFromArray = function (arr, itemToRemove, removeAll) {
            if (removeAll === void 0) { removeAll = false; }
            var found = false;
            for (var i = 0; i < arr.length; ++i) {
                var item = arr[i];
                if (itemToRemove === item) {
                    found = true;
                    arr.splice(i, 1);
                    if (!removeAll) {
                        break;
                    }
                    i--;
                }
            }
            return found;
        };
        Util.sendMessageToCurrentTab = function (message, callback) {
            if (callback === void 0) { callback = null; }
            Util.getCurrentTab(function (tab) {
                if (!tab) {
                    Util.logger.warn('no current tab found');
                    return;
                }
                message.tab = tab;
                chrome.tabs.sendMessage(tab.id, message, callback);
            });
        };
        Util.onDocumentReady = function (document, callback) {
            if (document.readyState === Common.DocumentState.COMPLETE) {
                callback();
                return;
            }
            document.addEventListener('DOMContentLoaded', callback, false);
        };
        Util.getDevicePixelRatio = function () {
            if (isNaN(window.devicePixelRatio)) {
                return 1;
            }
            return window.devicePixelRatio;
        };
        Util.readFileAsDataURI = function (file, callback) {
            var reader = new FileReader();
            function onReadSuccess() {
                onDone();
                callback(null, reader.result);
            }
            function onReadFailure() {
                onDone();
                callback((reader.error));
            }
            function onDone() {
                reader.removeEventListener('load', onReadSuccess);
                reader.removeEventListener('error', onReadFailure);
            }
            reader.addEventListener('load', onReadSuccess);
            reader.addEventListener('error', onReadFailure);
            reader.readAsDataURL(file);
        };
        Util.logger = new Logger('util');
        return Util;
    }());
    Common.Util = Util;
})(Common || (Common = {}));
//# sourceMappingURL=common.js.map