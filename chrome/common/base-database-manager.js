/// <reference path='common.ts'/>
var Common;
(function (Common) {
    var DatabaseIndexDescriptor = (function () {
        function DatabaseIndexDescriptor(indexName, indexKeyPath, indexProperties) {
            this.indexName = indexName;
            this.indexKeyPath = indexKeyPath;
            this.indexProperties = indexProperties;
        }
        return DatabaseIndexDescriptor;
    }());
    Common.DatabaseIndexDescriptor = DatabaseIndexDescriptor;
    var BaseDatabaseManager = (function () {
        function BaseDatabaseManager(dbName, indices) {
            this.dbName = dbName;
            this.indices = indices;
            this.onReadyPendingCallbacks = [];
            this.initDatabase();
        }
        BaseDatabaseManager.prototype.onReady = function (callback) {
            if (!!this.database) {
                callback();
                return;
            }
            this.onReadyPendingCallbacks.push(callback);
        };
        BaseDatabaseManager.prototype.initDatabase = function () {
            var self = this, openRequest = indexedDB.open(this.dbName, 1);
            openRequest.onupgradeneeded = function (e) {
                var db = (e.target).result;
                if (!db.objectStoreNames.contains(self.dbName)) {
                    var objectStore = db.createObjectStore(self.dbName, {
                        keyPath: 'auto_id',
                        autoIncrement: true
                    });
                    self.indices.forEach(function (index) {
                        objectStore.createIndex(index.indexName, index.indexKeyPath, index.indexProperties);
                    });
                }
            };
            openRequest.onsuccess = function (e) {
                self.database = (e.target).result;
                while (self.onReadyPendingCallbacks.length) {
                    var onReadyCallback = self.onReadyPendingCallbacks.shift();
                    try {
                        onReadyCallback();
                    }
                    catch (e) {
                        self.logger.error('error in calling db open callback', e);
                    }
                }
                self.database.onerror = function (e) {
                    self.logger.error(e);
                };
            };
        };
        BaseDatabaseManager.prototype.getDBTransaction = function (readWrite) {
            var self = this, transaction = this.database.transaction([this.dbName], 'readwrite');
            transaction.onerror = function (event) {
                self.logger.error('error in transaction', event);
            };
            return transaction;
        };
        BaseDatabaseManager.prototype.getStore = function () {
            var transaction = this.getDBTransaction(true);
            return transaction.objectStore(this.dbName);
        };
        BaseDatabaseManager.prototype.addOrUpdateObject = function (indexName, objectToAdd, callback, indexFieldName) {
            var previewDBManager = this;
            indexFieldName = indexFieldName === void 0 ? indexName : indexFieldName;
            this.onReady(function () {
                var store = previewDBManager.getStore();
                var index = store.index(indexName);
                var getRequest = index.get(objectToAdd[indexFieldName]);
                getRequest.onsuccess = function (event) {
                    var savedRecordInDB = (event.target).result;
                    var setRequest;
                    if (!!savedRecordInDB) {
                        //screenShotRecord.updatePersistedRecordScreenShot(savedRecordInDB);
                        setRequest = store.put(savedRecordInDB);
                    }
                    else {
                    }
                    setRequest.onsuccess = function () {
                        if (!!callback) {
                            callback(true);
                        }
                    };
                    setRequest.onerror = function (event) {
                        previewDBManager.logger.error('error in addScreenShotURL request', setRequest.error, event);
                        if (callback) {
                            callback(false);
                        }
                    };
                };
                getRequest.onerror = function (event) {
                    previewDBManager.logger.error('error in getting any existing screen shot', getRequest.error, event);
                    if (callback) {
                        callback(false);
                    }
                };
            });
        };
        return BaseDatabaseManager;
    }());
    Common.BaseDatabaseManager = BaseDatabaseManager;
})(Common || (Common = {}));
//# sourceMappingURL=base-database-manager.js.map