/// <reference path='lib/ts/chrome.d.ts'/>
/// <reference path='lib/ts/filesystem.d.ts'/>
/// <reference path='common/common.ts'/>
/// <reference path='tab-suspension-service.ts'/>
var Preview;
(function (Preview) {
    var Constants = {
        Databases: {
            SCREEN_SHOTS: 'screen-shots'
        },
        Indices: {
            ScreenShotEncodedURL: {
                NAME: 'encodedURL',
                FIELD: 'encodedURL',
                UNIQUE: true
            },
            ScreenShotDomain: {
                NAME: 'domain',
                FIELD: 'domain',
                UNIQUE: false
            }
        },
        DatabaseVersions: {
            SCREEN_SHOTS: 9
        },
        SCREEN_SHOT_TTL: 60 * 60 * 1000,
        FAVICON_TTL: 7 * 86400 * 1000,
        MAX_SCREEN_SHOT_SCREEN_FRACTION: 0.2,
        SCREEN_SHOT_TIMER_MIN_INTERVAL: 2 * 1000,
        SCREEN_SHOT_TIMER_MAX_INTERVAL: 128 * 1000
    };
    var BlobUtil = (function () {
        function BlobUtil() {
        }
        BlobUtil.base64URLToBlob = function (base64URL) {
            // src: https://code.google.com/p/chromium/issues/detail?id=67587#c57
            var bin = atob(base64URL.split(',')[1]), len = bin.length, len32 = len >> 2, a8 = new Uint8Array(len), a32 = new Uint32Array(a8.buffer, 0, len32);
            for (var i = 0, j = 0; i < len32; i++) {
                a32[i] = bin.charCodeAt(j++) |
                    bin.charCodeAt(j++) << 8 |
                    bin.charCodeAt(j++) << 16 |
                    bin.charCodeAt(j++) << 24;
            }
            var tailLength = len & 3;
            while (tailLength--) {
                a8[j] = bin.charCodeAt(j++);
            }
            return new Blob([a8], { 'type': 'image/webp' });
        };
        BlobUtil.canvasToBlob = function (canvas) {
            return this.base64URLToBlob(canvas.toDataURL('image/jpeg'));
        };
        BlobUtil.urlToBlob = function (url, callback) {
            if (!url) {
                callback(null);
                return;
            }
            if (url.toLowerCase().indexOf('data:') === 0) {
                callback(this.base64URLToBlob(url));
                return;
            }
            var resizeRequest = new ImageResizeRequest(url, void 0, void 0, 1, callback);
            ImageResizeManager.getInstance().resizeImage(resizeRequest);
        };
        return BlobUtil;
    }());
    var FaviconService = (function () {
        function FaviconService() {
            this.logger = new Common.Logger('favicon-service');
        }
        FaviconService.getInstance = function () {
            if (!this.instance) {
                this.instance = new FaviconService();
            }
            return this.instance;
        };
        FaviconService.prototype.getFaviconUrl = function (url) {
            return 'chrome://favicon/' + url;
        };
        FaviconService.prototype.getFaviconBlob = function (url, callback) {
            var origin = new window.URL(url).origin;
            var faviconUrl = this.getFaviconUrl(origin);
            var faviconService = this;
            var xhr = new XMLHttpRequest();
            xhr.onload = function (event) {
                var blob = new Blob([xhr.response], { type: 'image/webp' });
                callback(blob);
            };
            xhr.onerror = function (error) {
                faviconService.logger.error('error in loading favicon data', error);
                callback(null);
            };
            xhr.responseType = 'arraybuffer';
            xhr.open('GET', faviconUrl, true);
            xhr.send();
        };
        return FaviconService;
    }());
    var ImageResizeRequest = (function () {
        function ImageResizeRequest(url, originalWidth, originalHeight, desiredScale, callback) {
            this.url = url;
            this.originalWidth = originalWidth;
            this.originalHeight = originalHeight;
            this.desiredScale = desiredScale;
            this.callback = callback;
        }
        return ImageResizeRequest;
    }());
    var ImageResizeManager = (function () {
        function ImageResizeManager() {
            this.logger = new Common.Logger('image-resize-manager');
            this.canvases = [
                document.createElement('canvas'),
                document.createElement('canvas')
            ];
            this.image = new Image();
            this.requestQueue = [];
        }
        ImageResizeManager.getInstance = function () {
            if (!this.instance) {
                this.instance = new ImageResizeManager();
            }
            return this.instance;
        };
        ImageResizeManager.prototype.onRequestProcessed = function (request, blob) {
            try {
                request.callback(blob);
            }
            catch (e) {
                this.logger.error(e);
            }
            this.processNextFromQueue();
        };
        ImageResizeManager.prototype.imageToBlob = function (img) {
            var canvas = this.canvases[0];
            canvas.width = img.originalWidth;
            canvas.height = img.originalHeight;
            var context = canvas.getContext('2d');
            context.drawImage(img, 0, 0, img.originalWidth, img.originalHeight);
            return BlobUtil.canvasToBlob(canvas);
        };
        ImageResizeManager.prototype.processNextFromQueue = function () {
            if (this.requestQueue.length === 0) {
                return;
            }
            var imgResizeManager = this, requestQueue = this.requestQueue, logger = this.logger;
            var currentRequest = requestQueue[0];
            this.image.onload = function () {
                requestQueue.shift();
                var numSteps = -1 * Math.log(currentRequest.desiredScale) / Math.log(2);
                if (numSteps < 0) {
                    numSteps = 0;
                }
                numSteps = Math.round(numSteps);
                var originalWidth = this.naturalWidth, originalHeight = this.naturalHeight, scale = 1.0, drawingCanvas = null;
                // no scale needed
                if (numSteps === 0) {
                    var blob = imgResizeManager.imageToBlob(this);
                    imgResizeManager.onRequestProcessed(currentRequest, blob);
                    return;
                }
                for (var i = 0; i < numSteps; ++i) {
                    // if there is no scaling to be done just draw
                    scale = 1 / (Math.pow(2, i + 1));
                    drawingCanvas = imgResizeManager.canvases[i % 2];
                    var drawingContext = drawingCanvas.getContext('2d');
                    var image = i === 0 ? this : imgResizeManager.canvases[(i + 1) % 2];
                    drawingCanvas.width = originalWidth * scale;
                    drawingCanvas.height = originalHeight * scale;
                    drawingContext.drawImage(image, 0, 0, drawingCanvas.width, drawingCanvas.height);
                }
                var blob = BlobUtil.canvasToBlob(drawingCanvas);
                logger.log('re-sized image from url size', currentRequest.url.length, 'to', blob.size);
                imgResizeManager.onRequestProcessed(currentRequest, blob);
            };
            this.image.onerror = function (error) {
                requestQueue.shift();
                imgResizeManager.logger.error('ImageResizeManager:: error in loading image', error);
                imgResizeManager.onRequestProcessed(currentRequest, null);
            };
            this.image.src = currentRequest.url;
        };
        ImageResizeManager.prototype.resizeImage = function (request) {
            this.requestQueue.push(request);
            if (this.requestQueue.length === 1) {
                this.processNextFromQueue();
            }
        };
        return ImageResizeManager;
    }());
    var FileSystemManager = (function () {
        function FileSystemManager() {
            this.logger = new Common.Logger('file-system-manager');
            this.fileSystem = null;
            this.directoryEntries = {};
        }
        FileSystemManager.getInstance = function () {
            if (!this.instance) {
                this.instance = new FileSystemManager();
            }
            return this.instance;
        };
        FileSystemManager.prototype.getFileSystem = function (callback) {
            if (this.fileSystem) {
                callback(this.fileSystem);
                return;
            }
            var fsManager = this;
            window.webkitRequestFileSystem(window.PERSISTENT, 1024 * 1024, function (fs) {
                fsManager.fileSystem = fs;
                callback(fs);
            });
        };
        FileSystemManager.prototype.getDirectoryEntry = function (dirName, callback) {
            var existingEntry = this.directoryEntries[dirName];
            if (false && !!existingEntry) {
                callback(existingEntry);
                return;
            }
            var fsManager = this;
            this.getFileSystem(function (fs) {
                fs.root.getDirectory(dirName, { create: true, exclusive: false }, function (dirEntry) {
                    fsManager.directoryEntries[dirName] = dirEntry;
                    callback(dirEntry);
                });
            });
        };
        FileSystemManager.prototype.persistFile = function (directoryPath, fileName, fileContent, callback) {
            var logger = this.logger;
            this.getDirectoryEntry(directoryPath, function (dirEntry) {
                dirEntry.getFile(fileName, { create: true, exclusive: false }, function (fileEntry) {
                    fileEntry.createWriter(function (fileWriter) {
                        fileWriter.onwriteend = function (e) {
                            callback(fileEntry.toURL());
                        };
                        fileWriter.onerror = function (e) {
                            logger.error('failed to write file to fileSystem', e.toString(), e);
                            callback(null);
                        };
                        fileWriter.write(fileContent);
                    });
                }, function (error) {
                    logger.error('failed to get fileEntry', error.toString(), error);
                    callback(null);
                });
            });
        };
        FileSystemManager.prototype.readFileAsDataURI = function (fileURL, callback) {
            window.webkitResolveLocalFileSystemURL(fileURL, function (fileEntry) {
                fileEntry.file(function (file) {
                    Common.Util.readFileAsDataURI(file, callback);
                }, callback);
            }, callback);
        };
        FileSystemManager.Directories = {
            THUMBNAILS: 'thumbnails',
            SCREEN_SHOTS: 'screen-shots',
            FAVICONS: 'favicons'
        };
        return FileSystemManager;
    }());
    var PreviewDatabaseManager = (function () {
        function PreviewDatabaseManager() {
            this.logger = new Common.Logger('preview-database-manager');
            this.onReadyPendingCallbacks = [];
            this.urlToScreenShotRecordCache = new Common.SpatioTemporalBoundedMap(PreviewDatabaseManager.URL_TO_SCREEN_SHOT_CACHE_MAX_NUM_ENTRIES, PreviewDatabaseManager.URL_TO_SCREEN_SHOT_CACHE_ENTRY_TTL);
            this.domainToScreenShotRecordCache = new Common.SpatioTemporalBoundedMap(PreviewDatabaseManager.URL_TO_SCREEN_SHOT_CACHE_MAX_NUM_ENTRIES, PreviewDatabaseManager.URL_TO_SCREEN_SHOT_CACHE_ENTRY_TTL);
            this.initPreviewDatabases();
        }
        PreviewDatabaseManager.getInstance = function () {
            if (!this.instance) {
                this.instance = new PreviewDatabaseManager();
            }
            return this.instance;
        };
        PreviewDatabaseManager.prototype.addRecordToCache = function (previewRecord) {
            this.urlToScreenShotRecordCache.set(previewRecord.url, previewRecord);
            var domain = Common.PreviewRecord.getDomainForUrl(previewRecord.url);
            if (domain) {
                this.domainToScreenShotRecordCache.set(domain, previewRecord);
            }
        };
        PreviewDatabaseManager.prototype.getRecordFromCache = function (url) {
            if (this.urlToScreenShotRecordCache.has(url)) {
                return this.urlToScreenShotRecordCache.get(url);
            }
            var domain = Common.PreviewRecord.getDomainForUrl(url);
            if (domain && this.domainToScreenShotRecordCache.has(domain)) {
                return this.domainToScreenShotRecordCache.get(domain);
            }
            return null;
        };
        PreviewDatabaseManager.prototype.onReady = function (callback) {
            if (!!this.database) {
                callback();
                return;
            }
            this.onReadyPendingCallbacks.push(callback);
        };
        PreviewDatabaseManager.prototype.initPreviewDatabases = function () {
            var self = this, openRequest = indexedDB.open(Constants.Databases.SCREEN_SHOTS, Constants.DatabaseVersions.SCREEN_SHOTS);
            openRequest.onupgradeneeded = function (e) {
                var target = (e.target);
                var db = target.result;
                var objectStore = null;
                if (!db.objectStoreNames.contains(Constants.Databases.SCREEN_SHOTS)) {
                    objectStore = db.createObjectStore(Constants.Databases.SCREEN_SHOTS, {
                        keyPath: "id",
                        autoIncrement: true
                    });
                }
                else {
                    objectStore = target.transaction.objectStore(Constants.Databases.SCREEN_SHOTS);
                }
                // make sure all indices exist
                var indexNames = Array.prototype.slice.call(objectStore.indexNames, 0);
                Object.keys(Constants.Indices).forEach(function (indexKey) {
                    var requiredIndexInfo = Constants.Indices[indexKey];
                    var requiredIndexName = requiredIndexInfo.NAME;
                    var requiredIndexField = requiredIndexInfo.FIELD;
                    var isRequiredIndexUnique = requiredIndexInfo.UNIQUE;
                    if (indexNames.indexOf(requiredIndexName) >= 0) {
                        return;
                    }
                    objectStore.createIndex(requiredIndexName, requiredIndexField, { unique: isRequiredIndexUnique });
                });
            };
            openRequest.onsuccess = function (e) {
                self.database = (e.target).result;
                while (self.onReadyPendingCallbacks.length) {
                    var onReadyCallback = self.onReadyPendingCallbacks.shift();
                    onReadyCallback();
                }
                self.database.onerror = function (e) {
                    self.logger.error(e);
                };
            };
        };
        PreviewDatabaseManager.prototype.getDBTransaction = function (readWrite) {
            var self = this, transaction = this.database.transaction([Constants.Databases.SCREEN_SHOTS], 'readwrite');
            transaction.onerror = function (event) {
                self.logger.error('error in transaction', event);
            };
            return transaction;
        };
        PreviewDatabaseManager.prototype.getScreenShotStore = function () {
            var transaction = this.getDBTransaction(true);
            return transaction.objectStore(Constants.Databases.SCREEN_SHOTS);
        };
        PreviewDatabaseManager.prototype.addScreenShotURL = function (url, screenShotRecord, callback) {
            if (!screenShotRecord.screenShotURL) {
                this.logger.warn('ignored empty screen shot url', screenShotRecord.screenShotURL, url);
                if (!!callback) {
                    callback(false);
                }
                return;
            }
            this.addRecordToCache(screenShotRecord);
            var previewDBManager = this;
            this.onReady(function () {
                var screenShotStore = previewDBManager.getScreenShotStore();
                var encodedUrlIndex = screenShotStore.index(Constants.Indices.ScreenShotEncodedURL.NAME);
                var encodedUrl = Common.PreviewRecord.getEncodedUrl(url);
                var getRequest = encodedUrlIndex.get(encodedUrl);
                getRequest.onsuccess = function (event) {
                    var savedRecordInDB = (event.target).result;
                    var setRequest;
                    if (!!savedRecordInDB) {
                        previewDBManager.logger.debug('found existing record for url, updating', url, savedRecordInDB);
                        screenShotRecord.updatePersistedRecordScreenShot(savedRecordInDB);
                        setRequest = screenShotStore.put(savedRecordInDB);
                    }
                    else {
                        setRequest = screenShotStore.put(screenShotRecord);
                    }
                    setRequest.onsuccess = function () {
                        if (!!callback) {
                            callback(true);
                        }
                    };
                    setRequest.onerror = function (event) {
                        previewDBManager.logger.error('error in addScreenShotURL request', setRequest.error, event, url);
                        if (callback) {
                            callback(false);
                        }
                    };
                };
                getRequest.onerror = function (event) {
                    previewDBManager.logger.error('error in getting any existing screen shot', getRequest.error, event);
                    if (callback) {
                        callback(false);
                    }
                };
            });
        };
        PreviewDatabaseManager.prototype.getScreenShotRecordUsingIndex = function (indexName, key, callback) {
            var previewDBManager = this;
            this.onReady(function () {
                var screenShotStore = previewDBManager.getScreenShotStore(), index = screenShotStore.index(indexName), request = index.get(key);
                request.onsuccess = function (event) {
                    var persistedObject = (event.target).result;
                    if (!persistedObject) {
                        callback(null);
                    }
                    else {
                        var screenShotRecord = Common.PreviewRecord.fromPersistedRecord(persistedObject);
                        previewDBManager.addRecordToCache(screenShotRecord);
                        callback(screenShotRecord);
                    }
                };
                request.onerror = function (error) {
                    previewDBManager.logger.error('error in getting screenshot for key in index', error, key, indexName);
                    callback(null);
                };
            });
        };
        PreviewDatabaseManager.prototype.getScreenShotRecord = function (url, callback, exactUrlMatchOnly) {
            if (exactUrlMatchOnly === void 0) { exactUrlMatchOnly = false; }
            var cachedRecord = this.getRecordFromCache(url);
            if (!!cachedRecord) {
                callback(cachedRecord);
                return;
            }
            var previewDBManager = this;
            this.getScreenShotRecordUsingIndex(Constants.Indices.ScreenShotEncodedURL.NAME, Common.PreviewRecord.getEncodedUrl(url), function (previewRecord) {
                if (!!previewRecord) {
                    callback(previewRecord);
                    return;
                }
                if (!!exactUrlMatchOnly) {
                    callback(null);
                    return;
                }
                var domain = Common.PreviewRecord.getDomainForUrl(url);
                if (!domain) {
                    callback(null);
                    return;
                }
                previewDBManager.getScreenShotRecordUsingIndex(Constants.Indices.ScreenShotDomain.NAME, domain, callback);
            });
        };
        PreviewDatabaseManager.URL_TO_SCREEN_SHOT_CACHE_MAX_NUM_ENTRIES = 30;
        PreviewDatabaseManager.URL_TO_SCREEN_SHOT_CACHE_ENTRY_TTL = 10 * 60 * 1000;
        return PreviewDatabaseManager;
    }());
    var PreviewManager = (function () {
        function PreviewManager() {
            this.logger = new Common.Logger('preview-manager');
            this.activeTabScreenShotTimer = null;
            this.fallbackPreviewCache = new Common.SpatioTemporalBoundedMap(PreviewManager.FALLBACK_CACHE_MAX_ENTRIES, PreviewManager.FALLBACK_CACHE_TTL);
            this.setUpTabListeners();
            this.populateInitialPreview();
        }
        PreviewManager.getInstance = function () {
            if (!this.instance) {
                this.instance = new PreviewManager();
            }
            return this.instance;
        };
        PreviewManager.prototype.getPreviewRecords = function (pageURLs, callback) {
            var index = 0, previewURLs = [], dbManager = PreviewDatabaseManager.getInstance(), previewManager = this;
            var getNextPreviewURL = function () {
                if (index >= pageURLs.length) {
                    callback(previewURLs);
                    return;
                }
                var nextURL = pageURLs[index++];
                dbManager.getScreenShotRecord(nextURL, function (record) {
                    previewURLs[index - 1] = record || previewManager.fallbackPreviewCache.get(nextURL) || null;
                    getNextPreviewURL();
                });
            };
            getNextPreviewURL();
        };
        PreviewManager.prototype.getDataURIForFileSystemURL = function (filesystemURL, callback) {
            FileSystemManager.getInstance().readFileAsDataURI(filesystemURL, callback);
        };
        PreviewManager.prototype.setUpTabListeners = function () {
            var self = this;
            chrome.tabs.onActivated.addListener(function (activeInfo) {
                self.restartActiveTabScreenShotTimer();
            });
            chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
                if (!tab.active) {
                    // can take screenshot of active tab only
                    return;
                }
                if (!changeInfo.url && !changeInfo.title) {
                    return;
                }
                self.restartActiveTabScreenShotTimer();
            });
        };
        PreviewManager.prototype.encodeURL = function (url) {
            return btoa(encodeURIComponent(url).replace(/%([0-9A-F]{2})/g, function (match, p1) {
                return String.fromCharCode(parseInt('0x' + p1));
            })).replace(/[/]/g, '_');
        };
        PreviewManager.prototype.saveScreenShotBlobToDisk = function (tab, fileBlob, isThumbnail, callback) {
            var fileName = this.encodeURL(tab.url);
            var fsManager = FileSystemManager.getInstance();
            var dirName = isThumbnail ? FileSystemManager.Directories.THUMBNAILS
                : FileSystemManager.Directories.SCREEN_SHOTS;
            fsManager.persistFile(dirName, fileName, fileBlob, callback);
        };
        PreviewManager.prototype.getTabPhysicalResolution = function (tab) {
            var devicePixelRatio = Common.Util.getDevicePixelRatio();
            var width = tab.width * devicePixelRatio;
            var height = tab.height * devicePixelRatio;
            return new Common.Size(width, height);
        };
        PreviewManager.prototype.getFullScreenShotScale = function () {
            // The size of the screenshot dimensions is twice the screen
            // dimensions on retina screens. We care less about that level
            // of quality of the screen shot than we care about the disk space
            // it will take. Hence we save the full screen shot at the
            // logical resolution instead of physical.
            var devicePixelRatio = Common.Util.getDevicePixelRatio();
            return 1 / devicePixelRatio;
        };
        PreviewManager.prototype.getThumbnailScale = function (tab) {
            var tabPhysicalResolution = this.getTabPhysicalResolution(tab);
            var maxWidth = Constants.MAX_SCREEN_SHOT_SCREEN_FRACTION * window.screen.availWidth;
            var maxHeight = Constants.MAX_SCREEN_SHOT_SCREEN_FRACTION * window.screen.availHeight;
            var scale = 1.0;
            if (tabPhysicalResolution.width >= tabPhysicalResolution.height) {
                var width = Math.min(maxWidth, tabPhysicalResolution.width);
                scale = width / tabPhysicalResolution.width;
            }
            else {
                var height = Math.min(maxHeight, tabPhysicalResolution.height);
                scale = height / tabPhysicalResolution.height;
            }
            scale = Math.min(1.0, scale);
            return scale;
        };
        PreviewManager.prototype.saveScreenShotAtScale = function (tab, dataURL, scale, isThumbnail, callback) {
            var tabPhysicalResolution = this.getTabPhysicalResolution(tab);
            var previewManager = this;
            var resizeRequest = new ImageResizeRequest(dataURL, tabPhysicalResolution.width, tabPhysicalResolution.height, scale, function (fullScreenShotBlob) {
                previewManager.saveScreenShotBlobToDisk(tab, fullScreenShotBlob, isThumbnail, callback);
            });
            ImageResizeManager.getInstance().resizeImage(resizeRequest);
        };
        PreviewManager.prototype.saveScreenShotDataURLToDisk = function (tab, dataURL, callback) {
            var previewManager = this;
            var fullScreenShotScale = this.getFullScreenShotScale();
            this.saveScreenShotAtScale(tab, dataURL, fullScreenShotScale, false, function (fullScreenShotFileURL) {
                var thumbnailScale = previewManager.getThumbnailScale(tab);
                previewManager.saveScreenShotAtScale(tab, dataURL, thumbnailScale, true, function (thumbnailScreenShotFileURL) {
                    if (callback) {
                        callback([fullScreenShotFileURL, thumbnailScreenShotFileURL]);
                    }
                });
            });
        };
        PreviewManager.prototype.saveFaviconToDisk = function (tab, callback) {
            var fileName = this.encodeURL(tab.url);
            FaviconService.getInstance().getFaviconBlob(tab.url, function (blob) {
                if (!blob) {
                    callback(null);
                    return;
                }
                var fsManager = FileSystemManager.getInstance();
                fsManager.persistFile(FileSystemManager.Directories.FAVICONS, fileName, blob, callback);
            });
        };
        PreviewManager.prototype.updateScreenShotInRecord = function (tab, record, callback) {
            var previewManager = this, dbManager = PreviewDatabaseManager.getInstance();
            var message = new Common.PostMessage(Common.Commands.CAN_TAKE_SCREEN_SHOT, null);
            chrome.tabs.sendMessage(tab.id, message, function (canTakeScreenShot) {
                if (!canTakeScreenShot) {
                    callback(false);
                    return;
                }
                // attempt to avoid "Failed to capture tab: unknown error" errors, not sure if it helps
                setTimeout(function () {
                    chrome.tabs.captureVisibleTab(null, { format: 'jpeg' }, function (dataURL) {
                        previewManager.logger.log('captured visible tab', dataURL && dataURL.length);
                        if (!dataURL) {
                            previewManager.logger.warn('empty dataURL from captureVisibleTab');
                            if (callback) {
                                callback(false);
                            }
                            return;
                        }
                        previewManager.saveScreenShotDataURLToDisk(tab, dataURL, function (persistedFileURLs) {
                            previewManager.logger.log('persisted processed screen shot to fileSystem', persistedFileURLs);
                            var screenShotURL = persistedFileURLs[0];
                            var thumbnailURL = persistedFileURLs[1];
                            record.setScreenShotURL(screenShotURL || null, thumbnailURL || null);
                            dbManager.addScreenShotURL(tab.url, record, callback);
                        });
                    });
                }, 64);
            });
        };
        PreviewManager.prototype.restartActiveTabScreenShotTimer = function () {
            if (this.activeTabScreenShotTimer !== null) {
                clearTimeout(this.activeTabScreenShotTimer);
                this.activeTabScreenShotTimer = null;
            }
            var previewManager = this;
            var interval = Constants.SCREEN_SHOT_TIMER_MIN_INTERVAL / 2;
            var onTimer = function () {
                previewManager.activeTabScreenShotTimer = null;
                previewManager.ensureScreenShotForActiveTab(function (didEnsurePersistedValidScreenShot) {
                    // if there was already a valid persisted screen shot or we could get one and persist
                    // it into the database we stop the loop for this tab. the loop will be re-triggered once
                    // the active tab changes.
                    if (didEnsurePersistedValidScreenShot) {
                        return;
                    }
                    // if another timer was started while we were busy getting the screen shot and persisting it
                    // we stop our own loop
                    if (!!previewManager.activeTabScreenShotTimer) {
                        return;
                    }
                    interval *= 2;
                    if (interval > Constants.SCREEN_SHOT_TIMER_MAX_INTERVAL) {
                        previewManager.logger.warn('Active tab screen shot time gave up after', interval / 2);
                        return;
                    }
                    previewManager.activeTabScreenShotTimer = setTimeout(onTimer, interval);
                });
            };
            // try once right away
            onTimer();
        };
        PreviewManager.prototype.ensureScreenShotForActiveTab = function (callback) {
            var previewManager = this;
            Common.Util.getCurrentTab(function (tab) {
                if (!tab || tab.status !== 'complete') {
                    callback(false);
                    return;
                }
                if (TabNavigator.TabSuspensionService.getInstance().isSuspendedTabURL(tab.url)) {
                    callback(false);
                    return;
                }
                var tabURL = tab.url, dbManager = PreviewDatabaseManager.getInstance();
                dbManager.getScreenShotRecord(tabURL, function (record) {
                    var now = new Date().getTime();
                    if (!!record && now - record.screenShotTimestamp <= Constants.SCREEN_SHOT_TTL) {
                        callback(true);
                        return;
                    }
                    if (!!record && !!record.faviconURL && now - record.faviconTimestamp <= Constants.FAVICON_TTL) {
                        previewManager.updateScreenShotInRecord(tab, record, callback);
                    }
                    else {
                        if (!record) {
                            record = new Common.PreviewRecord(tab.url, null, null, null);
                        }
                        previewManager.saveFaviconToDisk(tab, function (persistedFaviconURL) {
                            if (!!persistedFaviconURL) {
                                record.setFaviconURL(persistedFaviconURL);
                            }
                            previewManager.updateScreenShotInRecord(tab, record, callback);
                        });
                    }
                }, true);
            });
        };
        PreviewManager.prototype.populateInitialPreview = function (callback) {
            var self = this;
            chrome.tabs.query({}, function (tabs) {
                self.populateInitialPreviewsForTabs(tabs, callback);
            });
        };
        PreviewManager.prototype.populateInitialPreviewsForTabs = function (tabs, callback) {
            if (tabs.length === 0) {
                if (callback) {
                    callback();
                }
                return;
            }
            var previewManager = this, logger = this.logger, nextTab = tabs.shift(), tabURL = nextTab.url, dbManager = PreviewDatabaseManager.getInstance();
            dbManager.getScreenShotRecord(tabURL, function (record) {
                if (!!record) {
                    logger.log('found an existing persisted screenshot record for url', tabURL);
                    previewManager.populateInitialPreviewsForTabs(tabs, callback);
                    return;
                }
                logger.log('attempting to extract fallback preview to get a short term screen shot');
                var message = new Common.PostMessage(Common.Commands.GET_PAGE_PREVIEW, null);
                chrome.tabs.sendMessage(nextTab.id, message, function (previewURL) {
                    logger.log('short term preview URL length', previewURL && previewURL.length);
                    if (!previewURL) {
                        previewManager.populateInitialPreviewsForTabs(tabs, callback);
                        return;
                    }
                    BlobUtil.urlToBlob(previewURL, function (blob) {
                        if (!blob) {
                            previewManager.populateInitialPreviewsForTabs(tabs, callback);
                            return;
                        }
                        previewManager.saveScreenShotBlobToDisk(nextTab, blob, false, function (persistedScreenShotURL) {
                            previewManager.saveFaviconToDisk(nextTab, function (persistedFaviconURL) {
                                if (!persistedFaviconURL) {
                                    persistedFaviconURL = '';
                                }
                                var screenShotRecord = new Common.PreviewRecord(tabURL, persistedScreenShotURL, 
                                // TODO (shashank): resize this
                                persistedScreenShotURL, persistedFaviconURL);
                                screenShotRecord.makeShortLived();
                                previewManager.fallbackPreviewCache.set(tabURL, screenShotRecord);
                                previewManager.populateInitialPreviewsForTabs(tabs, callback);
                                logger.log('short term screen shot cached', screenShotRecord);
                            });
                        });
                    });
                });
            });
        };
        PreviewManager.FALLBACK_CACHE_MAX_ENTRIES = 30;
        PreviewManager.FALLBACK_CACHE_TTL = 10 * 60 * 1000;
        return PreviewManager;
    }());
    Preview.PreviewManager = PreviewManager;
})(Preview || (Preview = {}));
//# sourceMappingURL=preview.js.map