/// <reference path='../../lib/ts/chrome.d.ts'/>
/// <reference path='../../common/common.ts'/>
/// <reference path='../../tab-suspension-service.ts'/>

'use strict';

module TabNavigator {
    import PreviewRecord = Common.PreviewRecord;
    export class SuspendedTabUI {
        private logger = new Common.Logger('suspended-tab-ui');

        constructor() {
            var suspendedTabUI = this;

            var tabSuspensionService: TabSuspensionService
                = suspendedTabUI.getTabSuspensionService();

            // the tab was just suspended, load suspended state
            var shouldLoadSource = tabSuspensionService.shouldLoadSourceTabOnSuspendedTabLoad(
                window.location.href,
                !document.hidden
            );

            if (shouldLoadSource) {
                suspendedTabUI.unSuspend();
            } else {
                // remove the freshly-suspended marker from the URL
                window.location.hash = '';
                this.setUpListeners();
                suspendedTabUI.loadContent();
            }
        }

        private setUpListeners(): void {
            var suspendedTabUI = this;

            window.addEventListener('error', function(evt: ErrorEvent){
                // only catch events in extension context
                if (evt.filename && evt.filename.indexOf(chrome.runtime.id) < 0) {
                    return;
                }
                suspendedTabUI.logger.error('Uncaught exception in suspended tab UI', evt);
            });

            Common.Util.onDocumentReady(document, function(){
                var reloadButton = document.querySelector('.reload-button');

                var source: SuspendedTabSource = suspendedTabUI.getSource();
                var url = source.url || '';
                reloadButton.setAttribute('title', url);

                reloadButton.addEventListener('click', function(){
                    suspendedTabUI.unSuspend();
                });
            });
        }

        private getTabSuspensionService(): TabSuspensionService {
            var backgroundPage = chrome.extension.getBackgroundPage();
            return (<any>backgroundPage).TabNavigator.TabSuspensionService.getInstance();
        }

        private getSource(): SuspendedTabSource {
            var suspensionService: TabSuspensionService
                = this.getTabSuspensionService();
            return suspensionService.getSourceForSuspendedTab(window.location.href);
        }

        private setSourceLink(url: string) {
            var sourceLinkContainer = <HTMLElement>document.querySelector('.source-link-container');
            if (!url) {
                sourceLinkContainer.setAttribute('hidden', 'true');
                return;
            }

            sourceLinkContainer.setAttribute('hidden', 'false');
            sourceLinkContainer.setAttribute('title', url);
            var sourceLink = <HTMLAnchorElement>sourceLinkContainer.querySelector('.source-link');
            sourceLink.setAttribute('href', url);
            sourceLink.innerText = url;
        }

        private loadContent() {
            var suspendedTabUI  = this;

            var source: SuspendedTabSource = suspendedTabUI.getSource();
            this.loadScreenShot(source.url, function(screenShotURL: string){
                var sourceLinkContainer = <HTMLElement>document.querySelector('.source-tab-link-container');

                if (!screenShotURL) {
                    suspendedTabUI.logger.debug(
                        'No screen-shot found for suspended source tab',
                        source
                    );
                    suspendedTabUI.setSourceLink(source.url);
                    return;
                }

                suspendedTabUI.setSourceLink(null);
                suspendedTabUI.setScreenShot(screenShotURL);
            });

            document.title = source.title + ' [Suspended]';
            var faviconNode = document.querySelector('.favicon');
            faviconNode.setAttribute('href', source.faviconURL);
        }

        private loadScreenShot(sourceURL: string, callback: Common.StringCallback) {

            var previewRecordRequest = new Common.PreviewRecordRequest(sourceURL);
            var message = new Common.PostMessage(
                Common.Commands.GET_PREVIEW_RECORD,
                JSON.stringify(previewRecordRequest)
            );

            (<any>window).chrome.runtime.sendMessage(message, function(previewRecord: Common.PreviewRecord){
                if (!previewRecord) {
                    callback(null);
                    return;
                }

                // use screen-shots for exact url match only (and not
                // fallback domain match e.g.);
                if (!PreviewRecord.isRecordForURL(previewRecord, sourceURL)) {
                    callback(null);
                    return;
                }

                callback(previewRecord.screenShotURL);
            });
        }

        private setScreenShot(screenShotURL: string) {
            var backgroundImage = <HTMLElement>document.querySelector('.content .background-image');
            backgroundImage.setAttribute('src', screenShotURL);
        }

        private unSuspend() {
            var source: SuspendedTabSource = this.getSource();
            if (!source.url) {
                this.logger.error('Source tab url not found when trying to un-suspend tab', source);
                return;
            }
            window.location.href = source.url;
        }
    }
}

var suspendedTabUI = new TabNavigator.SuspendedTabUI();