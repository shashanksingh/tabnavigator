/// <reference path='../../lib/ts/chrome.d.ts'/>
/// <reference path='../../common/common.ts'/>
/// <reference path='../../tab-suspension-service.ts'/>
'use strict';
var TabNavigator;
(function (TabNavigator) {
    var PreviewRecord = Common.PreviewRecord;
    var SuspendedTabUI = (function () {
        function SuspendedTabUI() {
            this.logger = new Common.Logger('suspended-tab-ui');
            var suspendedTabUI = this;
            var tabSuspensionService = suspendedTabUI.getTabSuspensionService();
            // the tab was just suspended, load suspended state
            var shouldLoadSource = tabSuspensionService.shouldLoadSourceTabOnSuspendedTabLoad(window.location.href, !document.hidden);
            if (shouldLoadSource) {
                suspendedTabUI.unSuspend();
            }
            else {
                // remove the freshly-suspended marker from the URL
                window.location.hash = '';
                this.setUpListeners();
                suspendedTabUI.loadContent();
            }
        }
        SuspendedTabUI.prototype.setUpListeners = function () {
            var suspendedTabUI = this;
            window.addEventListener('error', function (evt) {
                // only catch events in extension context
                if (evt.filename && evt.filename.indexOf(chrome.runtime.id) < 0) {
                    return;
                }
                suspendedTabUI.logger.error('Uncaught exception in suspended tab UI', evt);
            });
            Common.Util.onDocumentReady(document, function () {
                var reloadButton = document.querySelector('.reload-button');
                var source = suspendedTabUI.getSource();
                var url = source.url || '';
                reloadButton.setAttribute('title', url);
                reloadButton.addEventListener('click', function () {
                    suspendedTabUI.unSuspend();
                });
            });
        };
        SuspendedTabUI.prototype.getTabSuspensionService = function () {
            var backgroundPage = chrome.extension.getBackgroundPage();
            return backgroundPage.TabNavigator.TabSuspensionService.getInstance();
        };
        SuspendedTabUI.prototype.getSource = function () {
            var suspensionService = this.getTabSuspensionService();
            return suspensionService.getSourceForSuspendedTab(window.location.href);
        };
        SuspendedTabUI.prototype.setSourceLink = function (url) {
            var sourceLinkContainer = document.querySelector('.source-link-container');
            if (!url) {
                sourceLinkContainer.setAttribute('hidden', 'true');
                return;
            }
            sourceLinkContainer.setAttribute('hidden', 'false');
            sourceLinkContainer.setAttribute('title', url);
            var sourceLink = sourceLinkContainer.querySelector('.source-link');
            sourceLink.setAttribute('href', url);
            sourceLink.innerText = url;
        };
        SuspendedTabUI.prototype.loadContent = function () {
            var suspendedTabUI = this;
            var source = suspendedTabUI.getSource();
            this.loadScreenShot(source.url, function (screenShotURL) {
                var sourceLinkContainer = document.querySelector('.source-tab-link-container');
                if (!screenShotURL) {
                    suspendedTabUI.logger.debug('No screen-shot found for suspended source tab', source);
                    suspendedTabUI.setSourceLink(source.url);
                    return;
                }
                suspendedTabUI.setSourceLink(null);
                suspendedTabUI.setScreenShot(screenShotURL);
            });
            document.title = source.title + ' [Suspended]';
            var faviconNode = document.querySelector('.favicon');
            faviconNode.setAttribute('href', source.faviconURL);
        };
        SuspendedTabUI.prototype.loadScreenShot = function (sourceURL, callback) {
            var previewRecordRequest = new Common.PreviewRecordRequest(sourceURL);
            var message = new Common.PostMessage(Common.Commands.GET_PREVIEW_RECORD, JSON.stringify(previewRecordRequest));
            window.chrome.runtime.sendMessage(message, function (previewRecord) {
                if (!previewRecord) {
                    callback(null);
                    return;
                }
                // use screen-shots for exact url match only (and not
                // fallback domain match e.g.);
                if (!PreviewRecord.isRecordForURL(previewRecord, sourceURL)) {
                    callback(null);
                    return;
                }
                callback(previewRecord.screenShotURL);
            });
        };
        SuspendedTabUI.prototype.setScreenShot = function (screenShotURL) {
            var backgroundImage = document.querySelector('.content .background-image');
            backgroundImage.setAttribute('src', screenShotURL);
        };
        SuspendedTabUI.prototype.unSuspend = function () {
            var source = this.getSource();
            if (!source.url) {
                this.logger.error('Source tab url not found when trying to un-suspend tab', source);
                return;
            }
            window.location.href = source.url;
        };
        return SuspendedTabUI;
    })();
    TabNavigator.SuspendedTabUI = SuspendedTabUI;
})(TabNavigator || (TabNavigator = {}));
var suspendedTabUI = new TabNavigator.SuspendedTabUI();
//# sourceMappingURL=suspended.js.map