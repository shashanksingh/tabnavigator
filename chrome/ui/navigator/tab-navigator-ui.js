/// <reference path='../../lib/ts/jquery.d.ts'/>
/// <reference path='../../lib/ts/angular.d.ts'/>
/// <reference path='../../common/common.ts'/>
'use strict';
var TabNavigator;
(function (TabNavigator) {
    var Constants = {
        MAX_SCREEN_SHOT_SCREEN_FRACTION: 0.15,
        MAX_FONT_SIZE: 15,
        WIDTH_DIFFERENCE_TOLERANCE_FACTOR: 1.25,
        MAX_WIDTH_FRACTION: 0.8,
        MIN_ITEM_WIDTH: 250,
        MAX_ITEM_WIDTH: 500,
        ITEM_GAP: 20,
        HIGHLIGHT_SCALE_FACTOR_DEFAULT: 1.1,
        HIGHLIGHT_SCALE_FACTOR_LARGE: 2.75,
        HIGHLIGHT_SCALE_LARGE_DELAY: 1000,
        MAX_SCALED_ITEM_SIZE_WINDOW_FRACTION: 0.75,
        QUERY_CHANGE_REACTION_DEBOUNCE_INTERVAL: 512
    };
    var CommandProperties = (function () {
        function CommandProperties() {
            this.keyCode = 0;
            this.isCommand = false;
            this.isCtrl = false;
            this.isShift = false;
            this.isAlt = false;
        }
        CommandProperties.prototype.isEquivalentToEvent = function ($event, disregardKeyCode) {
            if (disregardKeyCode === void 0) { disregardKeyCode = false; }
            if (this.isCommand && !$event.metaKey) {
                return false;
            }
            if (this.isCtrl && !$event.ctrlKey) {
                return false;
            }
            if (this.isAlt && !$event.altKey) {
                return false;
            }
            if (this.isShift && !$event.shiftKey) {
                return false;
            }
            if (!disregardKeyCode && $event.keyCode !== this.keyCode) {
                return false;
            }
            return true;
        };
        return CommandProperties;
    }());
    var NavigatorItemPreviewLoader = (function () {
        function NavigatorItemPreviewLoader() {
            this.previewLoadTimer = null;
            this.logger = new Common.Logger('navigator-item-preview-loader');
            this.urlPreviewRecordCache = new Common.SpatioTemporalBoundedMap(NavigatorItemPreviewLoader.PREVIEW_RECORD_CACHE_MAX_ENTRIES, NavigatorItemPreviewLoader.PREVIEW_RECORD_CACHE_MAX_LIFE);
        }
        NavigatorItemPreviewLoader.getInstance = function () {
            if (!NavigatorItemPreviewLoader.instance) {
                NavigatorItemPreviewLoader.instance = new NavigatorItemPreviewLoader();
            }
            return NavigatorItemPreviewLoader.instance;
        };
        NavigatorItemPreviewLoader.prototype.trigger = function (navigatorItems, onPreviewAvailable) {
            if (this.previewLoadTimer) {
                clearTimeout(this.previewLoadTimer);
            }
            var previewLoader = this, currentIndex = 0, loadNextPreview, previewCallback, scheduleNextPreviewLoad;
            previewCallback = function (navigatorItem, record) {
                try {
                    onPreviewAvailable(navigatorItem, record);
                }
                catch (e) {
                    previewLoader.logger.error('Error in onPreviewAvailable', e);
                }
            };
            scheduleNextPreviewLoad = function () {
                currentIndex++;
                previewLoader.previewLoadTimer = setTimeout(loadNextPreview, NavigatorItemPreviewLoader.PREVIEW_LOAD_GAP_MILLIS);
            };
            loadNextPreview = function () {
                if (currentIndex >= navigatorItems.length) {
                    return;
                }
                var navigatorItem = navigatorItems[currentIndex];
                if (!navigatorItem.url) {
                    previewCallback(navigatorItem, null);
                    scheduleNextPreviewLoad();
                    return;
                }
                var cachedRecord = previewLoader.urlPreviewRecordCache.get(navigatorItem.url);
                if (cachedRecord) {
                    previewCallback(navigatorItem, cachedRecord);
                    scheduleNextPreviewLoad();
                }
                else {
                    var previewRecordRequest = new Common.PreviewRecordRequest(navigatorItem.url);
                    var message = new Common.PostMessage(Common.Commands.GET_PREVIEW_RECORD, JSON.stringify(previewRecordRequest));
                    window.chrome.runtime.sendMessage(message, function (previewRecord) {
                        previewLoader.urlPreviewRecordCache.set(navigatorItem.url, previewRecord);
                        previewCallback(navigatorItem, previewRecord);
                        scheduleNextPreviewLoad();
                    });
                }
            };
            loadNextPreview();
        };
        NavigatorItemPreviewLoader.PREVIEW_RECORD_CACHE_MAX_ENTRIES = 100;
        NavigatorItemPreviewLoader.PREVIEW_RECORD_CACHE_MAX_LIFE = 10 * 60 * 1000;
        NavigatorItemPreviewLoader.PREVIEW_LOAD_GAP_MILLIS = 16;
        NavigatorItemPreviewLoader.instance = null;
        return NavigatorItemPreviewLoader;
    }());
    var TabNavigatorUI = (function () {
        function TabNavigatorUI() {
            this.logger = new Common.Logger('tab-navigator-ui');
            this.app = angular.module("tabNavigator", ['ngAnimate']);
            this.configureAngularApp();
            this.defineNavigatorDirective();
            this.defineNavigatorItemDirective();
            this.defineFaviconDirective();
            this.definePreviewImageDirective();
            var tabNavigatorUI = this;
            window.addEventListener('error', function (evt) {
                // only catch events in extension context
                if (evt.filename && evt.filename.indexOf(chrome.runtime.id) < 0) {
                    return;
                }
                tabNavigatorUI.logger.error('Uncaught exception in navigator UI', evt);
            });
        }
        TabNavigatorUI.prototype.isEmbeddedInCompanionApp = function () {
            return window.location.hash.indexOf(Common.Constants.NAVIGATOR_UI_APP_EMBED_URL_HASH) >= 0;
        };
        TabNavigatorUI.prototype.getBackgroundConnectionPortName = function () {
            var insideApp = this.isEmbeddedInCompanionApp();
            return insideApp
                ? Common.Constants.NAVIGATOR_UI_CONNECTION_ID_APP
                : Common.Constants.NAVIGATOR_UI_CONNECTION_ID_TAB;
        };
        TabNavigatorUI.prototype.configureAngularApp = function () {
            // src: http://stackoverflow.com/a/15769779
            this.app.config([
                '$compileProvider',
                '$rootScopeProvider',
                function ($compileProvider, $rootScopeProvider) {
                    $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|chrome|filesystem[:]chrome-extension):/);
                    $rootScopeProvider.digestTtl(30);
                }
            ]);
            this.app.run(['$rootScope', function ($rootScope) {
                }]);
        };
        TabNavigatorUI.safeApply = function ($scope) {
            if ($scope === void 0) { $scope = null; }
            if (!$scope) {
                $scope = angular.element(document.body).scope().$root;
            }
            var phase = $scope.$root.$$phase;
            if (phase != '$apply' && phase != '$digest') {
                $scope.$apply();
            }
        };
        TabNavigatorUI.registerOneShotEventListener = function ($scope, evt, callback) {
            var deRegisterer;
            deRegisterer = $scope.$on(evt, function () {
                deRegisterer();
                callback.apply(this, arguments);
            });
        };
        TabNavigatorUI.isDescendantOf = function (element, ancestorSelector) {
            while (element) {
                if (element.matches(ancestorSelector)) {
                    return true;
                }
                element = element.parentElement;
            }
            return false;
        };
        TabNavigatorUI.prototype.getCommandShortcutsMap = function (commands) {
            var map = {};
            var tabNavigatorUI = this;
            commands.forEach(function (command) {
                var shortcut = command.shortcut, parts = shortcut.split('+'), commandProperties = new CommandProperties();
                parts.forEach(function (part) {
                    switch (part) {
                        case 'Ctrl':
                            commandProperties.isCtrl = true;
                            break;
                        case 'Command':
                            commandProperties.isCommand = true;
                            break;
                        case 'Shift':
                            commandProperties.isShift = true;
                            break;
                        case 'Alt':
                            commandProperties.isAlt = true;
                            break;
                        default:
                            if (part.length !== 1) {
                                tabNavigatorUI.logger.error('unhandled keyboard command shortcut component', part, shortcut, command);
                                return;
                            }
                            var keyCode = part.charCodeAt(0);
                            commandProperties.keyCode = keyCode;
                            break;
                    }
                });
                map[command.name] = commandProperties;
            });
            return map;
        };
        TabNavigatorUI.getElementScale = function (elem) {
            var transform = /matrix\([^\)]+\)/.exec(window.getComputedStyle(elem)['transform']), scale = { 'x': 1, 'y': 1 };
            if (transform) {
                var parts = transform[0].replace('matrix(', '').replace(')', '').split(', ');
                scale.x = parseFloat(parts[0]);
                scale.y = parseFloat(parts[3]);
            }
            return scale;
        };
        TabNavigatorUI.parsePixelCssProp = function (value) {
            if (value === 'auto') {
                return 0;
            }
            var num = parseFloat(value.replace(/px$/, ''));
            if (isNaN(num)) {
                return 0;
            }
            return num;
        };
        TabNavigatorUI.prototype.hideNavigator = function () {
            var message = new Common.PostMessage(Common.Commands.HIDE_NAVIGATOR, null);
            window.parent.postMessage(message, '*');
        };
        TabNavigatorUI.prototype.getImageDataURI = function (imageURL, callback) {
            if (!imageURL) {
                callback(null);
                return;
            }
            var request = {
                fileSystemURL: imageURL
            };
            var message = new Common.PostMessage(Common.Commands.GET_IMAGE_DATA_URL, JSON.stringify(request));
            var navigatorUI = this;
            chrome.runtime.sendMessage(message, function (error, dataURI) {
                if (!!error) {
                    navigatorUI.logger.error('Error in getting data URI for filesystem url', error, imageURL);
                    callback(null);
                    return;
                }
                callback(dataURI);
            });
        };
        TabNavigatorUI.prototype.getImageDataURIs = function (imageURLs, callback) {
            var doneCount = 0;
            var dataURIs = [];
            var navigatorUI = this;
            var numImages = imageURLs.length;
            imageURLs.forEach(function (imageURL, index) {
                navigatorUI.getImageDataURI(imageURL, function (dataURI) {
                    dataURIs[index] = dataURI;
                    doneCount++;
                    if (doneCount === imageURLs.length) {
                        callback(dataURIs);
                    }
                });
            });
        };
        TabNavigatorUI.prototype.defineNavigatorDirective = function () {
            var navigatorUI = this;
            this.app.directive('navigator', ['$timeout', function ($timeout) {
                    var itemLayouts = [];
                    var itemsPerRow = -1;
                    var numRows = -1;
                    var gridStyle = {};
                    var highlightedItemStyle = {};
                    function linker(scope, $el, attrs) {
                        function doLayout() {
                            var items = scope.navigatorItems;
                            if (!items) {
                                return;
                            }
                            var numItems = items.length;
                            var aspectRatio = screen.width / screen.height;
                            var availableWidth = $el[0].getBoundingClientRect().width * Constants.MAX_WIDTH_FRACTION;
                            var itemWidth = ((availableWidth - Constants.ITEM_GAP) / numItems) - Constants.ITEM_GAP;
                            if (itemWidth > Constants.MAX_ITEM_WIDTH) {
                                itemWidth = Constants.MAX_ITEM_WIDTH;
                            }
                            else if (itemWidth < Constants.MIN_ITEM_WIDTH) {
                                var maxInOneRow = (availableWidth - Constants.ITEM_GAP)
                                    / (Constants.MIN_ITEM_WIDTH + Constants.ITEM_GAP);
                                if (maxInOneRow < 1) {
                                    navigatorUI.logger.error('not enough width to show even one item', availableWidth);
                                    return;
                                }
                                itemWidth =
                                    ((availableWidth - Constants.ITEM_GAP) / Math.floor(maxInOneRow)) - Constants.ITEM_GAP;
                            }
                            // use the same aspect ratios as the screen.
                            // the screen shots will almost always be in
                            // the same aspect ratio.
                            var itemHeight = itemWidth / aspectRatio;
                            var currentTop = Constants.ITEM_GAP;
                            var currentLeft = Constants.ITEM_GAP;
                            numRows = 1;
                            itemsPerRow = -1;
                            items.forEach(function (item, itemIndex) {
                                var style = {
                                    top: currentTop + 'px',
                                    left: currentLeft + 'px',
                                    width: itemWidth + 'px',
                                    height: itemHeight + 'px'
                                };
                                itemLayouts.push(style);
                                currentLeft += itemWidth + Constants.ITEM_GAP;
                                if (currentLeft >= availableWidth) {
                                    if (itemsPerRow < 0) {
                                        itemsPerRow = itemIndex + 1;
                                    }
                                    currentLeft = Constants.ITEM_GAP;
                                    currentTop += itemHeight + Constants.ITEM_GAP;
                                    numRows++;
                                }
                            });
                            gridStyle = {
                                width: (numRows === 1 ? currentLeft : availableWidth) + 'px',
                                height: (currentLeft == Constants.ITEM_GAP ? currentTop : (currentTop + itemHeight)) + 'px'
                            };
                        }
                        scope.focus = function () {
                            Common.Util.executeInNextEventLoop(function () {
                                document.body.focus();
                                $el[0].querySelector('.navigator-search-box').focus();
                            });
                        };
                        scope.updateLayout = function () {
                            doLayout();
                            Common.Util.executeInNextEventLoop(function () {
                                scope.focus();
                            });
                        };
                        scope.getItemStyle = function (itemIndex) {
                            return itemLayouts[itemIndex];
                        };
                        scope.getHighlightedItemStyle = function () {
                            return highlightedItemStyle;
                        };
                        scope.getGridStyle = function () {
                            return gridStyle;
                        };
                        scope.getNumColumns = function () {
                            return itemsPerRow;
                        };
                        scope.getNumRows = function () {
                            return numRows;
                        };
                        scope.updateHighlightedItem = function () {
                            if (!scope.navigatorItems || scope.navigatorItems.length === 0) {
                                return;
                            }
                            var highlightedItemIndex = scope.getHighlightedItemIndex();
                            if (highlightedItemIndex < 0) {
                                highlightedItemIndex = Math.min(1, scope.navigatorItems.length - 1);
                            }
                            var itemSelector = '.navigator-list' + ' .navigator-item';
                            var itemNode = $el[0].querySelectorAll(itemSelector)[highlightedItemIndex];
                            if (!itemNode) {
                                navigatorUI.logger.error('no item found at index', highlightedItemIndex);
                                return;
                            }
                            itemNode.scrollIntoView(false);
                        };
                        scope.onNavigatorItemClick = function ($event, itemIndex) {
                            var navigatorItem = scope.navigatorItems[itemIndex];
                            if (!navigatorItem) {
                                navigatorUI.logger.warn('onNavigatorItemClick: no navigator item at index', itemIndex, scope.navigatorItems);
                                return;
                            }
                            scope.highlightItem(itemIndex);
                            var inNewTab = $event.shiftKey;
                            scope.navigateToHighlightedItem(inNewTab);
                        };
                        scope.setTypeAheadText = function (text) {
                            $el[0].querySelector('.navigator-search-box-type-ahead').value = text;
                        };
                        scope.refreshNavigatorItem = function (navigatorItem) {
                            var itemSelector = Common.Util.printf('[navigator-item-id="{id}"]', {
                                id: navigatorItem.itemGuid
                            });
                            var itemNode = $el[0].querySelector(itemSelector);
                            if (!itemNode) {
                                navigatorUI.logger.log('No navigator item found with id', navigatorItem.itemGuid, navigatorItem);
                            }
                            TabNavigatorUI.safeApply(angular.element(itemNode).scope());
                        };
                        scope.shouldAllowItemRemoval = function (navigatorItem) {
                            // only closing of tabs allowed for now
                            if (navigatorItem.type !== Common.NavigatorItemType.TAB) {
                                return false;
                            }
                            // if we are in the app we can remove the current tab
                            // without worrying about losing the navigator UI
                            if (navigatorUI.isEmbeddedInCompanionApp()) {
                                return true;
                            }
                            // shouldn't allow dropping current tab
                            // as it could cause jarring behavior.
                            if (!navigatorUI.currentTab) {
                                navigatorUI.logger.error('Current tab not known');
                                return false;
                            }
                            if (navigatorItem.tabId === navigatorUI.currentTab.id) {
                                return false;
                            }
                            return true;
                        };
                        function onKeyUp(event) {
                            switch (event.keyCode) {
                                case 27:
                                    navigatorUI.hideNavigator();
                                    break;
                                case 13:
                                    scope.navigateToHighlightedItem();
                                    break;
                                case 9:
                                    if (!!event.shiftKey) {
                                        scope.highlightPreviousItem();
                                    }
                                    else {
                                        scope.highlightNextItem();
                                    }
                                    break;
                                case 39:
                                    scope.highlightItemInNextColumn();
                                    break;
                                case 37:
                                    scope.highlightItemInPreviousColumn();
                                    break;
                                case 40:
                                    scope.highlightItemInNextRow();
                                    break;
                                case 38:
                                    scope.highlightItemInPreviousRow();
                                    break;
                            }
                            return;
                            // check if there is any command whose meta key is still pressed
                            // if no, switch to the highlighted tab (and hide the navigator)
                            for (var commandName in scope.commandShortcuts) {
                                var properties = scope.commandShortcuts[commandName];
                                if (properties.isCommand && !!event.metaKey) {
                                    return;
                                }
                                if (properties.isCtrl && !!event.ctrlKey) {
                                    return;
                                }
                            }
                            scope.navigateToHighlightedItem();
                        }
                        function registerEventHandlers() {
                            document.addEventListener('keyup', onKeyUp, false);
                            $el.on('click', function (event) {
                                // clicks within navigator content area are ignored
                                if (TabNavigatorUI.isDescendantOf(event.target, '.navigator-content-container')) {
                                    return;
                                }
                                navigatorUI.hideNavigator();
                            });
                            $el.on('mousewheel', function (event) {
                                event.stopPropagation();
                                if (!TabNavigatorUI.isDescendantOf(event.target, '.navigator-list-container')) {
                                    event.preventDefault();
                                    return;
                                }
                                // prevent scrolling of the underlying page
                                var scrollParent = $el[0].querySelector('.navigator-list-container');
                                if (event.wheelDelta >= 0
                                    && scrollParent.scrollTop === 0) {
                                    event.preventDefault();
                                    return;
                                }
                                if (event.wheelDelta <= 0
                                    && Math.abs(scrollParent.scrollTop
                                        + scrollParent.clientHeight
                                        - scrollParent.scrollHeight) <= 1) {
                                    event.preventDefault();
                                    return;
                                }
                            });
                            scope.$on('$destroy', function () {
                                document.removeEventListener('keyup', onKeyUp);
                            });
                        }
                        registerEventHandlers();
                        // focus only if visible
                        if ($el[0].offsetHeight) {
                            scope.focus();
                        }
                    }
                    return {
                        restrict: 'A',
                        replace: true,
                        scope: {},
                        templateUrl: 'templates/navigator.html',
                        link: linker,
                        controller: 'NavigatorController'
                    };
                }]);
            this.app.controller("NavigatorController", function ($scope) {
                var highlightedItemGuid = null;
                // for some reason post messages fired once from the background
                // page are appearing in here multiple times we are patching
                // it by ignoring duplicates using the ID
                var lastNavigationMessageId = -1;
                $scope.queryText = '';
                $scope.navigatorItems = null; // Common.NavigatorItem[]
                $scope.commandShortcuts = {};
                function getMatchingNavigatorItems(queryText, itemType, doFuzzyMatch, callback) {
                    var request = new Common.SearchRequest(queryText, itemType, doFuzzyMatch);
                    var message = new Common.PostMessage(Common.Commands.GET_SEARCH_RESULTS, JSON.stringify(request));
                    window.chrome.runtime.sendMessage(message, callback);
                }
                function getMatchingTabs(queryText, doFuzzyMatch, callback) {
                    getMatchingNavigatorItems(queryText, Common.NavigatorItemType.TAB, doFuzzyMatch, callback);
                }
                function getMatchingHistory(queryText, doFuzzyMatch, callback) {
                    getMatchingNavigatorItems(queryText, Common.NavigatorItemType.BROWSING_HISTORY, doFuzzyMatch, callback);
                }
                function getMatchingWebSearches(queryText, callback) {
                    getMatchingNavigatorItems(queryText, Common.NavigatorItemType.WEB_SEARCH, false, callback);
                }
                function connectToBackgroundPage() {
                    // pushing events to this page from background happens via
                    // the content-script page if this page is injected by a
                    // content-script.
                    if (!navigatorUI.isEmbeddedInCompanionApp()) {
                        return;
                    }
                    if (navigatorUI.bgConnectionPort) {
                        navigatorUI.bgConnectionPort.disconnect();
                    }
                    var connectionName = navigatorUI.getBackgroundConnectionPortName();
                    navigatorUI.bgConnectionPort = chrome.runtime.connect({
                        name: connectionName
                    });
                    navigatorUI.bgConnectionPort.onDisconnect.addListener(function () {
                        navigatorUI.bgConnectionPort = null;
                        navigatorUI.logger.warn('TabNavigator UI, background port disconnected, retrying');
                        connectToBackgroundPage();
                    });
                    navigatorUI.bgConnectionPort.onMessage.addListener(function (message, port) {
                        handleMessage(message);
                    });
                }
                function registerEventHandlers() {
                    connectToBackgroundPage();
                    var deBouncedUpdater = Common.Util.debounce(updateSizesAndPositions, 50);
                    window.addEventListener('message', handleMessageEvent, false);
                    window.addEventListener('resize', deBouncedUpdater, false);
                    window.addEventListener('blur', onWindowFocusLost, false);
                    $scope.$on('$destroy', function () {
                        window.removeEventListener('resize', deBouncedUpdater);
                        window.removeEventListener('message', handleMessageEvent);
                        window.removeEventListener('blur', onWindowFocusLost);
                    });
                }
                function onWindowFocusLost() {
                    navigatorUI.hideNavigator();
                }
                function updateSizesAndPositions() {
                    TabNavigatorUI.safeApply();
                    Common.Util.executeInNextEventLoop(function () {
                        $scope.updateLayout();
                        $scope.updateHighlightedItem();
                        TabNavigatorUI.safeApply();
                    });
                }
                function handleMessageEvent(event) {
                    var message = event.data;
                    return handleMessage(message);
                }
                function handleMessage(message) {
                    if (!message) {
                        return;
                    }
                    if (!message || message.typeId !== Common.PostMessage.POST_MESSAGE_GUID) {
                        return;
                    }
                    if (message.tab) {
                        navigatorUI.currentTab = message.tab;
                    }
                    switch (message.command) {
                        case Common.Commands.NAVIGATE_FORWARD:
                        case Common.Commands.NAVIGATE_BACKWARDS:
                            handleNavigationMessage(message);
                            break;
                        case Common.Commands.HIDE_NAVIGATOR:
                            $scope.queryText = '';
                            break;
                        default:
                            navigatorUI.logger.error('unhandled message command', message.command, event);
                    }
                }
                function setInitialQuery(queryText) {
                    if (!queryText) {
                        return;
                    }
                    $scope.queryText = queryText;
                    $scope.onQueryChange();
                }
                function handleNavigationMessage(data) {
                    if (data.uniqueId === lastNavigationMessageId) {
                        return;
                    }
                    lastNavigationMessageId = data.uniqueId;
                    //var commands: Common.KeyboardCommand[] = config.userPreferences.commands;
                    //$scope.commandShortcuts = navigatorUI.getCommandShortcutsMap(commands);
                    switch (data.command) {
                        case Common.Commands.NAVIGATE_FORWARD:
                            highlightNextItem();
                            setInitialQuery(data.data && data.data.lastQueryText);
                            break;
                        case Common.Commands.NAVIGATE_BACKWARDS:
                            highlightPreviousItem();
                            setInitialQuery(data.data && data.data.lastQueryText);
                            break;
                        default:
                            navigatorUI.logger.error('unhandled navigation message event', data.command);
                    }
                }
                function getItemIndexWithGUID(guid) {
                    return $scope.navigatorItems.findIndex(function (navigatorItem) {
                        return navigatorItem.itemGuid === guid;
                    });
                }
                function getHighlightedItemIndex() {
                    if (!$scope.navigatorItems) {
                        return -1;
                    }
                    return getItemIndexWithGUID(highlightedItemGuid);
                }
                function shiftHighlightedItem(backward) {
                    if (!$scope.navigatorItems || $scope.navigatorItems.length === 0) {
                        navigatorUI.logger.warn('highlightNext: no navigatorItems available', $scope.navigatorItems);
                        return;
                    }
                    var highlightedItemIndex = getHighlightedItemIndex();
                    if (highlightedItemIndex < 0) {
                        if (backward) {
                            highlightedItemIndex = $scope.navigatorItems.length - 1;
                        }
                        else {
                            // if there are at least two items, navigating forward initially
                            // will highlight the 2nd item
                            highlightedItemIndex = Math.min(1, $scope.navigatorItems.length - 1);
                        }
                    }
                    else {
                        if (backward) {
                            highlightedItemIndex--;
                            if (highlightedItemIndex < 0) {
                                highlightedItemIndex %= $scope.navigatorItems.length;
                                highlightedItemIndex += $scope.navigatorItems.length;
                            }
                        }
                        else {
                            highlightedItemIndex = (highlightedItemIndex + 1) % $scope.navigatorItems.length;
                        }
                    }
                    $scope.highlightItem(highlightedItemIndex);
                }
                function highlightNextItem() {
                    ensureNavigatorItemsAvailable(function () {
                        shiftHighlightedItem(false);
                    });
                }
                function highlightPreviousItem() {
                    ensureNavigatorItemsAvailable(function () {
                        shiftHighlightedItem(true);
                    });
                }
                function shiftHighlightedCell(rowDelta, colDelta) {
                    var highlightedItemIndex = getHighlightedItemIndex();
                    var numCols = $scope.getNumColumns();
                    var numRows = $scope.getNumRows();
                    var row = Math.floor(highlightedItemIndex / numCols);
                    var col = highlightedItemIndex % numCols;
                    row += rowDelta;
                    col += colDelta;
                    row = Math.max(0, Math.min(row, numRows - 1));
                    col = Math.max(0, Math.min(col, numCols - 1));
                    var nextItemIndex = row * numCols + col;
                    nextItemIndex = Math.min(nextItemIndex, $scope.navigatorItems.length - 1);
                    $scope.highlightItem(nextItemIndex);
                }
                function executePreservingHighlightedItem(func, callback) {
                    var highlightedItemGUIDBefore = highlightedItemGuid;
                    func(function () {
                        var highlightedItemIndex = getItemIndexWithGUID(highlightedItemGUIDBefore);
                        if (highlightedItemIndex < 0) {
                            highlightedItemIndex = Math.max($scope.navigatorItems.length - 1, 1);
                        }
                        $scope.highlightItem(highlightedItemIndex);
                        callback();
                    });
                }
                function onNavigatorItemPreviewLoaded(navigatorItem, previewRecord) {
                    var thumbURL = (previewRecord && previewRecord.thumbnailURL) || null;
                    var faviconURL = (previewRecord && previewRecord.faviconURL) || null;
                    if (!navigatorUI.isEmbeddedInCompanionApp() || !thumbURL) {
                        navigatorItem.imageURL = thumbURL;
                        navigatorItem.faviconURL = faviconURL;
                        $scope.refreshNavigatorItem(navigatorItem);
                        return;
                    }
                    navigatorUI.getImageDataURIs([thumbURL, faviconURL], function (dataURIs) {
                        var thumbDataURI = dataURIs[0];
                        var faviconDataURI = dataURIs[1];
                        if (thumbDataURI) {
                            navigatorItem.imageURL = thumbDataURI;
                        }
                        if (faviconDataURI) {
                            navigatorItem.faviconURL = faviconDataURI;
                        }
                        if (!!thumbDataURI || !!faviconDataURI) {
                            $scope.refreshNavigatorItem(navigatorItem);
                        }
                    });
                }
                function setNavigatorItems(navigatorItems) {
                    $scope.navigatorItems = navigatorItems;
                    // if the highlighted item is still present keep it highlighted
                    // if it's been removed choose the first item as highlighted
                    var highlightedItemIndex = getHighlightedItemIndex();
                    if (highlightedItemIndex < 0) {
                        if ($scope.navigatorItems.length > 0) {
                            highlightedItemGuid = $scope.navigatorItems[0].itemGuid;
                        }
                        else {
                            highlightedItemGuid = null;
                        }
                    }
                    updateSizesAndPositions();
                    TabNavigatorUI.safeApply($scope);
                    NavigatorItemPreviewLoader.getInstance().trigger(navigatorItems, onNavigatorItemPreviewLoaded);
                }
                function appendNavigatorItems(navigatorItems, callback) {
                    // remove history results that are already in tab results
                    var existingItemUrlSet = {};
                    $scope.navigatorItems.forEach(function (tabItem) {
                        existingItemUrlSet[tabItem.url] = true;
                    });
                    navigatorItems = navigatorItems.filter(function (historyItem) {
                        return !existingItemUrlSet.hasOwnProperty(historyItem.url);
                    });
                    if (navigatorItems.length === 0) {
                        if (callback) {
                            callback();
                            return;
                        }
                    }
                    var existingNavigatorItems = $scope.navigatorItems;
                    Array.prototype.push.apply($scope.navigatorItems, navigatorItems);
                    setNavigatorItems(existingNavigatorItems);
                }
                function updateNavigatorItems(callback, doFuzzyMatch) {
                    doFuzzyMatch = !!doFuzzyMatch;
                    // try in order
                    // 1. tabs matching original query
                    // 2. local browsing history matching original query
                    // 3. only if nothing found so far, tabs matching fuzzily
                    // 4. local browsing history matching fuzzily
                    // 5. only if nothing found so far, web search matching fuzzily
                    getMatchingTabs($scope.queryText, doFuzzyMatch, function (tabItems) {
                        setNavigatorItems(tabItems);
                        getMatchingHistory($scope.queryText, doFuzzyMatch, function (historyItems) {
                            appendNavigatorItems(historyItems, callback);
                            if ($scope.navigatorItems.length === 0) {
                                if (doFuzzyMatch) {
                                    // we already tried fuzzy matching tabs and browsing history
                                    // but didn't find anything, we'll show web searches now
                                    getMatchingWebSearches($scope.queryText, function (webSearchItems) {
                                        appendNavigatorItems(webSearchItems, callback);
                                    });
                                }
                                else {
                                    // we tried strict matching tabs and browsing history
                                    // and didn't find anything, we'll try with fuzzy matching
                                    updateNavigatorItems(callback, true);
                                }
                            }
                        });
                    });
                }
                function ensureNavigatorItemsAvailable(callback) {
                    if (!!$scope.navigatorItems) {
                        callback();
                        return;
                    }
                    updateNavigatorItems(callback);
                }
                function getTypeAheadTextForHighlightedItem(callback) {
                    if (!$scope.queryText) {
                        callback('');
                        return;
                    }
                    var highlightedItem = $scope.getHighlightedItem();
                    if (!highlightedItem) {
                        callback('');
                        return;
                    }
                    var searchedProperties = ['title', 'url'];
                    for (var i = 0; i < searchedProperties.length; ++i) {
                        var propValue = highlightedItem[searchedProperties[i]];
                        if (propValue) {
                            propValue = propValue.toLowerCase();
                            if (propValue.indexOf($scope.queryText) === 0) {
                                callback(propValue);
                                return;
                            }
                        }
                    }
                    callback('');
                }
                var onQueryChangeDeBounced = Common.Util.debounce(function () {
                    updateNavigatorItems(function () {
                        getTypeAheadTextForHighlightedItem(function (typeAheadText) {
                            $scope.setTypeAheadText(typeAheadText);
                        });
                    });
                }, Constants.QUERY_CHANGE_REACTION_DEBOUNCE_INTERVAL);
                $scope.isEmbeddedInCompanionApp = function () {
                    return navigatorUI.isEmbeddedInCompanionApp();
                };
                $scope.isNavigatorItemTab = function (navigatorItem) {
                    return navigatorItem.type === Common.NavigatorItemType.TAB;
                };
                $scope.isNavigatorItemBrowsingHistory = function (navigatorItem) {
                    return navigatorItem.type === Common.NavigatorItemType.BROWSING_HISTORY;
                };
                $scope.getTrackingIdForItem = function (navigatorItem) {
                    if ($scope.isNavigatorItemTab(navigatorItem)) {
                        return navigatorItem.tabId;
                    }
                    if ($scope.isNavigatorItemBrowsingHistory(navigatorItem)) {
                        return navigatorItem.url;
                    }
                    return navigatorItem.itemGuid;
                };
                $scope.onQueryChange = function () {
                    if (!$scope.queryText) {
                        $scope.setTypeAheadText('');
                    }
                    onQueryChangeDeBounced();
                };
                $scope.highlightNextItem = function () {
                    highlightNextItem();
                };
                $scope.highlightPreviousItem = function () {
                    highlightPreviousItem();
                };
                $scope.getHighlightedItemIndex = function () {
                    return getHighlightedItemIndex();
                };
                $scope.highlightItemInNextRow = function () {
                    shiftHighlightedCell(1, 0);
                };
                $scope.highlightItemInPreviousRow = function () {
                    shiftHighlightedCell(-1, 0);
                };
                $scope.highlightItemInNextColumn = function () {
                    shiftHighlightedCell(0, 1);
                };
                $scope.highlightItemInPreviousColumn = function () {
                    shiftHighlightedCell(0, -1);
                };
                $scope.isHighlightedItem = function (index) {
                    var navigatorItem = $scope.navigatorItems[index];
                    if (!navigatorItem) {
                        navigatorUI.logger.error('isHighlightedItem: no navigatorItem at index', index, $scope.navigatorItems);
                        return false;
                    }
                    return navigatorItem.itemGuid === highlightedItemGuid;
                };
                $scope.getHighlightedItem = function () {
                    return $scope.navigatorItems.find(function (navigatorItem) {
                        return navigatorItem.itemGuid === highlightedItemGuid;
                    }) || null;
                };
                $scope.highlightItem = function (itemIndex) {
                    var itemToHighlight = $scope.navigatorItems[itemIndex];
                    if (!itemToHighlight) {
                        navigatorUI.logger.warn('no item to highlight at index', itemIndex, 'in list of items', $scope.navigatorItems);
                        return;
                    }
                    highlightedItemGuid = itemToHighlight.itemGuid;
                    updateSizesAndPositions();
                    TabNavigatorUI.safeApply($scope);
                };
                $scope.navigateToHighlightedItem = function (inNewTab) {
                    if (inNewTab === void 0) { inNewTab = false; }
                    var highlightedItem = $scope.getHighlightedItem();
                    var message;
                    if (!highlightedItem) {
                        if (!$scope.queryText) {
                            return;
                        }
                        message = new Common.PostMessage(Common.Commands.OPEN_GOOGLE_SEARCH, $scope.queryText);
                    }
                    else {
                        var data = new Common.NavigateToItemRequestData(highlightedItem, inNewTab);
                        message = new Common.PostMessage(Common.Commands.NAVIGATE_TO_ITEM, data.stringify());
                    }
                    window.chrome.runtime.sendMessage(message);
                };
                $scope.removeItem = function ($event, navigatorItem) {
                    $event.stopPropagation();
                    var message = new Common.PostMessage(Common.Commands.CLOSE_TAB, navigatorItem.tabId);
                    window.chrome.runtime.sendMessage(message, function () {
                        navigatorUI.logger.log('Successfully removed navigator item');
                    });
                    var itemToRemoveIsHighlighted = navigatorItem.itemGuid === highlightedItemGuid;
                    var itemToRemoveIndex = $scope.navigatorItems.indexOf(navigatorItem);
                    var removed = Common.Util.removeItemFromArray($scope.navigatorItems, navigatorItem);
                    if (!removed) {
                        navigatorUI.logger.warn('Failed to removed navigator item from local items list');
                    }
                    if (itemToRemoveIsHighlighted) {
                        var itemToHighlightIndex = Math.min(itemToRemoveIndex + 1, $scope.navigatorItems.length - 1);
                        $scope.highlightItem(itemToHighlightIndex);
                    }
                    $scope.focus();
                };
                registerEventHandlers();
            });
        };
        TabNavigatorUI.prototype.defineNavigatorItemDirective = function () {
            this.app.directive('navigatorItem', function () {
                function linker(scope, $el, attrs) {
                }
                return {
                    restrict: 'A',
                    scope: {
                        navigatorItem: '=item'
                    },
                    templateUrl: 'templates/navigator-item.html',
                    link: linker
                };
            });
        };
        TabNavigatorUI.prototype.defineFaviconDirective = function () {
            this.app.directive('favicon', function () {
                function linker(scope, $el, attrs) {
                    $el.css('display', 'none');
                    $el.find('img').bind('load', function () {
                        $el.css('display', 'inline-block');
                    });
                    $el.find('img').bind('error', function () {
                        $el.css('display', 'none');
                    });
                }
                return {
                    restrict: 'A',
                    scope: {
                        faviconUrl: '='
                    },
                    templateUrl: 'templates/favicon.html',
                    link: linker
                };
            });
        };
        TabNavigatorUI.prototype.definePreviewImageDirective = function () {
            this.app.directive('previewImage', function () {
                var WHITE_PIXEL_IMAGE = 'data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=';
                var placeholderSettingTimer = null;
                function removeHandlers(img) {
                    img.onload = null;
                    img.onerror = null;
                }
                function clearPlaceholderSettingTimer() {
                    clearTimeout(placeholderSettingTimer);
                    placeholderSettingTimer = null;
                }
                function onImageUrlChange($el, scope) {
                    if (placeholderSettingTimer) {
                        clearTimeout(placeholderSettingTimer);
                    }
                    // wait for a small delay before switching to placeholder image
                    // to avoid flickering in case the real image loads fast enough
                    placeholderSettingTimer = setTimeout(function () {
                        $el.attr('src', scope.loadPlaceholder);
                        $el.addClass('scaled-preview-image');
                    }, 100);
                    var img = new Image();
                    img.onload = function () {
                        removeHandlers(img);
                        clearPlaceholderSettingTimer();
                        clearTimeout(placeholderSettingTimer);
                        $el.removeClass('scaled-preview-image');
                        $el.attr('src', img.src);
                    };
                    img.onerror = function () {
                        removeHandlers(img);
                    };
                    img.src = scope.imageUrl;
                }
                function linker(scope, $el, attrs) {
                    if (!scope.loadPlaceholder) {
                        scope.loadPlaceholder = WHITE_PIXEL_IMAGE;
                    }
                    scope.$watch('imageUrl', function () {
                        onImageUrlChange($el, scope);
                    });
                }
                return {
                    restrict: 'A',
                    scope: {
                        loadPlaceholder: '@',
                        imageUrl: '@'
                    },
                    link: linker
                };
            });
        };
        return TabNavigatorUI;
    }());
    TabNavigator.TabNavigatorUI = TabNavigatorUI;
})(TabNavigator || (TabNavigator = {}));
var tabNavigatorUI = new TabNavigator.TabNavigatorUI();
//# sourceMappingURL=tab-navigator-ui.js.map