/// <reference path='../../lib/ts/chrome.d.ts'/>
/// <reference path='../../common/common.ts'/>

module TabNavigator {
    export class TabNavigatorAppWindow {
        private static NAVIGATOR_CONTENT_FRAME_SELECTOR = '.navigator-content-frame';
        private static NAVIGATOR_CONTENT_FRAME_URL_PATTERN
            = 'chrome-extension://{extensionId}/ui/navigator/tab-navigator-ui.html#{hash}';

        private logger: Common.Logger = new Common.Logger('app-window');

        constructor() {
            var logger = this.logger;
            this.loadNavigatorFrame(function(){
                logger.debug('Successfully loaded navigator frame in app');
            });
            window.addEventListener('message', this.handlePostMessage.bind(this));
        }

        private handlePostMessage(event: MessageEvent) {
            var message: Common.PostMessage = event.data;
            switch (message.command) {
                case Common.Commands.HIDE_NAVIGATOR:
                    // forward it to the background page so that it can
                    // hide the navigator window
                    chrome.runtime.sendMessage(message);
                    break;
                default:
                    this.logger.warn(
                        'Unknown command in companion app navigator window',
                        message.command,
                        message
                    );
                    return;
            }
        }

        private getNavigatorIFrameURL(): string {
            return Common.Util.printf(
                TabNavigatorAppWindow.NAVIGATOR_CONTENT_FRAME_URL_PATTERN,
                {
                    extensionId: Common.Constants.EXTENSION_ID,
                    hash: Common.Constants.NAVIGATOR_UI_APP_EMBED_URL_HASH
                }
            );
        }

        private loadNavigatorFrame(callback: Common.VoidCallback): void {
            var appWindow = this;

            Common.Util.onDocumentReady(document, function(){
                var iFrame = <HTMLFrameElement>document.querySelector(
                    TabNavigatorAppWindow.NAVIGATOR_CONTENT_FRAME_SELECTOR
                );

                var iFrameURL = appWindow.getNavigatorIFrameURL();
                iFrame.onload = function () {
                    iFrame.onload = null;
                    callback();
                };
                iFrame.setAttribute('src', iFrameURL);
            });
        }
    }
}

window['tabNavigatorAppWindow'] = new TabNavigator.TabNavigatorAppWindow();