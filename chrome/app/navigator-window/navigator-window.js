/// <reference path='../../lib/ts/chrome.d.ts'/>
/// <reference path='../../common/common.ts'/>
var TabNavigator;
(function (TabNavigator) {
    var TabNavigatorAppWindow = (function () {
        function TabNavigatorAppWindow() {
            this.logger = new Common.Logger('app-window');
            var logger = this.logger;
            this.loadNavigatorFrame(function () {
                logger.debug('Successfully loaded navigator frame in app');
            });
            window.addEventListener('message', this.handlePostMessage.bind(this));
        }
        TabNavigatorAppWindow.prototype.handlePostMessage = function (event) {
            var message = event.data;
            switch (message.command) {
                case Common.Commands.HIDE_NAVIGATOR:
                    // forward it to the background page so that it can
                    // hide the navigator window
                    chrome.runtime.sendMessage(message);
                    break;
                default:
                    this.logger.warn('Unknown command in companion app navigator window', message.command, message);
                    return;
            }
        };
        TabNavigatorAppWindow.prototype.getNavigatorIFrameURL = function () {
            return Common.Util.printf(TabNavigatorAppWindow.NAVIGATOR_CONTENT_FRAME_URL_PATTERN, {
                extensionId: Common.Constants.EXTENSION_ID,
                hash: Common.Constants.NAVIGATOR_UI_APP_EMBED_URL_HASH
            });
        };
        TabNavigatorAppWindow.prototype.loadNavigatorFrame = function (callback) {
            var appWindow = this;
            Common.Util.onDocumentReady(document, function () {
                var iFrame = document.querySelector(TabNavigatorAppWindow.NAVIGATOR_CONTENT_FRAME_SELECTOR);
                var iFrameURL = appWindow.getNavigatorIFrameURL();
                iFrame.onload = function () {
                    iFrame.onload = null;
                    callback();
                };
                iFrame.setAttribute('src', iFrameURL);
            });
        };
        TabNavigatorAppWindow.NAVIGATOR_CONTENT_FRAME_SELECTOR = '.navigator-content-frame';
        TabNavigatorAppWindow.NAVIGATOR_CONTENT_FRAME_URL_PATTERN = 'chrome-extension://{extensionId}/ui/navigator/tab-navigator-ui.html#{hash}';
        return TabNavigatorAppWindow;
    }());
    TabNavigator.TabNavigatorAppWindow = TabNavigatorAppWindow;
})(TabNavigator || (TabNavigator = {}));
window['tabNavigatorAppWindow'] = new TabNavigator.TabNavigatorAppWindow();
//# sourceMappingURL=navigator-window.js.map