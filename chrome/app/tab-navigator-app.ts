/// <reference path='../lib/ts/chrome.d.ts'/>
/// <reference path='../lib/ts/chrome-app.d.ts'/>
/// <reference path='../common/common.ts'/>

module TabNavigator {
    type AppWindowCallback = (appWindow: chrome.app.window.AppWindow) => void;

    export class TabNavigatorApp {
        private static NAVIGATOR_WINDOW_URL: string = 'navigator-window/navigator.html';
        private static NAVIGATOR_WINDOW_ID: string = 'tab-nav-app-window';
        private static NAVIGATOR_WINDOW_WIDTH_FRACTION: number = 0.7;
        private static NAVIGATOR_WINDOW_HEIGHT_FRACTION: number = 0.5;

        private logger = new Common.Logger('tab-navigator-app');
        private navigatorWindow: chrome.app.window.AppWindow = null;

        public constructor() {
            this.setUpListeners();
        }

        private setUpListeners() {
            var app = this;
            chrome.runtime.onMessageExternal.addListener(
                function(message: Common.PostMessage,
                         sender:chrome.runtime.MessageSender,
                         sendResponse:Function) {
                    if (sender.id !== Common.Constants.EXTENSION_ID) {
                        app.logger.warn(
                            'TabNavigator Companion App Received message from unknown sender',
                            sender.id
                        );
                        return;
                    }
                    return app.handleIncomingMessage(message, sendResponse);
                }
            );

            chrome.runtime.onMessage.addListener(
                function(message: Common.PostMessage,
                         sender:chrome.runtime.MessageSender,
                         sendResponse:Function){
                    return app.handleIncomingMessage(message, sendResponse);
                }
            );
        }

        private handleIncomingMessage(message: Common.PostMessage,
                                      sendResponse:Function) {
            switch (message.command) {
                case Common.Commands.NAVIGATE_FORWARD:
                case Common.Commands.NAVIGATE_BACKWARDS:
                    this.showNavigator(function(){
                        sendResponse(true);
                    });
                    return true;
                case Common.Commands.HIDE_NAVIGATOR:
                    //this.hideNavigator();
                    sendResponse(true);
                    return true;
                default:
                    this.logger.warn(
                        'Unknown command received by companion app',
                        message.command
                    );
                    return;
            }
        }

        private getNavigatorWindowBounds(): chrome.app.window.BoundsSpecification {
            var width: number
                = Math.round(screen.width * TabNavigatorApp.NAVIGATOR_WINDOW_WIDTH_FRACTION);
            var height: number
                = Math.round(screen.height * TabNavigatorApp.NAVIGATOR_WINDOW_HEIGHT_FRACTION);
            var left: number = Math.round((screen.width - width)/2);
            var top: number = Math.round((screen.height - height)/2);

            return {
                width: width,
                height: height,
                minWidth: width,
                minHeight: height,
                maxWidth: width,
                maxHeight: height,
                left: left,
                top: top
            };
        }

        private setBoundsOnWindow(appWindow: chrome.app.window.AppWindow,
                                  bounds: chrome.app.window.BoundsSpecification): void {
            appWindow.outerBounds.setSize(bounds.width, bounds.height);
            appWindow.outerBounds.setMinimumSize(bounds.minWidth, bounds.minHeight);
            appWindow.outerBounds.setMaximumSize(bounds.maxWidth, bounds.maxHeight);
        }


        private createNewNavigatorWindow(callback: AppWindowCallback): void {
            chrome.app.window.create(
                TabNavigatorApp.NAVIGATOR_WINDOW_URL,
                {
                    id: TabNavigatorApp.NAVIGATOR_WINDOW_ID,
                    frame: 'none',
                    alwaysOnTop: false,
                    resizable: false,
                    outerBounds: this.getNavigatorWindowBounds()
                },
                callback
            );
        }

        private getNavigatorWindow(callback: AppWindowCallback = null) {
            if (!!this.navigatorWindow) {
                if (callback) {
                    callback(this.navigatorWindow);
                }
                return;
            }

            var navigatorApp = this;
            this.createNewNavigatorWindow(function(appWindow: chrome.app.window.AppWindow){
                navigatorApp.navigatorWindow = appWindow;
                appWindow.onClosed.addListener(function(){
                    if (navigatorApp.navigatorWindow === appWindow) {
                        navigatorApp.navigatorWindow = null;
                    }
                });

                if (callback) {
                    callback(appWindow);
                }
            });
        }

        private showNavigator(callback: Common.VoidCallback): void {
            if (!this.navigatorWindow) {
                var navigatorApp = this;
                this.getNavigatorWindow(function(appWindow: chrome.app.window.AppWindow){
                    if (!appWindow) {
                        navigatorApp.logger.error('Failed to create a new navigator window');
                        return;
                    }
                    navigatorApp.setBoundsOnWindow(
                        appWindow,
                        navigatorApp.getNavigatorWindowBounds()
                    );
                    appWindow.show();
                    callback();
                });
                return;
            }

            this.navigatorWindow.show();
            callback();
        }

        private hideNavigator(): void {
            if (!this.navigatorWindow) {
                this.logger.warn('No navigator window to hide');
                return;
            }
            this.navigatorWindow.hide();
        }
    }
}

window['tabNavigatorApp'] = new TabNavigator.TabNavigatorApp();