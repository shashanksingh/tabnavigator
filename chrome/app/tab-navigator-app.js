/// <reference path='../lib/ts/chrome.d.ts'/>
/// <reference path='../lib/ts/chrome-app.d.ts'/>
/// <reference path='../common/common.ts'/>
var TabNavigator;
(function (TabNavigator) {
    var TabNavigatorApp = (function () {
        function TabNavigatorApp() {
            this.logger = new Common.Logger('tab-navigator-app');
            this.navigatorWindow = null;
            this.setUpListeners();
        }
        TabNavigatorApp.prototype.setUpListeners = function () {
            var app = this;
            chrome.runtime.onMessageExternal.addListener(function (message, sender, sendResponse) {
                if (sender.id !== Common.Constants.EXTENSION_ID) {
                    app.logger.warn('TabNavigator Companion App Received message from unknown sender', sender.id);
                    return;
                }
                return app.handleIncomingMessage(message, sendResponse);
            });
            chrome.runtime.onMessage.addListener(function (message, sender, sendResponse) {
                return app.handleIncomingMessage(message, sendResponse);
            });
        };
        TabNavigatorApp.prototype.handleIncomingMessage = function (message, sendResponse) {
            switch (message.command) {
                case Common.Commands.NAVIGATE_FORWARD:
                case Common.Commands.NAVIGATE_BACKWARDS:
                    this.showNavigator(function () {
                        sendResponse(true);
                    });
                    return true;
                case Common.Commands.HIDE_NAVIGATOR:
                    //this.hideNavigator();
                    sendResponse(true);
                    return true;
                default:
                    this.logger.warn('Unknown command received by companion app', message.command);
                    return;
            }
        };
        TabNavigatorApp.prototype.getNavigatorWindowBounds = function () {
            var width = Math.round(screen.width * TabNavigatorApp.NAVIGATOR_WINDOW_WIDTH_FRACTION);
            var height = Math.round(screen.height * TabNavigatorApp.NAVIGATOR_WINDOW_HEIGHT_FRACTION);
            var left = Math.round((screen.width - width) / 2);
            var top = Math.round((screen.height - height) / 2);
            return {
                width: width,
                height: height,
                minWidth: width,
                minHeight: height,
                maxWidth: width,
                maxHeight: height,
                left: left,
                top: top
            };
        };
        TabNavigatorApp.prototype.setBoundsOnWindow = function (appWindow, bounds) {
            appWindow.outerBounds.setSize(bounds.width, bounds.height);
            appWindow.outerBounds.setMinimumSize(bounds.minWidth, bounds.minHeight);
            appWindow.outerBounds.setMaximumSize(bounds.maxWidth, bounds.maxHeight);
        };
        TabNavigatorApp.prototype.createNewNavigatorWindow = function (callback) {
            chrome.app.window.create(TabNavigatorApp.NAVIGATOR_WINDOW_URL, {
                id: TabNavigatorApp.NAVIGATOR_WINDOW_ID,
                frame: 'none',
                alwaysOnTop: false,
                resizable: false,
                outerBounds: this.getNavigatorWindowBounds()
            }, callback);
        };
        TabNavigatorApp.prototype.getNavigatorWindow = function (callback) {
            if (callback === void 0) { callback = null; }
            if (!!this.navigatorWindow) {
                if (callback) {
                    callback(this.navigatorWindow);
                }
                return;
            }
            var navigatorApp = this;
            this.createNewNavigatorWindow(function (appWindow) {
                navigatorApp.navigatorWindow = appWindow;
                appWindow.onClosed.addListener(function () {
                    if (navigatorApp.navigatorWindow === appWindow) {
                        navigatorApp.navigatorWindow = null;
                    }
                });
                if (callback) {
                    callback(appWindow);
                }
            });
        };
        TabNavigatorApp.prototype.showNavigator = function (callback) {
            if (!this.navigatorWindow) {
                var navigatorApp = this;
                this.getNavigatorWindow(function (appWindow) {
                    if (!appWindow) {
                        navigatorApp.logger.error('Failed to create a new navigator window');
                        return;
                    }
                    navigatorApp.setBoundsOnWindow(appWindow, navigatorApp.getNavigatorWindowBounds());
                    appWindow.show();
                    callback();
                });
                return;
            }
            this.navigatorWindow.show();
            callback();
        };
        TabNavigatorApp.prototype.hideNavigator = function () {
            if (!this.navigatorWindow) {
                this.logger.warn('No navigator window to hide');
                return;
            }
            this.navigatorWindow.hide();
        };
        TabNavigatorApp.NAVIGATOR_WINDOW_URL = 'navigator-window/navigator.html';
        TabNavigatorApp.NAVIGATOR_WINDOW_ID = 'tab-nav-app-window';
        TabNavigatorApp.NAVIGATOR_WINDOW_WIDTH_FRACTION = 0.7;
        TabNavigatorApp.NAVIGATOR_WINDOW_HEIGHT_FRACTION = 0.5;
        return TabNavigatorApp;
    }());
    TabNavigator.TabNavigatorApp = TabNavigatorApp;
})(TabNavigator || (TabNavigator = {}));
window['tabNavigatorApp'] = new TabNavigator.TabNavigatorApp();
//# sourceMappingURL=tab-navigator-app.js.map