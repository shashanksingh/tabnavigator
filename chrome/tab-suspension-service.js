/// <reference path='lib/ts/chrome.d.ts'/>
/// <reference path='lib/ts/URIjs.d.ts'/>
/// <reference path='common/common.ts'/>
'use strict';
var TabNavigator;
(function (TabNavigator) {
    var TabSuspensionService = (function () {
        function TabSuspensionService() {
            this.logger = new Common.Logger('tab-unloading-service');
            this.initializationTime = Date.now();
            this.tabIdToTimerId = {};
            this.lastActivatedTabId = -1;
            this.initializeListeners();
        }
        TabSuspensionService.getInstance = function () {
            if (!this.instance) {
                this.instance = new TabSuspensionService();
            }
            return this.instance;
        };
        TabSuspensionService.prototype.getSourceForSuspendedTab = function (tabURL) {
            var uri = new URI(tabURL);
            var parameters = uri.search(true);
            return {
                url: parameters[TabSuspensionService.SuspendedTabURLParams.SOURCE_URL],
                title: parameters[TabSuspensionService.SuspendedTabURLParams.SOURCE_TITLE],
                faviconURL: parameters[TabSuspensionService.SuspendedTabURLParams.SOURCE_FAVICON_URL]
            };
        };
        TabSuspensionService.prototype.isSuspendedTabURL = function (tabURL) {
            return tabURL.indexOf(this.getSuspendedTabURL()) === 0;
        };
        TabSuspensionService.prototype.shouldLoadSourceTabOnSuspendedTabLoad = function (tabURL, isVisible) {
            var uri = new URI(tabURL);
            var hash = uri.hash();
            var marker = TabSuspensionService.SuspendedTabURLParams.FRESH_SUSPENSION_HASH;
            if (hash.indexOf(marker) >= 0) {
                return false;
            }
            var suspensionTime = this.getTabSuspensionTime(tabURL);
            if (isNaN(suspensionTime)) {
                return true;
            }
            var timeSinceSuspension = Date.now() - suspensionTime;
            if (timeSinceSuspension < TabSuspensionService.FRESHLY_SUSPENDED_THRESHOLD_MILLIS) {
                return false;
            }
            var timeSinceStart = Date.now() - this.initializationTime;
            if (!isVisible
                && timeSinceStart < TabSuspensionService.NO_RELOAD_ON_RESTART_THRESHOLD_MILLIS) {
                return false;
            }
            return true;
        };
        TabSuspensionService.prototype.getTabSuspensionTime = function (tabURL) {
            var uri = new URI(tabURL);
            var queryParameters = uri.search(true);
            var paramName = TabSuspensionService.SuspendedTabURLParams.SUSPENSION_TIME;
            if (queryParameters.hasOwnProperty(paramName)) {
                return parseInt(queryParameters[paramName], 10);
            }
            return NaN;
        };
        TabSuspensionService.prototype.initializeListeners = function () {
            var tabUnloadingService = this;
            chrome.tabs.query({}, function (tabs) {
                tabs.forEach(function (tab) {
                    tabUnloadingService.setUpTimerForTab(tab.id);
                });
            });
            chrome.tabs.onCreated.addListener(function (tab) {
                tabUnloadingService.setUpTimerForTab(tab.id);
            });
            chrome.tabs.onRemoved.addListener(function (tabId) {
                tabUnloadingService.clearTimerForTab(tabId);
            });
            chrome.tabs.onActivated.addListener(function (activeInfo) {
                // the previous tab is now idle, set up timer for it
                if (tabUnloadingService.lastActivatedTabId >= 0) {
                    tabUnloadingService.setUpTimerForTab(tabUnloadingService.lastActivatedTabId);
                }
                tabUnloadingService.lastActivatedTabId = activeInfo.tabId;
                tabUnloadingService.clearTimerForTab(activeInfo.tabId);
            });
        };
        TabSuspensionService.prototype.getSuspendedTabURL = function (sourceTab) {
            if (sourceTab === void 0) { sourceTab = null; }
            var url = chrome.extension.getURL(TabSuspensionService.SUSPENDED_TAB_URL);
            var uri = new URI(url);
            if (!!sourceTab) {
                var params = {};
                params[TabSuspensionService.SuspendedTabURLParams.SOURCE_URL]
                    = sourceTab.url;
                params[TabSuspensionService.SuspendedTabURLParams.SOURCE_TITLE]
                    = sourceTab.title;
                params[TabSuspensionService.SuspendedTabURLParams.SOURCE_FAVICON_URL]
                    = sourceTab.favIconUrl;
                params[TabSuspensionService.SuspendedTabURLParams.SUSPENSION_TIME]
                    = Date.now();
                uri.setQuery(params);
            }
            uri.hash(TabSuspensionService.SuspendedTabURLParams.FRESH_SUSPENSION_HASH);
            return uri.toString();
        };
        TabSuspensionService.prototype.setUpTimerForTab = function (tabId) {
            var tabUnloadingService = this;
            // just in case there is another timer
            this.clearTimerForTab(tabId);
            var timerId = setTimeout(function () {
                tabUnloadingService.onTabIdleForTooLong(tabId);
            }, TabSuspensionService.IDLE_TIMEOUT);
            tabUnloadingService.tabIdToTimerId[tabId] = timerId;
        };
        TabSuspensionService.prototype.clearTimerForTab = function (tabId) {
            var timerId = this.tabIdToTimerId[tabId];
            if (timerId === void 0) {
                this.logger.warn('No timer found for tab', tabId);
                return;
            }
            delete this.tabIdToTimerId[tabId];
            clearTimeout(timerId);
        };
        TabSuspensionService.prototype.canSuspendTab = function (tab, callback) {
            if (!tab) {
                callback(false);
                return;
            }
            if (tab.audible || tab.pinned || !/^https?:\/\//.test(tab.url)) {
                callback(false);
                return;
            }
            var confirmationMessage = new Common.PostMessage(Common.Commands.CAN_SUSPEND_TAB, null);
            chrome.tabs.sendMessage(tab.id, confirmationMessage, function (canSuspend) {
                callback(canSuspend);
            });
        };
        TabSuspensionService.prototype.onTabIdleForTooLong = function (tabId) {
            var tabUnloadingService = this;
            tabUnloadingService.logger.debug('unloading tab', tabId);
            chrome.tabs.get(tabId, function (tabToSuspend) {
                tabUnloadingService.canSuspendTab(tabToSuspend, function (canSuspend) {
                    if (!canSuspend) {
                        tabUnloadingService.logger.debug('Tab is unsuspendable', tabId);
                        // could not suspend this timer, set another timer
                        // TODO (shashank): may be a faster timer this time?
                        tabUnloadingService.setUpTimerForTab(tabId);
                        return;
                    }
                    tabUnloadingService.clearTimerForTab(tabId);
                    chrome.tabs.update(tabId, {
                        url: tabUnloadingService.getSuspendedTabURL(tabToSuspend)
                    });
                });
            });
        };
        TabSuspensionService.IDLE_TIMEOUT = 60 * 60 * 1000;
        TabSuspensionService.SUSPENDED_TAB_URL = 'ui/suspended/suspended.html';
        // A suspended tab is assumed to be `freshly` suspended for this long
        // after it was initially suspended. Any load during this interval
        // will cause the tab to remain suspended. Any load after this interval
        // and after the hash marker has been removed will cause the source
        // url to load.
        TabSuspensionService.FRESHLY_SUSPENDED_THRESHOLD_MILLIS = 5000;
        // If the user has a lot of suspended tabs we don't want to
        // load the source tabs for all of them on start up as that
        // would cause undesirable load on the system. Hence a suspended
        // tab loading within this interval of startup does not route
        // to the source page (unless this page is currently the
        // active tab).
        TabSuspensionService.NO_RELOAD_ON_RESTART_THRESHOLD_MILLIS = 10000;
        TabSuspensionService.SuspendedTabURLParams = {
            SOURCE_URL: 'u',
            SOURCE_TITLE: 't',
            SOURCE_FAVICON_URL: 'fu',
            SUSPENSION_TIME: 'ts',
            FRESH_SUSPENSION_HASH: 'suspended=true'
        };
        return TabSuspensionService;
    }());
    TabNavigator.TabSuspensionService = TabSuspensionService;
})(TabNavigator || (TabNavigator = {}));
//# sourceMappingURL=tab-suspension-service.js.map