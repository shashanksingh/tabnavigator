/// <reference path='lib/ts/chrome.d.ts'/>
/// <reference path='common/common.ts'/>
/// <reference path='tab-suspension-service.ts'/>
/// <reference path='preview.ts'/>
/// <reference path='search/search-service.ts'/>
/// <reference path='search/search-suggestion-service.ts'/>
/// <reference path='search/history/history-indexer.ts'/>
var TabNavigator;
(function (TabNavigator) {
    var PreviewManager = Preview.PreviewManager;
    var Constants = {
        CONTENT_SCRIPT_BATCH_EXECUTION_CALLBACK_TIMEOUT: 5000,
        MAX_HISTORY_ITEMS: 10,
        TAB_SWITCH_DELAY: 128,
        GOOGLE_SEARCH_URL_TEMPLATE: 'https://www.google.com/search?q={q}&hl={language}',
        GOOGLE_SEARCH_TITLE_TEMPLATE: '{q} - Google Search',
        GOOGLE_SEARCH_NAVIGATOR_ITEM_IMAGE: 'img/google-search-representative.jpg?v=4',
        GOOGLE_SEARCH_NAVIGATOR_ITEM_FAVICON: 'https://www.google.com/images/branding/product/ico/googleg_lodp.ico',
        WEB_HISTORY_NAVIGATOR_ITEM_IMAGE: 'img/web-history-representative.jpg'
    };
    var TabOrderManager = (function () {
        function TabOrderManager() {
            this.logger = new Common.Logger('tab-order-manager');
            this.orderedTabIds = [];
            this.init();
        }
        TabOrderManager.prototype.init = function () {
            SearchService.History.HistoryIndexer.getInstance().initialize();
            var tabOrderManager = this;
            chrome.tabs.query({}, function (tabs) {
                tabOrderManager.orderedTabIds = tabs.map(function (tab) {
                    return tab.id;
                });
            });
            chrome.tabs.onActivated.addListener(function (activeInfo) {
                var activeTabIndex = tabOrderManager.orderedTabIds.indexOf(activeInfo.tabId);
                if (activeTabIndex >= 0) {
                    // an existing tab was activated
                    tabOrderManager.orderedTabIds.splice(activeTabIndex, 1);
                }
                tabOrderManager.orderedTabIds.unshift(activeInfo.tabId);
            });
        };
        TabOrderManager.prototype.getTabIdToTabIndexMap = function () {
            var map = {};
            this.orderedTabIds.forEach(function (tabId, tabIndex) {
                map[tabId] = tabIndex;
            });
            return map;
        };
        return TabOrderManager;
    }());
    var Navigator = (function () {
        function Navigator() {
            this.companionAppConnectionPort = null;
            this.logger = new Common.Logger('tab-navigator');
            this.tabOrderManager = new TabOrderManager();
            this.lastActiveTabId = -1;
            this.previewManager = Preview.PreviewManager.getInstance();
            this.tabUnloadingService = TabNavigator.TabSuspensionService.getInstance();
            // inject content script in all tabs in case we were initialized after
            // page load, in case of new install e.g.
            this.setUpMessageListeners();
            this.setUpKeyboardCommands();
            this.setUpChromeExtensionButtonClickListener();
            this.setUpTabListeners();
            this.executeContentScriptsInExistingTabs(function () {
                // this will create a singleton and initialize the index
                SearchService.TabSearchService.getInstance();
            });
            var tabNavigator = this;
            window.addEventListener('error', function (evt) {
                tabNavigator.logger.error('Uncaught exception in background script', evt);
            });
        }
        Navigator.prototype.isCompanionAppConnected = function () {
            return this.companionAppConnectionPort !== null;
        };
        Navigator.prototype.setUpMessageListeners = function () {
            var _this = this;
            chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
                switch (request.command) {
                    case Common.Commands.NAVIGATE_TO_ITEM:
                        _this.notifySwitchedOutTab();
                        var requestData = JSON.parse(request.data);
                        // allow the current tab to re-render and hide the navigator before
                        // we do the switch. without the switch the current tab re-renders
                        // only when re-focused causing the old navigator state to flash
                        // for a moment
                        setTimeout(function () {
                            _this.navigateToItem(requestData);
                        }, Constants.TAB_SWITCH_DELAY);
                        return false;
                    case Common.Commands.OPEN_GOOGLE_SEARCH:
                        var query = request.data;
                        _this.openGoogleSearch(query);
                        return false;
                    case Common.Commands.GET_SEARCH_RESULTS:
                        var searchRequest = JSON.parse(request.data);
                        _this.getSearchResults(searchRequest, function (navigatorItems) {
                            _this.lastSearchRequest = searchRequest;
                            sendResponse(navigatorItems);
                        });
                        return true;
                    case Common.Commands.GET_PREVIEW_RECORD:
                        var recordRequest = JSON.parse(request.data);
                        _this.previewManager.getPreviewRecords([recordRequest.url], function (previewRecords) {
                            sendResponse(previewRecords[0]);
                        });
                        return true;
                    case Common.Commands.GET_IMAGE_DATA_URL:
                        var dataURIRequest = JSON.parse(request.data);
                        PreviewManager.getInstance().getDataURIForFileSystemURL(dataURIRequest.fileSystemURL, function (error, dataURI) {
                            sendResponse(error, dataURI);
                        });
                        return true;
                    case Common.Commands.GET_PAGE_SEARCHABLE_DATA:
                        // this component does not listen to this event
                        break;
                    case Common.Commands.CLOSE_TAB:
                        var tabId = request.data;
                        _this.closeTab(tabId, function () {
                            sendResponse();
                        });
                        return true;
                    default:
                        _this.logger.error('unhandled request', request.command, request);
                        return false;
                }
            });
            chrome.runtime.onConnect.addListener(function (port) {
                // incoming connection request from navigator UI loaded from the app
                if (port.name === Common.Constants.NAVIGATOR_UI_CONNECTION_ID_APP) {
                    _this.companionAppConnectionPort = port;
                    port.onDisconnect.addListener(function () {
                        // if this is the current port to the nav UI loaded from the
                        // app clear the state
                        if (this.companionAppConnectionPort === port) {
                            this.companionAppConnectionPort = null;
                        }
                    });
                }
                else if (port.name !== Common.Constants.NAVIGATOR_UI_CONNECTION_ID_TAB) {
                    _this.logger.warn('TabNavigator received connection from unknown source', port.name);
                }
            });
        };
        Navigator.prototype.setUpKeyboardCommands = function () {
            var _this = this;
            chrome.commands.onCommand.addListener(function (command) {
                switch (command) {
                    case Common.KeyboardCommands.NAVIGATE_FORWARD:
                        _this.navigate(Common.Commands.NAVIGATE_FORWARD);
                        break;
                    case Common.KeyboardCommands.NAVIGATE_BACKWARDS:
                        _this.navigate(Common.Commands.NAVIGATE_BACKWARDS);
                        break;
                    default:
                        _this.logger.error('unhandled command', command);
                        break;
                }
            });
        };
        Navigator.prototype.setUpTabListeners = function () {
            var self = this;
            chrome.tabs.onActivated.addListener(function (activeInfo) {
                if (self.lastActiveTabId === activeInfo.tabId) {
                    return;
                }
                self.logger.log('active tab id changed from', self.lastActiveTabId, 'to', activeInfo.tabId);
                self.notifySwitchedOutTab();
                self.lastActiveTabId = activeInfo.tabId;
                self.notifySwitchedInTab(activeInfo.tabId);
            });
        };
        Navigator.prototype.setUpChromeExtensionButtonClickListener = function () {
            var _this = this;
            chrome.browserAction.onClicked.addListener(function (tab) {
                _this.navigate(Common.Commands.TOGGLE_NAVIGATOR);
            });
        };
        Navigator.prototype.openGoogleSearch = function (query) {
            var googleURL = Common.Util.printf('https://www.google.com/search?q={q}', {
                q: encodeURIComponent(query)
            });
            chrome.tabs.create({ url: googleURL });
        };
        Navigator.prototype.closeTab = function (tabId, callback) {
            var tabNavigator = this;
            chrome.tabs.remove(tabId, function () {
                tabNavigator.logger.debug('Tab closed from navigator UI');
                if (callback) {
                    callback();
                }
            });
        };
        Navigator.prototype.getCurrentUserPreferences = function (callback) {
            var userPreferences = new Common.UserPreferences(), logger = this.logger;
            (chrome.commands).getAll(function (commands) {
                commands.forEach(function (command) {
                    if (!command.shortcut) {
                        logger.error('no shortcut for command', command);
                        return;
                    }
                });
                userPreferences.commands = commands;
                callback(userPreferences);
            });
        };
        Navigator.prototype.getAllTabsInLRUOrder = function (callback) {
            var tabNavigator = this;
            chrome.tabs.query({}, function (tabs) {
                var tabIdToTabIndex = tabNavigator.tabOrderManager.getTabIdToTabIndexMap();
                tabs.sort(function (tabA, tabB) {
                    var indexA = tabIdToTabIndex[tabA.id];
                    var indexB = tabIdToTabIndex[tabB.id];
                    if (isNaN(indexA) && isNaN(indexB)) {
                        return 0;
                    }
                    if (isNaN(indexA) && !isNaN(indexB)) {
                        return 1;
                    }
                    if (!isNaN(indexB) && isNaN(indexB)) {
                        return -1;
                    }
                    return indexA - indexB;
                });
                callback(tabs);
            });
        };
        Navigator.prototype.getAllTabsMatchingQuery = function (query, callback) {
            var tabNavigator = this;
            var searchService = SearchService.TabSearchService.getInstance();
            searchService.getMatchingTabIds(query, function (error, matchingTabIds) {
                if (!!error) {
                    tabNavigator.logger.error('Error in getting matching tabs for query', query, error);
                    // if all fails fall back to chrome's search in tab titles
                    // TODO: search in urls too
                    chrome.tabs.query({ title: query }, callback);
                    return;
                }
                // check again with list of tabs to remove
                // any tabs that are no longer present while
                // preserving the ranking
                chrome.tabs.query({}, function (allTabs) {
                    var allTabIdToTab = {};
                    allTabs.forEach(function (tab) {
                        allTabIdToTab[tab.id] = tab;
                    });
                    matchingTabIds = matchingTabIds.filter(function (tabId) {
                        return allTabIdToTab.hasOwnProperty(tabId + '');
                    });
                    var matchingTabs = matchingTabIds.map(function (tabId) {
                        return allTabIdToTab[tabId + ''];
                    });
                    callback(matchingTabs);
                });
            });
        };
        Navigator.prototype.getTabs = function (query, callback) {
            if (!query) {
                this.getAllTabsInLRUOrder(callback);
            }
            else {
                this.getAllTabsMatchingQuery(query, callback);
            }
        };
        Navigator.prototype.mapTabToNavigatorItem = function (tab) {
            var item = new Common.NavigatorItem();
            item.type = Common.NavigatorItemType.TAB;
            item.url = tab.url;
            item.title = tab.title;
            item.tabId = tab.id;
            item.windowId = tab.windowId;
            item.imageURL = '';
            item.faviconURL = '';
            return item;
            /*tabNavigator.getCurrentUserPreferences(function(userPreferences: Common.UserPreferences){
                var data = new Common.NavigationRequestData(navigatorItems, userPreferences);
                var message = new Common.PostMessage(navigationCommand, JSON.stringify(data));
                Common.Util.sendMessageToCurrentTab(message);
            });*/
        };
        Navigator.prototype.mapHistoryItemToNavigatorItem = function (historyItem) {
            var item = new Common.NavigatorItem();
            var isGoogleSearch = Common.Util.isGoogleSearchURL(historyItem.url);
            var imageURL = isGoogleSearch
                ? Constants.GOOGLE_SEARCH_NAVIGATOR_ITEM_IMAGE : Constants.WEB_HISTORY_NAVIGATOR_ITEM_IMAGE;
            var faviconURL = isGoogleSearch ? Constants.GOOGLE_SEARCH_NAVIGATOR_ITEM_FAVICON : '';
            item.type = Common.NavigatorItemType.BROWSING_HISTORY;
            item.url = historyItem.url;
            item.title = historyItem.title;
            item.tabId = -1;
            item.windowId = -1;
            item.imageURL = imageURL;
            item.faviconURL = faviconURL;
            return item;
        };
        Navigator.prototype.mapSearchSuggestionToNavigatorItem = function (searchSuggestion) {
            var url = Common.Util.printf(Constants.GOOGLE_SEARCH_URL_TEMPLATE, {
                q: encodeURIComponent(searchSuggestion),
                language: encodeURIComponent(navigator.language)
            });
            var title = Common.Util.printf(Constants.GOOGLE_SEARCH_TITLE_TEMPLATE, {
                q: searchSuggestion
            });
            var item = new Common.NavigatorItem();
            item.type = Common.NavigatorItemType.WEB_SEARCH;
            item.url = url;
            item.title = title;
            item.tabId = -1;
            item.windowId = -1;
            item.imageURL = Constants.GOOGLE_SEARCH_NAVIGATOR_ITEM_IMAGE;
            item.faviconURL = Constants.GOOGLE_SEARCH_NAVIGATOR_ITEM_FAVICON;
            return item;
        };
        Navigator.prototype.getAlternativeQuery = function (typedQuery, callback) {
            if (!typedQuery) {
                callback('');
                return;
            }
            SearchService.SearchSuggestionService.getInstance().getSuggestions(typedQuery, function (suggestions) {
                if (!suggestions || suggestions.length === 0) {
                    callback('');
                    return;
                }
                var wordToFrequency = {};
                suggestions.forEach(function (suggestion) {
                    var words = suggestion.split(/\s+/);
                    words.forEach(function (word) {
                        wordToFrequency[word] = (wordToFrequency[word] || 0) + 1;
                    });
                });
                var allWords = Object.keys(wordToFrequency);
                allWords.sort(function (wordA, wordB) {
                    var freqA = wordToFrequency[wordA];
                    var freqB = wordToFrequency[wordB];
                    return freqB - freqA;
                });
                var mostFrequentWord = allWords[0];
                callback(mostFrequentWord);
            });
        };
        /**
         * Returns the final query to be used for the given search request. If fuzzy matching needs
         * to be done the alternative query is returned.
         * @param searchRequest
         * @param callback
         */
        Navigator.prototype.getFinalQueryForSearchRequest = function (searchRequest, callback) {
            if (!searchRequest.doFuzzyMatch) {
                callback(searchRequest.query);
                return;
            }
            this.getAlternativeQuery(searchRequest.query, callback);
        };
        Navigator.prototype.getSearchResults = function (searchRequest, callback) {
            var tabNavigator = this;
            this.getFinalQueryForSearchRequest(searchRequest, function (finalQuery) {
                switch (searchRequest.itemType) {
                    case Common.NavigatorItemType.TAB:
                        tabNavigator.getTabSearchResults(finalQuery, callback);
                        break;
                    case Common.NavigatorItemType.BROWSING_HISTORY:
                        tabNavigator.getHistorySearchResults(finalQuery, callback);
                        break;
                    case Common.NavigatorItemType.WEB_SEARCH:
                        tabNavigator.getWebSearchSearchResults(searchRequest.query, callback);
                        break;
                    default:
                        tabNavigator.logger.error('unhandled navigator item type', searchRequest.itemType);
                        break;
                }
            });
        };
        Navigator.prototype.getTabSearchResults = function (query, callback) {
            var tabNavigator = this;
            this.getTabs(query, function (tabs) {
                var navigatorItems = tabs.map(function (tab) {
                    return tabNavigator.mapTabToNavigatorItem(tab);
                });
                callback(navigatorItems);
            });
        };
        Navigator.prototype.getHistorySearchResults = function (query, callback) {
            var tabNavigator = this;
            if (!query) {
                callback([]);
            }
            else {
                var historySearchTelemetryId = Common.Telemeter.getInstance().start('history-search', query);
                SearchService.History.HistoryIndexer.getInstance().getMatchingHistoryItems(query, function (error, matchingHistoryItems) {
                    Common.Telemeter.getInstance().end(historySearchTelemetryId);
                    if (!!error) {
                        tabNavigator.logger.error('Error in getting history search results', error);
                        callback([]);
                        return;
                    }
                    tabNavigator.logger.log('history search results', query, matchingHistoryItems.length);
                    var navigatorItems = matchingHistoryItems.map(function (historyItem) {
                        return tabNavigator.mapHistoryItemToNavigatorItem(historyItem);
                    });
                    callback(navigatorItems);
                });
            }
        };
        Navigator.prototype.getWebSearchSearchResults = function (query, callback) {
            var tabNavigator = this;
            SearchService.SearchSuggestionService.getInstance().getSuggestions(query, function (suggestions) {
                if (!suggestions) {
                    return;
                }
                var webSearchItems = suggestions.map(function (suggestion) {
                    return tabNavigator.mapSearchSuggestionToNavigatorItem(suggestion);
                });
                callback(webSearchItems);
            });
        };
        Navigator.prototype.navigate = function (navigationCommand) {
            var lastQueryText = null;
            if (this.lastSearchRequest) {
                var now = new Date().getTime();
                var timeSinceLastSearch = now - this.lastSearchRequest.timestamp;
                if (timeSinceLastSearch < Navigator.REMEMBER_LAST_SEARCH_MILLIS) {
                    lastQueryText = this.lastSearchRequest.query;
                }
            }
            var message = new Common.PostMessage(navigationCommand, {
                lastQueryText: lastQueryText
            });
            Common.Util.sendMessageToCurrentTab(message);
            /*

            // TODO (shashank): handle disconnection between
            // sending message to app and to the navigator
            // UI loaded by the app
            chrome.runtime.sendMessage(
                Common.Constants.APP_ID,
                message,
                (received: boolean) => {
                    if (received) {
                        if (this.companionAppConnectionPort) {
                            this.companionAppConnectionPort.postMessage(message);
                        }
                    } else {
                        Common.Util.sendMessageToCurrentTab(message);
                    }
                }
            );*/
        };
        Navigator.prototype.notifySwitchedOutTab = function () {
            if (this.lastActiveTabId < 0) {
                return;
            }
            var message;
            message = new Common.PostMessage(Common.Commands.HANDLE_TAB_SWITCH_OUT, null);
            chrome.tabs.sendMessage(this.lastActiveTabId, message);
            message = new Common.PostMessage(Common.Commands.HIDE_NAVIGATOR, null);
            chrome.tabs.sendMessage(this.lastActiveTabId, message);
        };
        Navigator.prototype.notifySwitchedInTab = function (tabId) {
            var message = new Common.PostMessage(Common.Commands.HANDLE_TAB_SWITCH_IN, null);
            chrome.tabs.sendMessage(tabId, message);
        };
        Navigator.prototype.navigateToTab = function (tabId, windowId) {
            chrome.tabs.update(tabId, { active: true });
            chrome.windows.update(windowId, { focused: true });
            Common.Util.executeInNextEventLoop(function () {
                var message = new Common.PostMessage(Common.Commands.HANDLE_TAB_SWITCH_IN, null);
                chrome.tabs.sendMessage(tabId, message);
            });
        };
        Navigator.prototype.navigateToItem = function (request) {
            var item = request.navigatorItem;
            if (request.inNewTab) {
                chrome.tabs.create({ url: item.url });
                return;
            }
            if (item.type === Common.NavigatorItemType.TAB) {
                this.navigateToTab(item.tabId, item.windowId);
            }
            else {
                chrome.tabs.query({ url: item.url }, function (tabs) {
                    if (tabs.length > 0) {
                        this.navigateToTab(item.tabId, item.windowId);
                    }
                    else {
                        chrome.tabs.create({ url: item.url });
                    }
                });
            }
        };
        Navigator.prototype.isAccessibleTab = function (tab) {
            return !!tab.url
                && tab.url.indexOf('chrome://') !== 0
                && tab.url.indexOf('data:') !== 0
                && tab.url.indexOf('about:') !== 0;
        };
        Navigator.prototype.executeContentScriptsInExistingTabs = function (callback) {
            // to make sure that failure of content script execution for any reason
            // does not prevent the callback from ever being called we make sure
            // it's certainly called after a certain time interval
            callback = Common.Util.getTemporallyAutoCallback(callback, Constants.CONTENT_SCRIPT_BATCH_EXECUTION_CALLBACK_TIMEOUT);
            var tabNavigator = this;
            var executedTabsCount = 0;
            chrome.tabs.query({}, function (tabs) {
                function onTabDone() {
                    executedTabsCount++;
                    if (executedTabsCount === tabs.length) {
                        callback();
                    }
                }
                tabs.forEach(function (tab) {
                    if (!tabNavigator.isAccessibleTab(tab)) {
                        onTabDone();
                        return;
                    }
                    // depending on chrome support for content script in special pages this block
                    // can fail
                    try {
                        chrome.tabs.insertCSS(tab.id, { file: 'content-script/content-script.css' }, function () {
                            chrome.tabs.executeScript(tab.id, { file: 'common/common.js' }, function () {
                                chrome.tabs.executeScript(tab.id, { file: 'content-script/content-script.js' }, function () {
                                    tabNavigator.logger.log('content script executed in existing tab', tab.url);
                                    onTabDone();
                                });
                            });
                        });
                    }
                    catch (e) {
                        tabNavigator.logger.error('Error in executing content scripts in existing tabs', e);
                        onTabDone();
                    }
                });
            });
        };
        Navigator.REMEMBER_LAST_SEARCH_MILLIS = 8 * 1000;
        return Navigator;
    }());
    TabNavigator.Navigator = Navigator;
})(TabNavigator || (TabNavigator = {}));
window['tabNavigator'] = new TabNavigator.Navigator();
//# sourceMappingURL=tabnavigator.js.map