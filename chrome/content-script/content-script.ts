/// <reference path='../lib/ts/chrome.d.ts'/>

/// <reference path='../common/common.ts'/>
/// <reference path='../tabnavigator.ts'/>


module NavigatorInjector {
    var Constants = {
        PREVIEW_IMG_IDEAL_ASPECT_RATIO: 1.0,
        PREVIEW_IMG_IDEAL_MIN_SIDE: 60,
        PREVIEW_CANVAS_SIZE: 400,
        // must start with an alpha char for css selector to work
        NAVIGATOR_IFRAME_ID: 'e458870c-06b6-4f0f-b757-7b0bef88a036',
        SUSPENDED_IFRAME_ID: '27b8c363-dd9c-4d00-8a54-e8bca1efb81f',
        IS_SUSPENDED_ATTRIBUTE: 'tn-is-suspended-d52ad59e-5ca6-4579-9613-208db4d43808',
        DOCUMENT_TEXT_MAX_LENGTH: 1<<16,
        LAST_ACTIVE_ELEMENT_WEAK_MAP_KEY: {},
        NON_TEXT_TAG_NAMES: [
            'BASE', 'HEAD', 'STYLE',
            'AREA', 'AUDIO', 'MAP', 'TRACK', 'VIDEO',
            'EMBED', 'OBJECT', 'PARAM', 'SOURCE',
            'CANVAS', 'NOSCRIPT', 'SCRIPT',
            'DEL', 'INS',
            'COL', 'COLGROUP', 'TABLE', 'TBODY', 'TR',
            'IFRAME', 'IMG', 'STYLE', 'META']
    };

    type IFrameCallback = (iFrame: HTMLIFrameElement) => void;

    class ImageProperties {
        public src: string;
        public width: number;
        public height: number;
        public aspectRatio: number;

        constructor(public image: HTMLImageElement) {
            this.src = image.src;
            this.width = image.naturalWidth;
            this.height = image.naturalHeight;
            this.aspectRatio = this.width/this.height;
        }
    }

    export class NavigatorUI {
        private logger: Common.Logger;

        private isInjectedAfterDocumentLoad: boolean = true;
        private unloadEventListeners: Function[] = [];

        private previewImageURL: string;
        private currentTab: chrome.tabs.Tab;

        // there is no way to hold a weak reference to a
        // DOM node so we use WeakMap from ES6. Note that
        // we can't use WeakSet because it does not allow
        // getting back the element. This is supported in
        // Chrome >= 36
        private lastActiveElement: WeakMap<string, Element>;

        constructor() {
            var navigatorUI = this;

            this.logger = new Common.Logger('navigator-ui');
            this.isInjectedAfterDocumentLoad = window.document && document.readyState != 'loading';
            this.interceptUnloadEventListener();

            this.lastActiveElement = new WeakMap<string, Element>();

            chrome.runtime.onMessage.addListener(this.handleIncomingMessage.bind(this));

            Common.Util.onDocumentReady(document, function(){
                navigatorUI.onDocumentReady()
            });

            window.addEventListener('message', function(event){
                var data: Common.PostMessage = event.data;
                if (!data || data.typeId !== Common.PostMessage.POST_MESSAGE_GUID) {
                    return;
                }

                switch (data.command) {
                    case Common.Commands.HIDE_NAVIGATOR:
                        navigatorUI.hideNavigator();
                        break;
                    default:
                        navigatorUI.logger.error('unknown command on window.message', data.command, event);
                }
            }, false);

            window.addEventListener('error', function(evt: ErrorEvent){
                // only catch events in extension context
                if (evt.filename && evt.filename.indexOf(chrome.runtime.id) < 0) {
                    return;
                }
                navigatorUI.logger.error('Uncaught exception in content script', evt);
            });
        }

        private isWindowUnloadEvent(eventName): boolean {
            return eventName === 'beforeunload' || eventName === 'unload';
        }

        private interceptUnloadEventListener = function () {
            var navigatorUI = this;
            var stockAddEventListener = Window.prototype.addEventListener;
            var stockRemoveEventListener = Window.prototype.removeEventListener;

            Window.prototype.addEventListener = function (eventName, handler, useCapture) {
                if (navigatorUI.isWindowUnloadEvent(eventName)) {
                    navigatorUI.unloadEventListeners.push(handler);
                }
                return stockAddEventListener.call(this, eventName, handler, useCapture);
            };
            Window.prototype.removeEventListener = function(eventName, handler, useCapture) {
                if (navigatorUI.isWindowUnloadEvent(eventName)) {
                    Common.Util.removeItemFromArray(
                        navigatorUI.unloadEventListeners,
                        handler,
                        true
                    );
                }
                return stockRemoveEventListener.call(this, eventName, handler, useCapture);
            };
        };

        private onDocumentReady() {
            var navigatorUI = this;
            // add the page to the index
            var message = new Common.PostMessage(
                Common.Commands.GET_PAGE_SEARCHABLE_DATA,
                JSON.stringify(navigatorUI.getSearchableText())
            );
            (<any>window).chrome.runtime.sendMessage(message);
        }

        private handleIncomingMessage(message: Common.PostMessage,
                                      sender:chrome.runtime.MessageSender,
                                      sendResponse:Function) {

            if (message.tab) {
                this.currentTab = message.tab;
            }

            switch (message.command) {
                case Common.Commands.NAVIGATE_FORWARD:
                    this.navigateForward(message.data);
                    break;
                case Common.Commands.NAVIGATE_BACKWARDS:
                    this.navigateBackwards(message.data);
                    break;
                case Common.Commands.HIDE_NAVIGATOR:
                    this.hideNavigator();
                    break;
                case Common.Commands.TOGGLE_NAVIGATOR:
                    this.toggleNavigator();
                    break;
                case Common.Commands.GET_PAGE_PREVIEW:
                    sendResponse(this.getPreviewImageURL());
                    break;
                case Common.Commands.GET_PAGE_SEARCHABLE_DATA:
                    sendResponse(this.getSearchableText());
                    break;
                case Common.Commands.IS_SHOWING_NAVIGATOR:
                    sendResponse(this.isShowingNavigator());
                    break;
                case Common.Commands.IS_TAB_SUSPENDED:
                    sendResponse(this.isTabSuspended());
                    break;
                case Common.Commands.CAN_TAKE_SCREEN_SHOT:
                    sendResponse(this.canTakeScreenShot());
                    break;
                case Common.Commands.HANDLE_TAB_SWITCH_OUT:
                    this.handleTabSwitchOut();
                    break;
                case Common.Commands.HANDLE_TAB_SWITCH_IN:
                    this.handleTabSwitchIn();
                    break;
                case Common.Commands.CAN_SUSPEND_TAB:
                    sendResponse(this.canSuspendTab());
                    break;
                default:
                    this.logger.error('unknown command on chrome.runtime', message);
                    break;
            }
        }

        private getPreviewImageURL(): string {
            if (!this.previewImageURL) {
                // TODO: grab text, render to canvas, use as image
                var previewImageElement: HTMLImageElement = this.extractPreviewImage();
                this.logger.debug('short term preview image element', previewImageElement);

                if (previewImageElement) {
                    this.previewImageURL = previewImageElement.src;
                }
            }
            return this.previewImageURL;
        }

        private extractPreviewImage(): HTMLImageElement {
            var images: HTMLImageElement[] = Array.prototype.slice.call(document.images),
                properties: ImageProperties[] = images.map(function(image: HTMLImageElement){
                    return new ImageProperties(image);
                });

            properties = properties.filter(function(image: ImageProperties){
                return !!image.src && !isNaN(image.aspectRatio);
            });

            properties.sort(function(propA, propB): number{
                var deltaA = Math.abs(propA.aspectRatio - Constants.PREVIEW_IMG_IDEAL_ASPECT_RATIO),
                    deltaB = Math.abs(propB.aspectRatio - Constants.PREVIEW_IMG_IDEAL_ASPECT_RATIO);
                return deltaA - deltaB;
            });

            for (var i: number = 0; i<properties.length; ++i) {
                var props: ImageProperties = properties[i];
                if (props.width >= Constants.PREVIEW_IMG_IDEAL_MIN_SIDE
                    && props.height >= Constants.PREVIEW_IMG_IDEAL_MIN_SIDE) {
                    return props.image;
                }
            }

            return null;
        }

        private getSearchableText(): Common.TabSearchableData {
            return new Common.TabSearchableData(
                document.location.href,
                document.title,
                this.getTabDescription(),
                this.getTabText(Constants.DOCUMENT_TEXT_MAX_LENGTH)
            );
        }

        private getTabDescription(): string {
            var metaTags: NodeListOf<HTMLMetaElement> = document.getElementsByTagName('meta');
            var descriptions = [];
            for (var i=0; i<metaTags.length; ++i) {
                var metaTag = metaTags[i];
                var tagName = metaTag.getAttribute('name');
                // tag name could be description, twitter:description etc.
                if (tagName && tagName.toLowerCase().indexOf('description') >= 0) {
                    var content = metaTag.getAttribute('content');
                    descriptions.push(content);
                }
            }
            return descriptions.join(' ');
        }

        private getTabText(maxLength: number): string {
            var currentTextLength: number = 0;
            var allTexts: string[] = [];

            var allNodes: NodeList = document.body.getElementsByTagName('*');
            for (var i=0; i<allNodes.length; ++i) {
                var node: HTMLElement = <HTMLElement>allNodes[i];
                if (node.childElementCount > 0) {
                    continue;
                }

                var tagName: string = node.tagName.toUpperCase();
                if (Constants.NON_TEXT_TAG_NAMES.indexOf(tagName) >= 0) {
                    continue;
                }

                var text = node.innerText;
                if (!text) {
                    continue;
                }

                allTexts.push(text);
                currentTextLength += text.length;

                if (currentTextLength >= maxLength) {
                    break;
                }
            }
            return allTexts.join(' ').substr(0, maxLength);
        }

        private createNewIFrame(url: string, id: string): HTMLIFrameElement {
            // the page might in the process of a reload
            if (!document.body) {
                return null;
            }

            var iFrame: HTMLIFrameElement = document.createElement('iframe');
            iFrame.setAttribute('id', id);
            iFrame.setAttribute('seamless', '');
            iFrame.frameBorder = '0';
            iFrame.src = chrome.extension.getURL(url);

            document.body.appendChild(iFrame);

            return iFrame;
        }

        private getLoadedIFrame(callback: IFrameCallback,
                                url: string,
                                id: string,
                                autoShow: boolean) {
            var iFrame: HTMLIFrameElement
                = <HTMLIFrameElement>document.getElementById(id);
            if (!!iFrame) {
                if (iFrame.getAttribute('loaded')) {
                    if (autoShow) {
                        iFrame.style.display = 'block';
                    }
                    if (callback) {
                        callback(iFrame);
                    }
                    return;
                }
            } else {
                iFrame = this.createNewIFrame(url, id);
            }

            if (!iFrame) {
                if (document.readyState === 'complete') {
                    this.logger.error('Failed to get iFrame even after document load');
                    callback(null);
                    return;
                }

                var navigatorUI = this;
                document.addEventListener('DOMContentLoaded', function(){
                    navigatorUI.getLoadedIFrame(callback, url, id, autoShow);
                });
                return;
            }

            iFrame.onload = function () {
                iFrame.onload = null;
                iFrame.setAttribute('loaded', 'true');

                // wait for angular to compile
                setTimeout(function(){
                    if (autoShow) {
                        iFrame.style.display = 'block';
                    }
                    if (callback) {
                        callback(iFrame);
                    }
                }, 0);
            };
        }

        private getLoadedNavigatorIFrame(callback: IFrameCallback, autoShow: boolean) {
            return this.getLoadedIFrame(
                function(iFrame: HTMLIFrameElement) {
                    if (!iFrame.parentNode) {
                        document.body.appendChild(iFrame);
                    }
                    if (callback) {
                        callback(iFrame);
                    }
                },
                'ui/navigator/tab-navigator-ui.html',
                Constants.NAVIGATOR_IFRAME_ID,
                autoShow
            );
        }

        private getLoadedSuspendedTabIFrame(callback: IFrameCallback) {
            return this.getLoadedIFrame(
                callback,
                'ui/suspended/suspended.html',
                Constants.SUSPENDED_IFRAME_ID,
                true // suspended tab frame is always auto shown
            );
        }

        private navigate(navigationCommand: any, data: any) {
            this.showNavigator((iFrame: HTMLIFrameElement) => {
                var message = new Common.PostMessage(navigationCommand, data);
                message.tab = this.currentTab;
                iFrame.contentWindow.postMessage(message, iFrame.src);
            });
        }

        private showNavigator(callback?: IFrameCallback): void {
            this.captureLastActiveElement();

            Common.Telemeter.getInstance().start('NAVIGATOR_IFRAME_LOAD');
            this.getLoadedNavigatorIFrame((iFrame: HTMLIFrameElement) => {
                if (!iFrame) {
                    return;
                }
                Common.Telemeter.getInstance().end('NAVIGATOR_IFRAME_LOAD');
                if (!!callback) {
                    callback(iFrame);
                }
            }, true);
        }

        private navigateForward(data?: any) {
            this.navigate(Common.Commands.NAVIGATE_FORWARD, data);
        }

        private navigateBackwards(data?: any) {
            this.navigate(Common.Commands.NAVIGATE_BACKWARDS, data);
        }

        private getNavigatorIFrame(): HTMLIFrameElement {
            return (<HTMLIFrameElement>(document.getElementById(Constants.NAVIGATOR_IFRAME_ID))) || null;
        }

        private hideNavigator() {
            var iFrame = this.getNavigatorIFrame();
            if (!!iFrame) {
                iFrame.style.display = 'none';
            }

            this.restoreLastActiveElement();
        }

        private removeNavigator() {
            var iFrame = this.getNavigatorIFrame();
            if (!!iFrame) {
                document.body.removeChild(iFrame);
            }
        }

        private toggleNavigator() {
            if (this.isShowingNavigator()) {
                this.hideNavigator();
            }
            this.showNavigator();
        }

        private isShowingNavigator(): boolean {
            var iFrame = this.getNavigatorIFrame();
            return iFrame && iFrame.style.display == 'block';
        }

        private isTabSuspended(): boolean {
            return document.documentElement.hasAttribute(Constants.IS_SUSPENDED_ATTRIBUTE);
        }

        private canTakeScreenShot(): boolean {
            return !this.isShowingNavigator() && !this.isTabSuspended();
        }

        private clearLastActiveElement(): void {
            this.lastActiveElement.set(
                Constants.LAST_ACTIVE_ELEMENT_WEAK_MAP_KEY,
                null
            );
        }

        private captureLastActiveElement(): void {
            this.clearLastActiveElement();

            if (!!document.activeElement) {
                this.lastActiveElement.set(
                    Constants.LAST_ACTIVE_ELEMENT_WEAK_MAP_KEY,
                    document.activeElement
                );
            }
        }

        private restoreLastActiveElement(): void {
            var lastActiveElement: Element = this.lastActiveElement.get(
                Constants.LAST_ACTIVE_ELEMENT_WEAK_MAP_KEY
            );

            this.clearLastActiveElement();

            if (!!lastActiveElement) {
                Common.Util.executeInNextEventLoop(function(){
                    (<HTMLElement>lastActiveElement).focus();
                });
            }
        }

        private handleTabSwitchOut(): void {
            this.removeNavigator();
            this.captureLastActiveElement();
        }

        private handleTabSwitchIn(): void {
            this.restoreLastActiveElement();
            // pre-load iframe for fast response time
            // iframe will be removed once the tab switches out
            this.getLoadedNavigatorIFrame(null, false);
        }

        private hasFilledTextInput(): boolean {
            var inputs:NodeListOf<Element> = document.querySelectorAll(
                'input[type="text"], input[type="textarea"]'
            );

            for (var i = 0; i < inputs.length; ++i) {
                var input = <HTMLInputElement>(inputs[i]);
                if (input.value) {
                    return true;
                }
            }
            return false;
        }

        private hasUnloadListeners(): boolean {
            // if scripts have already run we have no
            // way to tell if they page has installed
            // unload event listeners
            if (this.isInjectedAfterDocumentLoad) {
                return true;
            }

            if (!!window.onbeforeunload || !!window.onunload) {
                return true;
            }
            if (this.unloadEventListeners.length > 0) {
                return true;
            }
            return false;
        }

        private canSuspendTab(): boolean {
            if (this.hasFilledTextInput()) {
                return false;
            }
            if (this.hasUnloadListeners()) {
                return false;
            }
            return true;
        }
    }
}

window['navigatorUI'] = new NavigatorInjector.NavigatorUI();


