/// <reference path='../lib/ts/chrome.d.ts'/>
/// <reference path='../common/common.ts'/>
/// <reference path='../tabnavigator.ts'/>
var NavigatorInjector;
(function (NavigatorInjector) {
    var Constants = {
        PREVIEW_IMG_IDEAL_ASPECT_RATIO: 1.0,
        PREVIEW_IMG_IDEAL_MIN_SIDE: 60,
        PREVIEW_CANVAS_SIZE: 400,
        // must start with an alpha char for css selector to work
        NAVIGATOR_IFRAME_ID: 'e458870c-06b6-4f0f-b757-7b0bef88a036',
        SUSPENDED_IFRAME_ID: '27b8c363-dd9c-4d00-8a54-e8bca1efb81f',
        IS_SUSPENDED_ATTRIBUTE: 'tn-is-suspended-d52ad59e-5ca6-4579-9613-208db4d43808',
        DOCUMENT_TEXT_MAX_LENGTH: 1 << 16,
        LAST_ACTIVE_ELEMENT_WEAK_MAP_KEY: {},
        NON_TEXT_TAG_NAMES: [
            'BASE', 'HEAD', 'STYLE',
            'AREA', 'AUDIO', 'MAP', 'TRACK', 'VIDEO',
            'EMBED', 'OBJECT', 'PARAM', 'SOURCE',
            'CANVAS', 'NOSCRIPT', 'SCRIPT',
            'DEL', 'INS',
            'COL', 'COLGROUP', 'TABLE', 'TBODY', 'TR',
            'IFRAME', 'IMG', 'STYLE', 'META']
    };
    var ImageProperties = (function () {
        function ImageProperties(image) {
            this.image = image;
            this.src = image.src;
            this.width = image.naturalWidth;
            this.height = image.naturalHeight;
            this.aspectRatio = this.width / this.height;
        }
        return ImageProperties;
    }());
    var NavigatorUI = (function () {
        function NavigatorUI() {
            this.isInjectedAfterDocumentLoad = true;
            this.unloadEventListeners = [];
            this.interceptUnloadEventListener = function () {
                var navigatorUI = this;
                var stockAddEventListener = Window.prototype.addEventListener;
                var stockRemoveEventListener = Window.prototype.removeEventListener;
                Window.prototype.addEventListener = function (eventName, handler, useCapture) {
                    if (navigatorUI.isWindowUnloadEvent(eventName)) {
                        navigatorUI.unloadEventListeners.push(handler);
                    }
                    return stockAddEventListener.call(this, eventName, handler, useCapture);
                };
                Window.prototype.removeEventListener = function (eventName, handler, useCapture) {
                    if (navigatorUI.isWindowUnloadEvent(eventName)) {
                        Common.Util.removeItemFromArray(navigatorUI.unloadEventListeners, handler, true);
                    }
                    return stockRemoveEventListener.call(this, eventName, handler, useCapture);
                };
            };
            var navigatorUI = this;
            this.logger = new Common.Logger('navigator-ui');
            this.isInjectedAfterDocumentLoad = window.document && document.readyState != 'loading';
            this.interceptUnloadEventListener();
            this.lastActiveElement = new WeakMap();
            chrome.runtime.onMessage.addListener(this.handleIncomingMessage.bind(this));
            Common.Util.onDocumentReady(document, function () {
                navigatorUI.onDocumentReady();
            });
            window.addEventListener('message', function (event) {
                var data = event.data;
                if (!data || data.typeId !== Common.PostMessage.POST_MESSAGE_GUID) {
                    return;
                }
                switch (data.command) {
                    case Common.Commands.HIDE_NAVIGATOR:
                        navigatorUI.hideNavigator();
                        break;
                    default:
                        navigatorUI.logger.error('unknown command on window.message', data.command, event);
                }
            }, false);
            window.addEventListener('error', function (evt) {
                // only catch events in extension context
                if (evt.filename && evt.filename.indexOf(chrome.runtime.id) < 0) {
                    return;
                }
                navigatorUI.logger.error('Uncaught exception in content script', evt);
            });
        }
        NavigatorUI.prototype.isWindowUnloadEvent = function (eventName) {
            return eventName === 'beforeunload' || eventName === 'unload';
        };
        NavigatorUI.prototype.onDocumentReady = function () {
            var navigatorUI = this;
            // add the page to the index
            var message = new Common.PostMessage(Common.Commands.GET_PAGE_SEARCHABLE_DATA, JSON.stringify(navigatorUI.getSearchableText()));
            window.chrome.runtime.sendMessage(message);
        };
        NavigatorUI.prototype.handleIncomingMessage = function (message, sender, sendResponse) {
            if (message.tab) {
                this.currentTab = message.tab;
            }
            switch (message.command) {
                case Common.Commands.NAVIGATE_FORWARD:
                    this.navigateForward(message.data);
                    break;
                case Common.Commands.NAVIGATE_BACKWARDS:
                    this.navigateBackwards(message.data);
                    break;
                case Common.Commands.HIDE_NAVIGATOR:
                    this.hideNavigator();
                    break;
                case Common.Commands.TOGGLE_NAVIGATOR:
                    this.toggleNavigator();
                    break;
                case Common.Commands.GET_PAGE_PREVIEW:
                    sendResponse(this.getPreviewImageURL());
                    break;
                case Common.Commands.GET_PAGE_SEARCHABLE_DATA:
                    sendResponse(this.getSearchableText());
                    break;
                case Common.Commands.IS_SHOWING_NAVIGATOR:
                    sendResponse(this.isShowingNavigator());
                    break;
                case Common.Commands.IS_TAB_SUSPENDED:
                    sendResponse(this.isTabSuspended());
                    break;
                case Common.Commands.CAN_TAKE_SCREEN_SHOT:
                    sendResponse(this.canTakeScreenShot());
                    break;
                case Common.Commands.HANDLE_TAB_SWITCH_OUT:
                    this.handleTabSwitchOut();
                    break;
                case Common.Commands.HANDLE_TAB_SWITCH_IN:
                    this.handleTabSwitchIn();
                    break;
                case Common.Commands.CAN_SUSPEND_TAB:
                    sendResponse(this.canSuspendTab());
                    break;
                default:
                    this.logger.error('unknown command on chrome.runtime', message);
                    break;
            }
        };
        NavigatorUI.prototype.getPreviewImageURL = function () {
            if (!this.previewImageURL) {
                // TODO: grab text, render to canvas, use as image
                var previewImageElement = this.extractPreviewImage();
                this.logger.debug('short term preview image element', previewImageElement);
                if (previewImageElement) {
                    this.previewImageURL = previewImageElement.src;
                }
            }
            return this.previewImageURL;
        };
        NavigatorUI.prototype.extractPreviewImage = function () {
            var images = Array.prototype.slice.call(document.images), properties = images.map(function (image) {
                return new ImageProperties(image);
            });
            properties = properties.filter(function (image) {
                return !!image.src && !isNaN(image.aspectRatio);
            });
            properties.sort(function (propA, propB) {
                var deltaA = Math.abs(propA.aspectRatio - Constants.PREVIEW_IMG_IDEAL_ASPECT_RATIO), deltaB = Math.abs(propB.aspectRatio - Constants.PREVIEW_IMG_IDEAL_ASPECT_RATIO);
                return deltaA - deltaB;
            });
            for (var i = 0; i < properties.length; ++i) {
                var props = properties[i];
                if (props.width >= Constants.PREVIEW_IMG_IDEAL_MIN_SIDE
                    && props.height >= Constants.PREVIEW_IMG_IDEAL_MIN_SIDE) {
                    return props.image;
                }
            }
            return null;
        };
        NavigatorUI.prototype.getSearchableText = function () {
            return new Common.TabSearchableData(document.location.href, document.title, this.getTabDescription(), this.getTabText(Constants.DOCUMENT_TEXT_MAX_LENGTH));
        };
        NavigatorUI.prototype.getTabDescription = function () {
            var metaTags = document.getElementsByTagName('meta');
            var descriptions = [];
            for (var i = 0; i < metaTags.length; ++i) {
                var metaTag = metaTags[i];
                var tagName = metaTag.getAttribute('name');
                // tag name could be description, twitter:description etc.
                if (tagName && tagName.toLowerCase().indexOf('description') >= 0) {
                    var content = metaTag.getAttribute('content');
                    descriptions.push(content);
                }
            }
            return descriptions.join(' ');
        };
        NavigatorUI.prototype.getTabText = function (maxLength) {
            var currentTextLength = 0;
            var allTexts = [];
            var allNodes = document.body.getElementsByTagName('*');
            for (var i = 0; i < allNodes.length; ++i) {
                var node = allNodes[i];
                if (node.childElementCount > 0) {
                    continue;
                }
                var tagName = node.tagName.toUpperCase();
                if (Constants.NON_TEXT_TAG_NAMES.indexOf(tagName) >= 0) {
                    continue;
                }
                var text = node.innerText;
                if (!text) {
                    continue;
                }
                allTexts.push(text);
                currentTextLength += text.length;
                if (currentTextLength >= maxLength) {
                    break;
                }
            }
            return allTexts.join(' ').substr(0, maxLength);
        };
        NavigatorUI.prototype.createNewIFrame = function (url, id) {
            // the page might in the process of a reload
            if (!document.body) {
                return null;
            }
            var iFrame = document.createElement('iframe');
            iFrame.setAttribute('id', id);
            iFrame.setAttribute('seamless', '');
            iFrame.frameBorder = '0';
            iFrame.src = chrome.extension.getURL(url);
            document.body.appendChild(iFrame);
            return iFrame;
        };
        NavigatorUI.prototype.getLoadedIFrame = function (callback, url, id, autoShow) {
            var iFrame = document.getElementById(id);
            if (!!iFrame) {
                if (iFrame.getAttribute('loaded')) {
                    if (autoShow) {
                        iFrame.style.display = 'block';
                    }
                    if (callback) {
                        callback(iFrame);
                    }
                    return;
                }
            }
            else {
                iFrame = this.createNewIFrame(url, id);
            }
            if (!iFrame) {
                if (document.readyState === 'complete') {
                    this.logger.error('Failed to get iFrame even after document load');
                    callback(null);
                    return;
                }
                var navigatorUI = this;
                document.addEventListener('DOMContentLoaded', function () {
                    navigatorUI.getLoadedIFrame(callback, url, id, autoShow);
                });
                return;
            }
            iFrame.onload = function () {
                iFrame.onload = null;
                iFrame.setAttribute('loaded', 'true');
                // wait for angular to compile
                setTimeout(function () {
                    if (autoShow) {
                        iFrame.style.display = 'block';
                    }
                    if (callback) {
                        callback(iFrame);
                    }
                }, 0);
            };
        };
        NavigatorUI.prototype.getLoadedNavigatorIFrame = function (callback, autoShow) {
            return this.getLoadedIFrame(function (iFrame) {
                if (!iFrame.parentNode) {
                    document.body.appendChild(iFrame);
                }
                if (callback) {
                    callback(iFrame);
                }
            }, 'ui/navigator/tab-navigator-ui.html', Constants.NAVIGATOR_IFRAME_ID, autoShow);
        };
        NavigatorUI.prototype.getLoadedSuspendedTabIFrame = function (callback) {
            return this.getLoadedIFrame(callback, 'ui/suspended/suspended.html', Constants.SUSPENDED_IFRAME_ID, true // suspended tab frame is always auto shown
            );
        };
        NavigatorUI.prototype.navigate = function (navigationCommand, data) {
            var _this = this;
            this.showNavigator(function (iFrame) {
                var message = new Common.PostMessage(navigationCommand, data);
                message.tab = _this.currentTab;
                iFrame.contentWindow.postMessage(message, iFrame.src);
            });
        };
        NavigatorUI.prototype.showNavigator = function (callback) {
            this.captureLastActiveElement();
            Common.Telemeter.getInstance().start('NAVIGATOR_IFRAME_LOAD');
            this.getLoadedNavigatorIFrame(function (iFrame) {
                if (!iFrame) {
                    return;
                }
                Common.Telemeter.getInstance().end('NAVIGATOR_IFRAME_LOAD');
                if (!!callback) {
                    callback(iFrame);
                }
            }, true);
        };
        NavigatorUI.prototype.navigateForward = function (data) {
            this.navigate(Common.Commands.NAVIGATE_FORWARD, data);
        };
        NavigatorUI.prototype.navigateBackwards = function (data) {
            this.navigate(Common.Commands.NAVIGATE_BACKWARDS, data);
        };
        NavigatorUI.prototype.getNavigatorIFrame = function () {
            return (document.getElementById(Constants.NAVIGATOR_IFRAME_ID)) || null;
        };
        NavigatorUI.prototype.hideNavigator = function () {
            var iFrame = this.getNavigatorIFrame();
            if (!!iFrame) {
                iFrame.style.display = 'none';
            }
            this.restoreLastActiveElement();
        };
        NavigatorUI.prototype.removeNavigator = function () {
            var iFrame = this.getNavigatorIFrame();
            if (!!iFrame) {
                document.body.removeChild(iFrame);
            }
        };
        NavigatorUI.prototype.toggleNavigator = function () {
            if (this.isShowingNavigator()) {
                this.hideNavigator();
            }
            this.showNavigator();
        };
        NavigatorUI.prototype.isShowingNavigator = function () {
            var iFrame = this.getNavigatorIFrame();
            return iFrame && iFrame.style.display == 'block';
        };
        NavigatorUI.prototype.isTabSuspended = function () {
            return document.documentElement.hasAttribute(Constants.IS_SUSPENDED_ATTRIBUTE);
        };
        NavigatorUI.prototype.canTakeScreenShot = function () {
            return !this.isShowingNavigator() && !this.isTabSuspended();
        };
        NavigatorUI.prototype.clearLastActiveElement = function () {
            this.lastActiveElement.set(Constants.LAST_ACTIVE_ELEMENT_WEAK_MAP_KEY, null);
        };
        NavigatorUI.prototype.captureLastActiveElement = function () {
            this.clearLastActiveElement();
            if (!!document.activeElement) {
                this.lastActiveElement.set(Constants.LAST_ACTIVE_ELEMENT_WEAK_MAP_KEY, document.activeElement);
            }
        };
        NavigatorUI.prototype.restoreLastActiveElement = function () {
            var lastActiveElement = this.lastActiveElement.get(Constants.LAST_ACTIVE_ELEMENT_WEAK_MAP_KEY);
            this.clearLastActiveElement();
            if (!!lastActiveElement) {
                Common.Util.executeInNextEventLoop(function () {
                    lastActiveElement.focus();
                });
            }
        };
        NavigatorUI.prototype.handleTabSwitchOut = function () {
            this.removeNavigator();
            this.captureLastActiveElement();
        };
        NavigatorUI.prototype.handleTabSwitchIn = function () {
            this.restoreLastActiveElement();
            // pre-load iframe for fast response time
            // iframe will be removed once the tab switches out
            this.getLoadedNavigatorIFrame(null, false);
        };
        NavigatorUI.prototype.hasFilledTextInput = function () {
            var inputs = document.querySelectorAll('input[type="text"], input[type="textarea"]');
            for (var i = 0; i < inputs.length; ++i) {
                var input = (inputs[i]);
                if (input.value) {
                    return true;
                }
            }
            return false;
        };
        NavigatorUI.prototype.hasUnloadListeners = function () {
            // if scripts have already run we have no
            // way to tell if they page has installed
            // unload event listeners
            if (this.isInjectedAfterDocumentLoad) {
                return true;
            }
            if (!!window.onbeforeunload || !!window.onunload) {
                return true;
            }
            if (this.unloadEventListeners.length > 0) {
                return true;
            }
            return false;
        };
        NavigatorUI.prototype.canSuspendTab = function () {
            if (this.hasFilledTextInput()) {
                return false;
            }
            if (this.hasUnloadListeners()) {
                return false;
            }
            return true;
        };
        return NavigatorUI;
    }());
    NavigatorInjector.NavigatorUI = NavigatorUI;
})(NavigatorInjector || (NavigatorInjector = {}));
window['navigatorUI'] = new NavigatorInjector.NavigatorUI();
//# sourceMappingURL=content-script.js.map