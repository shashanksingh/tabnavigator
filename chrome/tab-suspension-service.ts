/// <reference path='lib/ts/chrome.d.ts'/>
/// <reference path='lib/ts/URIjs.d.ts'/>
/// <reference path='common/common.ts'/>

'use strict';

module TabNavigator {
    export interface SuspendedTabSource {
        url: string;
        title: string;
        faviconURL: string;
    }

    export class TabSuspensionService {
        private static IDLE_TIMEOUT: number = 60 * 60 * 1000;
        private static SUSPENDED_TAB_URL: string = 'ui/suspended/suspended.html';
        // A suspended tab is assumed to be `freshly` suspended for this long
        // after it was initially suspended. Any load during this interval
        // will cause the tab to remain suspended. Any load after this interval
        // and after the hash marker has been removed will cause the source
        // url to load.
        private static FRESHLY_SUSPENDED_THRESHOLD_MILLIS = 5000;
        // If the user has a lot of suspended tabs we don't want to
        // load the source tabs for all of them on start up as that
        // would cause undesirable load on the system. Hence a suspended
        // tab loading within this interval of startup does not route
        // to the source page (unless this page is currently the
        // active tab).
        private static NO_RELOAD_ON_RESTART_THRESHOLD_MILLIS = 10000;
        private static SuspendedTabURLParams = {
            SOURCE_URL: 'u',
            SOURCE_TITLE: 't',
            SOURCE_FAVICON_URL: 'fu',
            SUSPENSION_TIME: 'ts',
            FRESH_SUSPENSION_HASH: 'suspended=true'
        };

        private static instance: TabSuspensionService;

        private logger: Common.Logger = new Common.Logger('tab-unloading-service');
        private initializationTime: number = Date.now();
        private tabIdToTimerId: {[tabId: string] : number} = {};
        private lastActivatedTabId: number = -1;


        public static getInstance(): TabSuspensionService {
            if (!this.instance) {
                this.instance = new TabSuspensionService();
            }
            return this.instance;
        }

        constructor() {
            this.initializeListeners();
        }

        public getSourceForSuspendedTab(tabURL: string): SuspendedTabSource {
            var uri: uri.URI = new URI(tabURL);
            var parameters: any = uri.search(true);
            return {
                url: parameters[TabSuspensionService.SuspendedTabURLParams.SOURCE_URL],
                title: parameters[TabSuspensionService.SuspendedTabURLParams.SOURCE_TITLE],
                faviconURL: parameters[TabSuspensionService.SuspendedTabURLParams.SOURCE_FAVICON_URL],
            };
        }

        public isSuspendedTabURL(tabURL: string): boolean {
            return tabURL.indexOf(this.getSuspendedTabURL()) === 0;
        }

        public shouldLoadSourceTabOnSuspendedTabLoad(tabURL: string,
                                                     isVisible: boolean): boolean {
            var uri: uri.URI = new URI(tabURL);
            var hash: string = uri.hash();
            var marker: string
                = TabSuspensionService.SuspendedTabURLParams.FRESH_SUSPENSION_HASH;
            if (hash.indexOf(marker) >= 0) {
                return false;
            }

            var suspensionTime: number = this.getTabSuspensionTime(tabURL);
            if (isNaN(suspensionTime)) {
                return true;
            }

            var timeSinceSuspension: number = Date.now() - suspensionTime;
            if (timeSinceSuspension < TabSuspensionService.FRESHLY_SUSPENDED_THRESHOLD_MILLIS) {
                return false;
            }

            var timeSinceStart: number = Date.now() - this.initializationTime;
            if (!isVisible
                && timeSinceStart < TabSuspensionService.NO_RELOAD_ON_RESTART_THRESHOLD_MILLIS) {
                return false;
            }

            return true;
        }


        private getTabSuspensionTime(tabURL: string): number {
            var uri: uri.URI = new URI(tabURL);
            var queryParameters: any = uri.search(true);
            var paramName: string
                = TabSuspensionService.SuspendedTabURLParams.SUSPENSION_TIME;
            if (queryParameters.hasOwnProperty(paramName)) {
                return parseInt(queryParameters[paramName], 10);
            }
            return NaN;
        }

        private initializeListeners() {

            var tabUnloadingService = this;

            chrome.tabs.query({}, function(tabs: chrome.tabs.Tab[]) {
                tabs.forEach(function(tab: chrome.tabs.Tab){
                    tabUnloadingService.setUpTimerForTab(tab.id);
                });
            });

            chrome.tabs.onCreated.addListener(function(tab: chrome.tabs.Tab){
                tabUnloadingService.setUpTimerForTab(tab.id);
            });

            chrome.tabs.onRemoved.addListener(function(tabId: number){
                tabUnloadingService.clearTimerForTab(tabId);
            });

            chrome.tabs.onActivated.addListener(function(activeInfo: chrome.tabs.TabActiveInfo){
                // the previous tab is now idle, set up timer for it
                if (tabUnloadingService.lastActivatedTabId >= 0) {
                    tabUnloadingService.setUpTimerForTab(tabUnloadingService.lastActivatedTabId);
                }
                tabUnloadingService.lastActivatedTabId = activeInfo.tabId;
                tabUnloadingService.clearTimerForTab(activeInfo.tabId);
            });
        }

        private getSuspendedTabURL(sourceTab: chrome.tabs.Tab = null) {
            var url = chrome.extension.getURL(TabSuspensionService.SUSPENDED_TAB_URL);
            var uri: uri.URI = new URI(url);
            if (!!sourceTab) {
                var params: any = {};
                params[TabSuspensionService.SuspendedTabURLParams.SOURCE_URL]
                    = sourceTab.url;
                params[TabSuspensionService.SuspendedTabURLParams.SOURCE_TITLE]
                    = sourceTab.title;
                params[TabSuspensionService.SuspendedTabURLParams.SOURCE_FAVICON_URL]
                    = sourceTab.favIconUrl;
                params[TabSuspensionService.SuspendedTabURLParams.SUSPENSION_TIME]
                    = Date.now();

                uri.setQuery(params);
            }
            uri.hash(TabSuspensionService.SuspendedTabURLParams.FRESH_SUSPENSION_HASH);

            return uri.toString();
        }

        private setUpTimerForTab(tabId: number) {
            var tabUnloadingService = this;

            // just in case there is another timer
            this.clearTimerForTab(tabId);

            var timerId = setTimeout(function(){
                tabUnloadingService.onTabIdleForTooLong(tabId);
            }, TabSuspensionService.IDLE_TIMEOUT);
            tabUnloadingService.tabIdToTimerId[tabId] = timerId;
        }

        private clearTimerForTab(tabId: number) {
            var timerId = this.tabIdToTimerId[tabId];
            if (timerId === void 0) {
                this.logger.warn('No timer found for tab', tabId);
                return;
            }
            delete this.tabIdToTimerId[tabId];
            clearTimeout(timerId);
        }

        private canSuspendTab(tab: chrome.tabs.Tab, callback: Common.BooleanCallback): void {
            if (!tab) {
                callback(false);
                return;
            }

            if ((<any>tab).audible || tab.pinned || !/^https?:\/\//.test(tab.url)) {
                callback(false);
                return;
            }

            var confirmationMessage = new Common.PostMessage(
                Common.Commands.CAN_SUSPEND_TAB,
                null
            );
            chrome.tabs.sendMessage(tab.id, confirmationMessage, function(canSuspend) {
                callback(canSuspend);
            });
        }

        private onTabIdleForTooLong(tabId: number) {
            var tabUnloadingService = this;

            tabUnloadingService.logger.debug('unloading tab', tabId);

            chrome.tabs.get(tabId, function(tabToSuspend: chrome.tabs.Tab){
                tabUnloadingService.canSuspendTab(tabToSuspend, function(canSuspend: boolean){
                    if (!canSuspend) {
                        tabUnloadingService.logger.debug('Tab is unsuspendable', tabId);
                        // could not suspend this timer, set another timer
                        // TODO (shashank): may be a faster timer this time?
                        tabUnloadingService.setUpTimerForTab(tabId);
                        return;
                    }

                    tabUnloadingService.clearTimerForTab(tabId);

                    chrome.tabs.update(
                        tabId,
                        {
                            url: tabUnloadingService.getSuspendedTabURL(tabToSuspend)
                        }
                    );

                });
            });
        }
    }
}