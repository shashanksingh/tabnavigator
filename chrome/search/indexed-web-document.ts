/// <reference path='../lib/ts/URIjs.d.ts'/>

'use strict';

module SearchService {
    export class IndexedWebDocument {

        public static DocumentField = {
            ID: 'id',
            URL: 'url',
            URL_SIMPLIFIED: 'url_simplified',
            TITLE: 'title',
            DOMAIN: 'domain',
            PATH: 'path',
            PROTOCOL: 'protocol',
            DESCRIPTION: 'description',
            TEXT: 'text'
        };

        public static DocumentProperty = {
            FIELDS: 'fields',
            BOOST: 'boost'
        };

        private documentBoost: number = 1.0;
        private storeUrl: boolean = false;
        private storeTitle: boolean = false;
        private additionalFields: any[] = [];

        constructor(private id, private url: string, private title?: string,
                    private description?: string, private text?: string) {
            this.id = id + '';
        }

        public static getSearchFields(query: string): any[] {
            var allSearchableFields = this.getSearchableFields();
            return allSearchableFields.map(function(searchableField){
                return {
                    name: searchableField,
                    value: query,
                    matchType: ((<any>window).Lucene).LuceneJS.MatchType.FULL_TEXT
                };
            });
        }

        public addField(fieldName: string, fieldValue: any, shouldIndex: boolean = false,
                        shouldStore: boolean = false, shouldAnalzye: boolean = false,
                        boost: number = 1) {
            this.additionalFields.push({
                name: fieldName,
                value: fieldValue,
                shouldIndex: shouldIndex,
                shouldStore: shouldStore,
                shouldAnalyze: shouldAnalzye,
                boost: boost
            });
        }

        public setDocumentBoost(boost: number): void {
            this.documentBoost = boost;
        }

        public setDescription(description: string): void {
            this.description = description;
        }

        public setText(text: string): void {
            this.text = text;
        }

        public setStoreUrl(storeUrl: boolean): void {
            this.storeUrl = !!storeUrl;
        }

        public setStoreTitle(storeTitle: boolean): void {
            this.storeTitle = !!storeTitle;
        }

        private tokenizeHost(host: string): string {
            var parts = host.split('.');
            return parts.join(' ');
        }

        private getSimplifiedUrl(uri: uri.URI): string {
            return uri.host() + uri.path();
        }

        private getIndexFields(): any[] {
            var parsedURI = new URI(this.url);

            var indexFields = [{
                name: IndexedWebDocument.DocumentField.ID,
                value:  this.id,
                shouldIndex: true,
                shouldStore: true,
                shouldAnalyze: false
            }, {
                name: IndexedWebDocument.DocumentField.TITLE,
                value:  this.title || '',
                shouldIndex: true,
                shouldStore: this.storeTitle,
                shouldAnalyze: true
            }, {
                name: IndexedWebDocument.DocumentField.DOMAIN,
                // Lucene standard analyzer will not tokenize parts
                // of domain name by default. This implies that
                // www.google.com will not match the query `google`.
                // As a workaround we split the domain name into
                // tokens before indexing.
                value: this.tokenizeHost(parsedURI.host()) || '',
                shouldIndex: true,
                shouldStore: false,
                shouldAnalyze: true
            }, {
                name: IndexedWebDocument.DocumentField.PATH,
                value: parsedURI.path() || '',
                shouldIndex: true,
                shouldStore: false,
                shouldAnalyze: true
            }, {
                name: IndexedWebDocument.DocumentField.PROTOCOL,
                value: parsedURI.protocol() || '',
                shouldIndex: true,
                shouldStore: false,
                shouldAnalyze: false
            }, {
                name: IndexedWebDocument.DocumentField.URL,
                value: this.url,
                shouldIndex: true,
                shouldStore: this.storeUrl,
                shouldAnalyze: true
            }, {
                name: IndexedWebDocument.DocumentField.URL_SIMPLIFIED,
                value: this.getSimplifiedUrl(parsedURI),
                shouldIndex: true,
                shouldStore: false,
                shouldAnalyze: true
            }];

            if (!!this.description) {
                indexFields.push({
                    name: IndexedWebDocument.DocumentField.DESCRIPTION,
                    value: this.description,
                    shouldIndex: true,
                    shouldStore: false,
                    shouldAnalyze: true
                });
            }
            if (!!this.text) {
                indexFields.push({
                    name: IndexedWebDocument.DocumentField.TEXT,
                    value: this.text,
                    shouldIndex: true,
                    shouldStore: false,
                    shouldAnalyze: true
                });
            }

            indexFields.forEach(function(field: any){
                field.boost = IndexedWebDocument.getWebDocumentFieldBoost(field.name);
            });

            this.additionalFields.forEach(function(additionalField: any){
                indexFields.push(additionalField);
            });

            return indexFields;
        }

        public getIndexDocument(): any {
            var indexDocument = {};
            indexDocument[IndexedWebDocument.DocumentProperty.FIELDS] = this.getIndexFields();
            indexDocument[IndexedWebDocument.DocumentProperty.BOOST] = this.documentBoost;
            return indexDocument;
        }

        private static getWebDocumentFieldBoost(fieldName: string /*DocumentField*/): number {
            switch (fieldName) {
                case IndexedWebDocument.DocumentField.DESCRIPTION:
                    return 0.5;
                case IndexedWebDocument.DocumentField.URL_SIMPLIFIED:
                    return 8;
                case IndexedWebDocument.DocumentField.TITLE:
                    return 32;
                case IndexedWebDocument.DocumentField.DOMAIN:
                    return 512;
                default:
                    return 1;
            }
        }

        private static getSearchableFields(): string[] {
            return [
                IndexedWebDocument.DocumentField.TITLE,
                IndexedWebDocument.DocumentField.DOMAIN,
                IndexedWebDocument.DocumentField.URL_SIMPLIFIED,
                IndexedWebDocument.DocumentField.DESCRIPTION,
                IndexedWebDocument.DocumentField.TEXT
            ];
        }
    }
}