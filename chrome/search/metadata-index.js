/// <reference path='../lib/ts/URIjs.d.ts'/>
var SearchService;
(function (SearchService) {
    var MetadataDocument = (function () {
        function MetadataDocument(id, url, title) {
            this.id = id;
            var parsedURI = new URI(url);
            this.url = url.toLowerCase();
            this.title = title.toLowerCase();
            this.domain = parsedURI.host().toLowerCase();
            this.port = parsedURI.port().toLowerCase();
            this.path = parsedURI.path().toLowerCase();
            this.query = parsedURI.query().toLowerCase();
            this.hash = parsedURI.fragment().toLowerCase();
            this.protocol = parsedURI.protocol().toLowerCase();
        }
        return MetadataDocument;
    }());
    SearchService.MetadataDocument = MetadataDocument;
    var MetadataIndex = (function () {
        function MetadataIndex() {
            this.documentIdToDocument = {};
        }
        MetadataIndex.prototype.add = function (document) {
            this.documentIdToDocument[document.id] = document;
        };
        MetadataIndex.prototype.remove = function (documentId) {
            delete this.documentIdToDocument[documentId];
        };
        MetadataIndex.prototype.search = function (query) {
            var allDocumentIds = Object.keys(this.documentIdToDocument);
            var docIdToDoc = this.documentIdToDocument;
            var allDocuments = allDocumentIds.map(function (docId) {
                return docIdToDoc[docId];
            });
            query = query.toLowerCase();
            allDocuments.sort(function (docA, docB) {
                for (var i = 0; i < MetadataIndex.DOCUMENT_FIELDS_SORTED_BY_MATCH_PRIORITY.length; ++i) {
                    var fieldName = MetadataIndex.DOCUMENT_FIELDS_SORTED_BY_MATCH_PRIORITY[i];
                    var matchIndexA = docA[fieldName].indexOf(query);
                    var matchIndexB = docB[fieldName].indexOf(query);
                    if (matchIndexA >= 0 && matchIndexB < 0) {
                        return -1;
                    }
                    if (matchIndexA < 0 && matchIndexB >= 0) {
                        return 1;
                    }
                    // the earlier the query matches in the field the better match it is
                    if (matchIndexA >= 0 && matchIndexB >= 0) {
                        return matchIndexA - matchIndexB;
                    }
                    continue;
                }
                return 0;
            });
            var noMatchBeginIndex = 0;
            for (; noMatchBeginIndex < allDocuments.length; ++noMatchBeginIndex) {
                var doc = allDocuments[noMatchBeginIndex];
                if (doc.url.indexOf(query) < 0 && doc.title.indexOf(query) < 0) {
                    break;
                }
            }
            return allDocuments.slice(0, noMatchBeginIndex);
        };
        MetadataIndex.DOCUMENT_FIELDS_SORTED_BY_MATCH_PRIORITY = [
            'domain',
            'title',
            'path',
            'hash',
            'port',
            'protocol',
            // query includes `facebook` a lot of times as source causing
            // unnecessary pages to show up when searching for facebook
            'query',
            'url'
        ];
        return MetadataIndex;
    }());
    SearchService.MetadataIndex = MetadataIndex;
})(SearchService || (SearchService = {}));
//# sourceMappingURL=metadata-index.js.map