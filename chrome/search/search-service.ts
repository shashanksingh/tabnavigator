/// <reference path='../lib/ts/lunr.d.ts'/>
/// <reference path='../common/common.ts'/>
/// <reference path='../tab-suspension-service.ts'/>
/// <reference path='indexed-web-document.ts'/>
/// <reference path='metadata-index.ts'/>


module SearchService {

    import Tab = chrome.tabs.Tab;
    export class TabSearchService {
        private static LIVE_TAB_SEARCH_INDEX_NAME = 'tabs';

        private static instance: TabSearchService = null;

        private logger:Common.Logger = new Common.Logger('tab-search-service');
        private luceneReady: boolean = false;

        // metadata index serves as a backup index in case lucene fails
        private metadataIndex: SearchService.MetadataIndex = null;

        private static Lucene: any = ((<any>window).Lucene);

        constructor() {
            if (TabSearchService.instance) {
                throw new Error('Use TabSearchService.getInstance()');
            }
            this.init();
        }

        public static getInstance(): TabSearchService {
            if (!TabSearchService.instance) {
                TabSearchService.instance = new TabSearchService();
            }
            return TabSearchService.instance;
        }

        private isSuspendedTab(tab: chrome.tabs.Tab): boolean {
            var service = TabNavigator.TabSuspensionService.getInstance();
            return service.isSuspendedTabURL(tab.url);
        }

        private triggerTabDataIndexingRequest(tab: chrome.tabs.Tab) {
            if (this.isSuspendedTab(tab)) {
                return;
            }

            var tabSearchService = this;
            var message = new Common.PostMessage(Common.Commands.GET_PAGE_SEARCHABLE_DATA, null);
            chrome.tabs.sendMessage(tab.id, message, function(response: Common.TabSearchableData) {
                tabSearchService.updateOrAddTabToIndex(tab, response);
            });
        }

        private init() {
            this.metadataIndex = new SearchService.MetadataIndex();

            var tabSearchService = this;
            TabSearchService.Lucene.LuceneJS.openOrCreateIndex(
                TabSearchService.LIVE_TAB_SEARCH_INDEX_NAME,
                TabSearchService.Lucene.LuceneJS.IndexType.IN_MEMORY,
                function(error: Error){
                    if (!!error) {
                        tabSearchService.logger.error('Error in opening live tab index', error);
                        return;
                    }

                    tabSearchService.luceneReady = true;

                    chrome.tabs.query({}, function (tabs:chrome.tabs.Tab[]) {
                        tabs.forEach(function(tab: chrome.tabs.Tab){
                            tabSearchService.updateOrAddTabToIndex(tab);
                            tabSearchService.triggerTabDataIndexingRequest(tab);
                        });
                    });
                }
            );

            chrome.tabs.onUpdated.addListener(function(tabId: number, changeInfo: any, tab: chrome.tabs.Tab) {
                if (tab.status !== Common.TabState.COMPLETE) {
                    return;
                }
                tabSearchService.updateOrAddTabToIndex(tab);
                tabSearchService.triggerTabDataIndexingRequest(tab);
            });
            chrome.tabs.onCreated.addListener(function(tab: chrome.tabs.Tab) {
                if (tab.status !== Common.TabState.COMPLETE) {
                    return;
                }
                tabSearchService.updateOrAddTabToIndex(tab);
                tabSearchService.triggerTabDataIndexingRequest(tab);
            });

            chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
                switch (request.command) {
                    case Common.Commands.GET_PAGE_SEARCHABLE_DATA:
                        var tab: chrome.tabs.Tab = sender.tab;
                        if (!tab) {
                            tabSearchService.logger.error('GET_PAGE_SEARCHABLE_DATA received from non-tab', sender);
                            return;
                        }

                        tabSearchService.updateOrAddTabToIndex(tab, JSON.parse(request.data));
                        return false;
                    default:
                        return false;
                }
            });

            chrome.tabs.onRemoved.addListener(function(tabId: number, removeInfo: any){
                if (!tabSearchService.luceneReady) {
                    tabSearchService.logger.debug('tab removed before lucene was ready, ignored');
                    return;
                }
                tabSearchService.removeTabFromIndex(tabId);
            });
        }

        private updateOrAddTabToIndex(tab: chrome.tabs.Tab, tabData?: Common.TabSearchableData) {
            if (this.isSuspendedTab(tab)) {
                return;
            }

            var metadataDoc = new SearchService.MetadataDocument(tab.id, tab.url, tab.title);
            this.metadataIndex.add(metadataDoc);

            if (!TabSearchService.Lucene.LuceneJS.isInitialized()) {
                return;
            }

            var tabSearchService = this;
            var luceneDocument: any[] = tabSearchService.mapTabToIndexDocument(tab, tabData);
            tabSearchService.logger.debug('updating tab in index', tab.id, tabData, luceneDocument);

            TabSearchService.Lucene.LuceneJS.addOrUpdateDocuments(
                TabSearchService.LIVE_TAB_SEARCH_INDEX_NAME,
                [luceneDocument],
                IndexedWebDocument.DocumentField.ID,
                function(error: Error) {
                    if (!!error) {
                        tabSearchService.logger.error('Error in updating tab in index', error);
                    }
                }
            );
        }

        private removeTabFromIndex(tabId: number, callback?: Common.GenericCallback) {

            this.metadataIndex.remove(tabId + '');

            if (!TabSearchService.Lucene.LuceneJS.isInitialized()) {
                return;
            }

            this.logger.debug('deleting tab from index', tabId);

            var tabSearchService: TabSearchService = this;
            TabSearchService.Lucene.LuceneJS.deleteDocuments(
                TabSearchService.LIVE_TAB_SEARCH_INDEX_NAME,
                [{
                    name: IndexedWebDocument.DocumentField.ID,
                    value: tabId,
                    matchType: TabSearchService.Lucene.LuceneJS.MatchType.EXACT_EQUAL
                }],
                function (error: Error) {
                    if (!!error) {
                        tabSearchService.logger.error('error in deleting tab from index on tab close', error);
                    }

                    if (!!callback) {
                        callback(error);
                    }
                }
            );
        }

        private mapTabToIndexDocument(tab: chrome.tabs.Tab, tabData?: Common.TabSearchableData): any[] {
            var url = tabData ? tabData.url : tab.url;
            var title: string = !!tabData ? tabData.title : tab.title;

            var indexedDocument: IndexedWebDocument = new IndexedWebDocument(tab.id, url, title);
            if (!!tabData) {
                if (!!tabData.description) {
                    indexedDocument.setDescription(tabData.description);
                }
                if (!!tabData.text) {
                    indexedDocument.setText(tabData.text);
                }
            }

            return indexedDocument.getIndexDocument();
        }

        private searchUsingBackupIndex(query: string, callback: Common.GenericCallback) {
            var tabSearchService = this;

            chrome.tabs.query({}, function(allTabs: chrome.tabs.Tab[]) {
                var matchedDocuments = tabSearchService.metadataIndex.search(query);
                var matchingTabIds = matchedDocuments.map(
                    function(metadataDoc: MetadataDocument){
                        return metadataDoc.id;
                    }
                );
                callback(null, Common.Util.removeDuplicates(matchingTabIds));
            });
        }



        public getMatchingTabIds(query: string, callback: Common.GenericCallback): void {
            if (!this.luceneReady) {
                this.searchUsingBackupIndex(query, callback);
                return;
            }

            var luceneQuery = IndexedWebDocument.getSearchFields(query);

            var tabSearchService = this;
            TabSearchService.Lucene.LuceneJS.search(
                TabSearchService.LIVE_TAB_SEARCH_INDEX_NAME,
                luceneQuery,
                [IndexedWebDocument.DocumentField.ID],
                function(error: Error, matches: any[]) {
                    if (!error) {
                        var matchingTabIds = matches.map(function(match){
                            return parseInt(match[IndexedWebDocument.DocumentField.ID]);
                        });

                        if (matchingTabIds.length > 0) {
                            callback(null, Common.Util.removeDuplicates(matchingTabIds));
                            return;
                        }
                    }

                    if (!!error) {
                        tabSearchService.logger.error('Error in searching in tabs, trying backup index', error);
                    }

                    tabSearchService.searchUsingBackupIndex(query, callback);
                }
            );
        }
    }
}