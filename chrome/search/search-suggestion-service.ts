/// <reference path='../common/common.ts'/>

module SearchService {
    export class SearchSuggestionService {
        private static SUGGESTION_API_URL_PATTERN = 'https://api.bing.com/osjson.aspx?query={q}&language={lang}';
        private static DEFAULT_LOCALE = 'en-US';
        private static MAX_CACHE_ENTRIES = 1000;
        private static CACHE_TTL = 86400 * 7 * 1000;
        private static MAX_SUGGESTIONS = 5;

        private static instance: SearchSuggestionService = null;

        private logger:Common.Logger = new Common.Logger('search-suggestion-service');
        private pendingXHR: XMLHttpRequest = null;
        private suggestionsCache: Common.SpatioTemporalBoundedMap<string[]>;

        constructor() {
            if (SearchSuggestionService.instance) {
                throw new Error('Use SearchSuggestionService.getInstance()');
            }
            this.suggestionsCache = new Common.SpatioTemporalBoundedMap<string[]>(
                SearchSuggestionService.MAX_CACHE_ENTRIES,
                SearchSuggestionService.CACHE_TTL
            );
        }

        public static getInstance(): SearchSuggestionService {
            if (!SearchSuggestionService.instance) {
                SearchSuggestionService.instance = new SearchSuggestionService();
            }
            return SearchSuggestionService.instance;
        }

        private static getUserLocale(): string {
            return window.navigator.userLanguage
                || window.navigator.language
                || SearchSuggestionService.DEFAULT_LOCALE;
        }

        private getSuggestionAPIURL(query: string): string {
            return Common.Util.printf(SearchSuggestionService.SUGGESTION_API_URL_PATTERN, {
                q: encodeURIComponent(query),
                lang: encodeURIComponent(SearchSuggestionService.getUserLocale())
            });
        }

        public getSuggestions(query: string, callback: Common.StringArrayCallback) {
            if (!!this.pendingXHR) {
                try {
                    this.pendingXHR.abort();
                } catch (e) {
                    this.logger.error('Ignored error in aborting pending xhr', e);
                }
            }

            var cachedSuggestions = this.suggestionsCache.get(query);
            if (!!cachedSuggestions) {
                callback(cachedSuggestions.slice(0, SearchSuggestionService.MAX_SUGGESTIONS));
                return;
            }

            var xhr = this.pendingXHR = new XMLHttpRequest();
            var url = this.getSuggestionAPIURL(query);

            xhr.open("GET", url, true);
            xhr.responseType = 'json';
            xhr.send();

            var suggestionService = this;

            xhr.onload = function (event) {
                var response = xhr.response;
                var suggestions = response[1];
                if (!suggestions) {
                    callback(null);
                    return;
                }
                suggestionService.suggestionsCache.set(query, suggestions);
                callback(suggestions.slice(0, SearchSuggestionService.MAX_SUGGESTIONS));
            };

            xhr.onabort = function (event) {
                suggestionService.logger.warn('Error in getting suggestions, XHR aborted', event);
                callback(null);
            };

            xhr.onerror = function (event) {
                suggestionService.logger.error('Error in getting suggestions, XHR error', event);
                callback(null);
            };
        }
    }
}