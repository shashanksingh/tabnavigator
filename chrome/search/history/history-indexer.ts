/// <reference path='../../common/common.ts'/>
/// <reference path='../indexed-web-document.ts'/>

module SearchService.History {

    export class HistoryIndexer {
        private static IMPORTED_HISTORY_LAST_INDEX_VERSION_STORAGE_KEY = 'searchservice.history.last_index_version';
        private static HISTORY_INDEX_NAME = 'history';

        private static NewVisitIndexing = {
            // This is up to how long we wait for a visit for a
            // particular url before we process it. This is important
            // because for some website (e.g. docs.google.com) onVisit gets
            // fired a large number of times (~20) in quick succession on a
            // single load. I am not sure why, possibly because the page
            // changes the url hash. We want to bunch all these visit events
            // into one to avoid taking too much CPU. We do this by pushing
            // the history items into a SpatioTemporalBoundedMap keyed by
            // the URL. Once the small TTL expires we add the last history
            // item for the URL in the purge callback.
            PENDING_VISIT_TTL: 1000,
            MAX_PENDING_VISITS: Number.MAX_VALUE
        };

        private static HistoryImport = {
            BATCH_SIZE: 1000,
            // chrome history is known to be unreliable in respecting the
            // time range provided to it. It also does not provide a way
            // to sort the results. To ensure that we don't miss any history
            // items while still not loading too many items in one call we
            // need to ask for data in batch size + time interval pair such
            // that there is a reasonable guarantee that the batch size will
            // always be big enough to cover all the history items for the
            // give time interval for almost every user.
            DEFAULT_BATCH_TIME_INTERVAL: 6 * 60 * 60 * 1000, // 6 hours

            MAX_BATCH_TIME_INTERVAL: 30 * 86400 * 1000, // 30 days

            MIN_BATCH_TIME_INTERVAL: 5 * 60 * 1000, // 5 minutes

            // we only remember history items that were visited at least once in last 30 days
            MAX_HISTORY_TIME_PERIOD: 30 * 86400 * 1000, // 30 days

            // wait between batches to reduce CPU load
            INTER_BATCH_SLEEP_DURATION: 1000
        };

        private static UrlBulkDelete = {
            // if more than these many urls were deleted by the user in one
            // go we'll trigger an index refresh rather than updating index
            // one url at a time
            MAX_INDIVIDUAL_DELETE_THRESHOLD: 1000,
            // we'll update the index for so many urls without
            // any pause (not accounting any pauses introduced
            // by the async nature of these calls). After every
            // so many urls we'll pause for a little while to
            // reduce CPU load
            BULK_DELETE_RELENTLESS_BATCH_SIZE: 50,
            // pause duration between individual url delete
            // batches.
            BULK_DELETE_RELENT_INTERVAL: 3000
        };

        private static Alarms = {
            RefreshHistoryIndex: {
                NAME: 'REFRESH_HISTORY_INDEX',
                INTERVAL: 24 * 60 // 1 day in minutes
            },
            OptimizeHistoryIndex: {
                NAME: 'OPTIMIZE_HISTORY_INDEX',
                INTERVAL: 1 * 60 // 1 hour in minutes
            }
        };

        private static HistoryIndexDocumentField = {
            INDEX_VERSION: 'index_version',
            LAST_VISIT_TIME: 'last_visit_time',
            VISIT_COUNT: 'visit_count',
            TYPED_VISIT_COUNT: 'typed_visit_count'
        };

        private static instance: HistoryIndexer = null;
        private static Lucene: any = (<any>window).Lucene;

        private logger: Common.Logger = new Common.Logger('history-indexer');
        private indexRefreshInProgress: boolean = false;
        private luceneReady: boolean = false;
        private newVisitUrlToHistoryItem: Common.SpatioTemporalBoundedMap<chrome.history.HistoryItem>;

        constructor() {
            if (HistoryIndexer.instance) {
                throw new Error('Use HistoryIndexer.getInstance()');
            }

            var indexer = this;

            this.newVisitUrlToHistoryItem = new Common.SpatioTemporalBoundedMap<chrome.history.HistoryItem>(
                HistoryIndexer.NewVisitIndexing.MAX_PENDING_VISITS,
                HistoryIndexer.NewVisitIndexing.PENDING_VISIT_TTL,
                function(purgedUrl: string, purgedHistoryItem: chrome.history.HistoryItem) {
                    indexer.updateMetadataOnHistoryItem(purgedHistoryItem, function(error: Error){
                        // even if there was an error in updating metadata we still index
                        // whatever info we have
                        if (!!error) {
                            indexer.logger.warn(
                                'Error in attempt to update metadata of history item before indexing',
                                error,
                                purgedHistoryItem.url,
                                purgedHistoryItem
                            );
                        }

                        indexer.updateHistoryItemInIndex(purgedHistoryItem, function(error: Error){
                            if (!!error) {
                                indexer.logger.error(
                                    'Error in updating history item in index on `onVisit` event',
                                    purgedHistoryItem,
                                    error
                                );
                            } else {
                                indexer.logger.log(
                                    'Successfully updated history item in index on `onVisit` event',
                                    purgedHistoryItem
                                );
                            }
                        });
                    });
                }
            );
        }

        public static getInstance(): HistoryIndexer {
            if (!HistoryIndexer.instance) {
                HistoryIndexer.instance = new HistoryIndexer();
            }
            return HistoryIndexer.instance;
        }

        public initialize(): void {
            this.setUpIncrementalIndexUpdate();
            this.setUpPeriodicIndexRefresh();
            this.setUpPeriodicIndexOptimization();
        }

        public isIndexRefreshInProgress(): boolean {
            return this.indexRefreshInProgress;
        }

        public getMatchingHistoryItems(query: string, callback: Common.GenericCallback): void {
            if (!this.luceneReady) {
                this.logger.warn('History indexer unable to serve searches because lucene is not ready yet');
                callback(null, []);
                return;
            }

            var luceneQuery = IndexedWebDocument.getSearchFields(query);

            var indexer = this;
            HistoryIndexer.Lucene.LuceneJS.search(
                HistoryIndexer.HISTORY_INDEX_NAME,
                luceneQuery,
                indexer.getHistoryItemQueryOutputFields(),
                function(error: Error, matches: any[]) {
                    if (!!error) {
                        indexer.logger.error('Error in searching in browser history', error);
                        callback(error);
                        return;
                    }

                    var matchingHistoryItems = matches.map(function(match){
                        return indexer.mapIndexDocumentToHistoryItem(match);
                    });

                    matchingHistoryItems = Common.Util.removeDuplicates(
                        matchingHistoryItems,
                        function(historyItem: chrome.history.HistoryItem) {
                            return historyItem.url;
                        }
                    );
                    callback(null, matchingHistoryItems);
                }
            );
        }

        private getLastSuccessfulIndexVersion(): number {
            var storedValue = localStorage[HistoryIndexer.IMPORTED_HISTORY_LAST_INDEX_VERSION_STORAGE_KEY];
            if (storedValue === void 0) {
                storedValue = 0;
            }
            return parseInt(storedValue);
        }

        private incrementLastSuccessfulIndexVersion(): void {
            var key = HistoryIndexer.IMPORTED_HISTORY_LAST_INDEX_VERSION_STORAGE_KEY;
            localStorage[key] = this.getLastSuccessfulIndexVersion() + 1;
        }

        private setUpIncrementalIndexUpdate() {
            var indexer = this;

            // TODO: handle race conditions when history items are added or removed
            // while index refresh is in progress.

            chrome.history.onVisited.addListener(function(historyItem: chrome.history.HistoryItem){
                indexer.newVisitUrlToHistoryItem.set(historyItem.url, historyItem);
            });

            chrome.tabs.onUpdated.addListener(function(tabId: number, changeInfo: any, tab: chrome.tabs.Tab){
                if (tab.status !== 'complete' || !tab.title) {
                    return;
                }

                var pendingHistoryItem: chrome.history.HistoryItem
                    = indexer.newVisitUrlToHistoryItem.get(tab.url);

                // if there is a history item waiting for indexing we'll just
                // piggy back on it

                // TODO: what if the next onVisited overwrite the metadata (title etc.)
                // that we update here?
                if (!!pendingHistoryItem) {
                    pendingHistoryItem.title = tab.title;
                    return;
                }

                indexer.updateHistoryItemForUrl(tab.url, tab.title, function(error: Error){
                    if (!!error) {
                        indexer.logger.error(
                            'Error in updating indexed history item for url on tab update',
                            error,
                            tab.url,
                            tab.title
                        );
                    } else {
                        indexer.logger.log(
                            'Successfully updated indexed history item for url on tab update',
                            tab.url,
                            tab.title
                        );
                    }
                });
            });

            chrome.history.onVisitRemoved.addListener(function(removed: chrome.history.RemovedResult){
                if (removed.allHistory) {
                    indexer.clearIndex(function(error: Error){
                        if (!!error) {
                            indexer.logger.error(
                                'Error in clearing history index on all history removal by the user',
                                error
                            );
                        } else {
                            indexer.logger.log(
                                'Successfully cleared history index on all history removal by the user'
                            );
                        }
                    });
                    return;
                }

                var uniqueUrls: string[] = [];
                var urlToDeletedVisitsCount: {[url: string]: number} = {};
                removed.urls.forEach(function(removedUrl: string){
                    var urlCount = urlToDeletedVisitsCount[removedUrl];
                    if (urlCount === void 0) {
                        urlCount = 0;
                        uniqueUrls.push(removedUrl);
                    }
                    urlCount++;
                    urlToDeletedVisitsCount[removedUrl] = urlCount;
                });

                // too many urls removed, refreshing the entire index
                // instead of updating history items in the index one
                // by one
                if (uniqueUrls.length > HistoryIndexer.UrlBulkDelete.MAX_INDIVIDUAL_DELETE_THRESHOLD) {
                    indexer.refreshIndex(function(error: Error){
                        if (!!error) {
                            indexer.logger.error(
                                'Error in refreshing index after bulk delete of visits by the user',
                                error
                            );
                        } else {
                            indexer.logger.log(
                                'Successfully refreshed index after bulk delete of visits by the user',
                                error
                            );
                        }
                    });
                    return;
                }

                indexer.deleteUrlVisitsFromIndex(
                    uniqueUrls,
                    urlToDeletedVisitsCount,
                    function(error: Error){
                        if (!!error) {
                            indexer.logger.error(
                                'Error in clearing history index on select url visit removal',
                                error
                            );
                        }
                    }
                );
            });
        }

        private setUpPeriodicIndexRefresh() {
            var indexer = this;

            Common.Util.setPeriodicAlarm(
                HistoryIndexer.Alarms.RefreshHistoryIndex.NAME,
                HistoryIndexer.Alarms.RefreshHistoryIndex.INTERVAL,
                function (alarm: chrome.alarms.Alarm) {
                    indexer.refreshIndex(function(error: Error){
                        if (!!error) {
                            indexer.logger.error('Error in refreshing index on alarm', error);
                        } else {
                            indexer.logger.log('Successfully refreshed index on alarm');
                        }
                    });
                }
            );

            // trigger the first refresh when the extension starts
            indexer.refreshIndex(function(error: Error){
                if (!!error) {
                    indexer.logger.error('Error in refreshing index on init', error);
                } else {
                    indexer.logger.log('Successfully refreshed index on init');
                }
            });
        }

        private setUpPeriodicIndexOptimization() {
            var indexer = this;

            Common.Util.setPeriodicAlarm(
                HistoryIndexer.Alarms.OptimizeHistoryIndex.NAME,
                HistoryIndexer.Alarms.OptimizeHistoryIndex.INTERVAL,
                function(alarm: chrome.alarms.Alarm) {
                    HistoryIndexer.Lucene.LuceneJS.optimizeIndex(
                        HistoryIndexer.HISTORY_INDEX_NAME,
                        function(error: Error) {
                            if (!!error) {
                                indexer.logger.error('Error in refreshing index on alarm', error);
                            } else {
                                indexer.logger.log('Successfully refreshed index on alarm');
                            }
                        }
                    );
                }
            );

        }

        private refreshIndex(callback: Common.GenericCallback) {
            if (this.indexRefreshInProgress) {
                this.logger.warn('index refresh already in progress, ignoring request to start another');
                return;
            }
            this.indexRefreshInProgress = true;

            var historyIndexRefreshTelemetryId = Common.Telemeter.getInstance().start('history-index-refresh');

            var indexer = this;
            this.ensureLuceneReady(function(error: Error){
                if (!!error) {
                    Common.Telemeter.getInstance().end(historyIndexRefreshTelemetryId);

                    indexer.indexRefreshInProgress = false;
                    callback(error);
                    return;
                }

                indexer.indexAllHistory(function(error: Error){
                    Common.Telemeter.getInstance().end(historyIndexRefreshTelemetryId);
                    indexer.indexRefreshInProgress = false;
                    if (!!error) {
                        callback(error);
                        return;
                    }

                    // Lucene doesn't have a way to update a document
                    // besides delete+create. Updating a lot of history
                    // items will be very costly this way as we'll have
                    // to identify them all by the individual unique
                    // IDs. As a work around we keep versioned indices.
                    // At regular interval we re-import the entire history
                    // that we want to index and drop the older version
                    // once we have successfully finished importing the
                    // new version. This allows us to not worry about purging
                    // older items from the index and guarantees fewer issues
                    // around missing updating any history items. We do still
                    // keep updating history items as history item visit events
                    // fire as the user visits web pages. This keeps the index
                    // up to date between the bulk index refresh and is not
                    // costly as delete+create is done one history item at a time.
                    indexer.incrementLastSuccessfulIndexVersion();
                    indexer.deleteOlderVersionsFromIndex(function(error: Error){
                        if (!!error) {
                            indexer.logger.error(
                                'Error in deleting older version of history after index refresh',
                                error
                            );
                            callback(error);
                            return;
                        }

                        HistoryIndexer.Lucene.LuceneJS.optimizeIndex(
                            HistoryIndexer.HISTORY_INDEX_NAME,
                            callback
                        );
                    });
                });
            });
        }

        private ensureLuceneReady(callback: Common.GenericCallback) {

            if (this.luceneReady) {
                callback(null);
                return;
            }

            var indexer = this;
            HistoryIndexer.Lucene.LuceneJS.openOrCreateIndex(
                HistoryIndexer.HISTORY_INDEX_NAME,
                HistoryIndexer.Lucene.LuceneJS.IndexType.PERSISTENT,
                function(error: Error){
                    if (!!error) {
                        indexer.logger.error('Error in opening history index', error);
                    }
                    indexer.luceneReady = !error;
                    callback(error);
                }
            );
        }

        private indexAllHistory(callback: Common.GenericCallback,
                                batchTimeInterval?: number,
                                importTimeRangeStart?: number,
                                historyItemsPendingIndexing?: chrome.history.HistoryItem[],
                                processedHistoryItemIds?: {[historyItemId: string]: boolean}) {

            if (batchTimeInterval === void 0) {
                batchTimeInterval = HistoryIndexer.HistoryImport.DEFAULT_BATCH_TIME_INTERVAL;
            }

            if (importTimeRangeStart === void 0) {
                importTimeRangeStart = Math.max(
                    0,
                    new Date().getTime() - batchTimeInterval
                );
            }

            if (!historyItemsPendingIndexing) {
                historyItemsPendingIndexing = [];
            }

            if (!processedHistoryItemIds) {
                processedHistoryItemIds = {};
            }

            // we have exhausted the entire time interval, index any pending
            // items and exit the import cycle
            if (importTimeRangeStart <= new Date().getTime() - HistoryIndexer.HistoryImport.MAX_HISTORY_TIME_PERIOD) {
                this.addHistoryItemsToIndex(historyItemsPendingIndexing, callback);
                return;
            }

            var importTimeRangeEnd = importTimeRangeStart + batchTimeInterval;
            var indexer = this;

            chrome.history.search(
                {
                    text: '',
                    startTime: importTimeRangeStart,
                    endTime: importTimeRangeEnd,
                    maxResults: HistoryIndexer.HistoryImport.BATCH_SIZE
                },
                function(historyResults: chrome.history.HistoryItem[]) {
                    if (historyResults.length >= HistoryIndexer.HistoryImport.BATCH_SIZE) {
                        // the time interval was too big to fit all the history in one batch
                        // of data, try with a smaller time interval and the same starting point
                        if (batchTimeInterval > HistoryIndexer.HistoryImport.MIN_BATCH_TIME_INTERVAL) {
                            batchTimeInterval = Math.max(
                                batchTimeInterval/2,
                                HistoryIndexer.HistoryImport.MIN_BATCH_TIME_INTERVAL
                            );

                            setTimeout(function(){
                                indexer.indexAllHistory(
                                    callback,
                                    batchTimeInterval,
                                    importTimeRangeStart,
                                    historyItemsPendingIndexing,
                                    processedHistoryItemIds
                                );
                            }, HistoryIndexer.HistoryImport.INTER_BATCH_SLEEP_DURATION);

                            return;
                        } else {
                            // we already tried with the minimum time interval, there
                            // are way too many history items in this small time interval
                            // we give up, take the batch the we have and ignore the rest
                            indexer.logger.warn('Too many history items even in the smallest batch interval');
                        }
                    } else if (historyResults.length === 0) {
                        // there were no history items in this time window. increase
                        // the size of the window we are looking for going forward to
                        // avoid too many small request in case the user does not have
                        // any history for a long period of time or in case we have
                        // already seen the oldest item in the user's history.
                        // Note that we still move the time window as there is
                        // no point in looking in this time window.
                        batchTimeInterval = Math.min(
                            2 * batchTimeInterval,
                            HistoryIndexer.HistoryImport.MAX_BATCH_TIME_INTERVAL
                        );

                        setTimeout(function(){
                            indexer.indexAllHistory(
                                callback,
                                batchTimeInterval,
                                importTimeRangeStart - batchTimeInterval,
                                historyItemsPendingIndexing,
                                processedHistoryItemIds
                            );
                        }, HistoryIndexer.HistoryImport.INTER_BATCH_SLEEP_DURATION);

                        return;
                    }

                    historyResults.forEach(function(historyItem: chrome.history.HistoryItem){
                        // we have already seen this history item in this import cycle
                        // and will not get any new info by processing it now.
                        if (processedHistoryItemIds.hasOwnProperty(historyItem.id)) {
                            return;
                        }
                        processedHistoryItemIds[historyItem.id] = true;
                        historyItemsPendingIndexing.push(historyItem);
                    });

                    if (historyItemsPendingIndexing.length >= HistoryIndexer.HistoryImport.BATCH_SIZE) {
                        indexer.addHistoryItemsToIndex(historyItemsPendingIndexing, function(error: Error){
                            if (!!error) {
                                callback(error);
                                return;
                            }

                            setTimeout(function(){
                                // the current batch of pending items is processed
                                // we start with the next time range, with an empty
                                // list of pending items but keep the list of seem
                                // items as all history items have the same info
                                // regardless of which time-range query they are
                                // returned by chrome in response to.
                                indexer.indexAllHistory(
                                    callback,
                                    batchTimeInterval,
                                    importTimeRangeStart - batchTimeInterval,
                                    [],
                                    processedHistoryItemIds
                                );
                            }, HistoryIndexer.HistoryImport.INTER_BATCH_SLEEP_DURATION);
                        });
                    } else {
                        // there are not enough items accumulated, keep collecting
                        setTimeout(function(){
                            indexer.indexAllHistory(
                                callback,
                                batchTimeInterval,
                                importTimeRangeStart - batchTimeInterval,
                                historyItemsPendingIndexing,
                                processedHistoryItemIds
                            );
                        }, HistoryIndexer.HistoryImport.INTER_BATCH_SLEEP_DURATION);
                    }
                }
            );
        }

        private clearIndex(callback: Common.GenericCallback) {
            this.deleteVersionsFromIndex(9999999999, callback);
        }

        private deleteOlderVersionsFromIndex(callback: Common.GenericCallback) {
            var lastVersion: number = this.getLastSuccessfulIndexVersion();
            if (lastVersion <= 0) {
                callback(null);
                return;
            }
            this.deleteVersionsFromIndex(lastVersion - 1, callback);
        }

        private deleteVersionsFromIndex(newestVersionToDelete: number, callback: Common.GenericCallback) {
            var indexer= this;
            HistoryIndexer.Lucene.LuceneJS.deleteDocuments(
                HistoryIndexer.HISTORY_INDEX_NAME,
                [{
                    name: HistoryIndexer.HistoryIndexDocumentField.INDEX_VERSION,
                    value: [0, newestVersionToDelete],
                    matchType: HistoryIndexer.Lucene.LuceneJS.MatchType.RANGE
                }],
                function (error: Error) {
                    if (!!error) {
                        indexer.logger.error('error in deleting older versions of history index', error);
                    }
                    callback(error);
                }
            );
        }

        private addHistoryItemsToIndex(historyItems: chrome.history.HistoryItem[],
                                       callback: Common.GenericCallback) {

            if (historyItems.length === 0) {
                callback(null);
                return;
            }

            var indexer = this;

            var lastIndexVersion = this.getLastSuccessfulIndexVersion();
            var indexVersion = this.indexRefreshInProgress ? lastIndexVersion + 1 : lastIndexVersion;
            var indexDocuments = historyItems.map(function(historyItem: chrome.history.HistoryItem){
                return indexer.mapHistoryItemToIndexDocument(historyItem, indexVersion);
            });


            this.logger.log('Indexing history items', indexDocuments.length, indexDocuments);

            HistoryIndexer.Lucene.LuceneJS.addDocumentsToIndex(
                HistoryIndexer.HISTORY_INDEX_NAME,
                indexDocuments,
                callback
            );
        }

        private updateHistoryItemInIndex(historyItem: chrome.history.HistoryItem,
                                         callback: Common.GenericCallback): void {

            var indexer = this;
            this.deleteHistoryItemFromIndex(historyItem, function(error){
                if (!!error) {
                    indexer.logger.error('Error in deleting history item from index while updating item', error);
                    callback(error);
                    return;
                }
                indexer.addHistoryItemsToIndex([historyItem], callback);
            });
        }

        private deleteHistoryItemFromIndex(historyItem: chrome.history.HistoryItem,
                                           callback: Common.GenericCallback): void {

            var indexer = this;
            HistoryIndexer.Lucene.LuceneJS.deleteDocuments(
                HistoryIndexer.HISTORY_INDEX_NAME,
                [{
                    name: IndexedWebDocument.DocumentField.ID,
                    value: historyItem.id,
                    matchType: HistoryIndexer.Lucene.LuceneJS.MatchType.EXACT_EQUAL
                }],
                function (error: Error) {
                    if (!!error) {
                        indexer.logger.error('Error in deleting history item from index', error);
                    }
                    callback(error);
                }
            );
        }

        private deleteUrlVisitsFromIndex(urls: string[],
                                         urlToDeletedVisitsMap: {[url: string]: number},
                                         callback: Common.GenericCallback): void {

            if (urls.length === 0) {
                callback(null);
                return;
            }

            var urlToDelete: string = urls.shift();
            var numDeletedVisits = urlToDeletedVisitsMap[urlToDelete];
            delete urlToDeletedVisitsMap[urlToDelete];

            var indexer = this;
            this.deleteIndividualUrlVisitsFromIndex(
                urlToDelete,
                numDeletedVisits,
                function(error: Error){
                    // ignore error
                    var batchSize = HistoryIndexer.UrlBulkDelete.BULK_DELETE_RELENTLESS_BATCH_SIZE;
                    var pauseInterval = urls.length % batchSize === 0
                        ? HistoryIndexer.UrlBulkDelete.BULK_DELETE_RELENT_INTERVAL : 0;

                    // wait for at least the next event loop after every url is updated
                    setTimeout(function(){
                        indexer.deleteUrlVisitsFromIndex(urls, urlToDeletedVisitsMap, callback);
                    }, pauseInterval);
                }
            );
        }

        private deleteIndividualUrlVisitsFromIndex(url: string,
                                                   numVisitsDeleted: number,
                                                   callback: Common.GenericCallback): void {
            var indexer = this;

            this.getHistoryItemForUrl(url, function(error: Error, historyItem: chrome.history.HistoryItem){
                if (!!error) {
                    callback(error);
                    return;
                }

                if (!historyItem) {
                    callback(null);
                    return;
                }

                indexer.deleteHistoryItemFromIndex(historyItem, function(error: Error) {
                    if (!!error) {
                        callback(error);
                        return;
                    }

                    // add the updated history item back to the index if there are any
                    // more visits left, in case the user removed all the visits of the
                    // item from the browser history this will effectively delete the item
                    // from our index too. note that this update does not always correctly
                    // update the typed visit count and the last visit time
                    historyItem.visitCount -= numVisitsDeleted;
                    if (historyItem.visitCount <= 0) {
                        callback(null);
                        return;
                    }

                    // this is our best guess of the updated typed count
                    historyItem.typedCount = Math.min(historyItem.typedCount, historyItem.visitCount);
                    indexer.addHistoryItemsToIndex([historyItem], callback);
                });
            });
        }

        private getHistoryItemForUrl(url: string, callback: Common.GenericCallback): void {
            var indexer = this;

            HistoryIndexer.Lucene.LuceneJS.search(
                HistoryIndexer.HISTORY_INDEX_NAME,
                [{
                    name: IndexedWebDocument.DocumentField.URL,
                    value: url,
                    matchType: HistoryIndexer.Lucene.LuceneJS.MatchType.EXACT_EQUAL
                }],
                indexer.getHistoryItemQueryOutputFields(),
                function (error: Error, matches: any[]) {
                    if (!!error) {
                        indexer.logger.error('Error in getting history item from index', url, error);
                        callback(error);
                        return;
                    }

                    if (matches.length === 0) {
                        indexer.logger.warn('No matching history item found for url', url);
                        callback(null, null);
                        return;
                    }

                    if (matches.length > 1) {
                        indexer.logger.warn(
                            'More than one history items found for url',
                            url,
                            matches.length,
                            matches
                        );
                   }


                    var latestVersion = indexer.getLastSuccessfulIndexVersion();
                    var match = Common.Util.findInArray(matches, function(match){
                        var docVersion = parseInt(match[HistoryIndexer.HistoryIndexDocumentField.INDEX_VERSION]);
                        return docVersion === latestVersion;
                    });
                    if (!match) {
                        indexer.logger.warn(
                            'No history item found for url with the latest version',
                            url,
                            matches.length,
                            matches
                        );
                        callback(null, null);
                        return;
                    }

                    callback(null, indexer.mapIndexDocumentToHistoryItem(match));
                }
            );
        }

        private updateHistoryItemForUrl(url: string, title: string, callback: Common.GenericCallback) {

            if (!title) {
                callback(null);
                return;
            }

            var indexer = this;

            indexer.getHistoryItemForUrl(
                url,
                function(error: Error, historyItem: chrome.history.HistoryItem) {
                    if (!!error) {
                        indexer.logger.error(
                            'updateHistoryItemForUrl: Error in getting history item for url',
                            error,
                            url
                        );
                        callback(error);
                        return;
                    }

                    if (!historyItem) {
                        indexer.logger.warn(
                            'updateHistoryItemForUrl:: No history item found for url',
                            error,
                            url
                        );
                        callback(null);
                        return;
                    }

                    if (historyItem.title === title) {
                        callback(null);
                        return;
                    }

                    historyItem.title = title;
                    indexer.updateHistoryItemInIndex(
                        historyItem,
                        function(error: Error) {
                            if (!!error) {
                                indexer.logger.error(
                                    'updateHistoryItemForUrl: Error in updating history metadata',
                                    error,
                                    url
                                );
                            }
                            callback(error);
                        }
                    );
                }
            );
        }

        private updateMetadataOnHistoryItem(historyItem: chrome.history.HistoryItem,
                                            callback: Common.GenericCallback) {

            // TODO: fetch other metadata (text, description etc.) as well
            if (!!historyItem.title) {
                callback(null);
                return;
            }

            var indexer = this;
            chrome.tabs.query({url: historyItem.url}, function(matchingTabs: chrome.tabs.Tab[]){
                if (matchingTabs.length === 0) {
                    var error = new Error('No matching tab found for history item with url ' + historyItem.url);
                    callback(error);
                    return;
                }

                if (matchingTabs.length > 1) {
                    indexer.logger.log(
                        'More than one tabs found with history item url, using last',
                        historyItem.url,
                        historyItem
                    );
                    return;
                }

                var matchingTab = matchingTabs[matchingTabs.length - 1];
                if (!!matchingTab.title) {
                    historyItem.title = matchingTab.title;
                }
                callback(null);
            });
        }

        private getHistoryItemQueryOutputFields(): string[] {
            return [
                IndexedWebDocument.DocumentField.ID,
                IndexedWebDocument.DocumentField.URL,
                IndexedWebDocument.DocumentField.TITLE,
                IndexedWebDocument.DocumentField.DESCRIPTION,
                IndexedWebDocument.DocumentField.TEXT,
                HistoryIndexer.HistoryIndexDocumentField.VISIT_COUNT,
                HistoryIndexer.HistoryIndexDocumentField.TYPED_VISIT_COUNT,
                HistoryIndexer.HistoryIndexDocumentField.LAST_VISIT_TIME
            ];
        }

        private mapIndexDocumentToHistoryItem(indexDocument: any): chrome.history.HistoryItem {
            return {
                id: indexDocument[IndexedWebDocument.DocumentField.ID],
                url: indexDocument[IndexedWebDocument.DocumentField.URL],
                title: indexDocument[IndexedWebDocument.DocumentField.TITLE],
                visitCount: parseInt(indexDocument[HistoryIndexer.HistoryIndexDocumentField.VISIT_COUNT] || 0),
                typedCount: parseInt(indexDocument[HistoryIndexer.HistoryIndexDocumentField.TYPED_VISIT_COUNT] || 0),
                lastVisitTime: parseInt(indexDocument[HistoryIndexer.HistoryIndexDocumentField.LAST_VISIT_TIME] || 0)
            };
        }

        private mapHistoryItemToIndexDocument(historyItem: chrome.history.HistoryItem, indexVersion: number): any[] {


            var indexedDocument: IndexedWebDocument = new IndexedWebDocument(
                historyItem.id,
                historyItem.url,
                historyItem.title
            );

            indexedDocument.addField(
                HistoryIndexer.HistoryIndexDocumentField.INDEX_VERSION,
                indexVersion,
                true,
                false,
                false
            );
            indexedDocument.addField(
                HistoryIndexer.HistoryIndexDocumentField.LAST_VISIT_TIME,
                historyItem.lastVisitTime,
                true,
                false,
                false
            );
            indexedDocument.addField(
                HistoryIndexer.HistoryIndexDocumentField.VISIT_COUNT,
                historyItem.visitCount,
                true,
                true, // we need to store this to be able to delete individual visits
                false
            );
            indexedDocument.addField(
                HistoryIndexer.HistoryIndexDocumentField.TYPED_VISIT_COUNT,
                historyItem.typedCount,
                true,
                false,
                false
            );

            // arbitrarily chosen formula for boost
            var documentBoost = (historyItem.visitCount || 0) + (historyItem.typedCount || 0);
            indexedDocument.setDocumentBoost(documentBoost);

            indexedDocument.setStoreUrl(true);
            indexedDocument.setStoreTitle(true);

            return indexedDocument.getIndexDocument();
        }
    }
}