/// <reference path='../lib/ts/URIjs.d.ts'/>

module SearchService {
    export class MetadataDocument {
        public url: string;
        public title: string;
        public domain: string;
        public port: string;
        public path: string;
        public query: string;
        public hash: string;
        public protocol: string;


        constructor(public id: any, url: string, title: string) {
            var parsedURI = new URI(url);
            this.url = url.toLowerCase();
            this.title = title.toLowerCase();
            this.domain = parsedURI.host().toLowerCase();
            this.port = parsedURI.port().toLowerCase();
            this.path = parsedURI.path().toLowerCase();
            this.query = parsedURI.query().toLowerCase();
            this.hash = parsedURI.fragment().toLowerCase();
            this.protocol = parsedURI.protocol().toLowerCase();
        }
    }

    export class MetadataIndex {
        private static DOCUMENT_FIELDS_SORTED_BY_MATCH_PRIORITY = [
            'domain',
            'title',
            'path',
            'hash',
            'port',
            'protocol',
            // query includes `facebook` a lot of times as source causing
            // unnecessary pages to show up when searching for facebook
            'query',
            'url'
        ];
        private documentIdToDocument: {[documentId: string]: MetadataDocument} = {};

        public add(document: MetadataDocument) {
            this.documentIdToDocument[document.id] = document;
        }

        public remove(documentId: string) {
            delete this.documentIdToDocument[documentId];
        }

        public search(query: string): MetadataDocument[] {
            var allDocumentIds: string[] = Object.keys(this.documentIdToDocument);
            var docIdToDoc = this.documentIdToDocument;
            var allDocuments: MetadataDocument[] = allDocumentIds.map(function(docId: string){
                return docIdToDoc[docId];
            });

            query = query.toLowerCase();

            allDocuments.sort(function(docA: MetadataDocument, docB: MetadataDocument){

                for (var i=0; i< MetadataIndex.DOCUMENT_FIELDS_SORTED_BY_MATCH_PRIORITY.length; ++i) {
                    var fieldName = MetadataIndex.DOCUMENT_FIELDS_SORTED_BY_MATCH_PRIORITY[i];

                    var matchIndexA: number = docA[fieldName].indexOf(query);
                    var matchIndexB: number = docB[fieldName].indexOf(query);

                    if (matchIndexA >= 0 && matchIndexB < 0) {
                        return -1;
                    }
                    if (matchIndexA < 0 && matchIndexB >= 0) {
                        return 1;
                    }

                    // the earlier the query matches in the field the better match it is
                    if (matchIndexA >= 0 && matchIndexB >= 0) {
                        return matchIndexA - matchIndexB;
                    }
                    continue;
                }

                return 0;
            });

            var noMatchBeginIndex = 0;
            for (; noMatchBeginIndex<allDocuments.length; ++noMatchBeginIndex) {
                var doc = allDocuments[noMatchBeginIndex];
                if (doc.url.indexOf(query) < 0 && doc.title.indexOf(query) < 0) {
                    break;
                }
            }

            return allDocuments.slice(0, noMatchBeginIndex);
        }
    }
}