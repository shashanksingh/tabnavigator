/// <reference path='../lib/ts/lunr.d.ts'/>
/// <reference path='../common/common.ts'/>
/// <reference path='../tab-suspension-service.ts'/>
/// <reference path='indexed-web-document.ts'/>
/// <reference path='metadata-index.ts'/>
var SearchService;
(function (SearchService) {
    var TabSearchService = (function () {
        function TabSearchService() {
            this.logger = new Common.Logger('tab-search-service');
            this.luceneReady = false;
            // metadata index serves as a backup index in case lucene fails
            this.metadataIndex = null;
            if (TabSearchService.instance) {
                throw new Error('Use TabSearchService.getInstance()');
            }
            this.init();
        }
        TabSearchService.getInstance = function () {
            if (!TabSearchService.instance) {
                TabSearchService.instance = new TabSearchService();
            }
            return TabSearchService.instance;
        };
        TabSearchService.prototype.isSuspendedTab = function (tab) {
            var service = TabNavigator.TabSuspensionService.getInstance();
            return service.isSuspendedTabURL(tab.url);
        };
        TabSearchService.prototype.triggerTabDataIndexingRequest = function (tab) {
            if (this.isSuspendedTab(tab)) {
                return;
            }
            var tabSearchService = this;
            var message = new Common.PostMessage(Common.Commands.GET_PAGE_SEARCHABLE_DATA, null);
            chrome.tabs.sendMessage(tab.id, message, function (response) {
                tabSearchService.updateOrAddTabToIndex(tab, response);
            });
        };
        TabSearchService.prototype.init = function () {
            this.metadataIndex = new SearchService.MetadataIndex();
            var tabSearchService = this;
            TabSearchService.Lucene.LuceneJS.openOrCreateIndex(TabSearchService.LIVE_TAB_SEARCH_INDEX_NAME, TabSearchService.Lucene.LuceneJS.IndexType.IN_MEMORY, function (error) {
                if (!!error) {
                    tabSearchService.logger.error('Error in opening live tab index', error);
                    return;
                }
                tabSearchService.luceneReady = true;
                chrome.tabs.query({}, function (tabs) {
                    tabs.forEach(function (tab) {
                        tabSearchService.updateOrAddTabToIndex(tab);
                        tabSearchService.triggerTabDataIndexingRequest(tab);
                    });
                });
            });
            chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
                if (tab.status !== Common.TabState.COMPLETE) {
                    return;
                }
                tabSearchService.updateOrAddTabToIndex(tab);
                tabSearchService.triggerTabDataIndexingRequest(tab);
            });
            chrome.tabs.onCreated.addListener(function (tab) {
                if (tab.status !== Common.TabState.COMPLETE) {
                    return;
                }
                tabSearchService.updateOrAddTabToIndex(tab);
                tabSearchService.triggerTabDataIndexingRequest(tab);
            });
            chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
                switch (request.command) {
                    case Common.Commands.GET_PAGE_SEARCHABLE_DATA:
                        var tab = sender.tab;
                        if (!tab) {
                            tabSearchService.logger.error('GET_PAGE_SEARCHABLE_DATA received from non-tab', sender);
                            return;
                        }
                        tabSearchService.updateOrAddTabToIndex(tab, JSON.parse(request.data));
                        return false;
                    default:
                        return false;
                }
            });
            chrome.tabs.onRemoved.addListener(function (tabId, removeInfo) {
                if (!tabSearchService.luceneReady) {
                    tabSearchService.logger.debug('tab removed before lucene was ready, ignored');
                    return;
                }
                tabSearchService.removeTabFromIndex(tabId);
            });
        };
        TabSearchService.prototype.updateOrAddTabToIndex = function (tab, tabData) {
            if (this.isSuspendedTab(tab)) {
                return;
            }
            var metadataDoc = new SearchService.MetadataDocument(tab.id, tab.url, tab.title);
            this.metadataIndex.add(metadataDoc);
            if (!TabSearchService.Lucene.LuceneJS.isInitialized()) {
                return;
            }
            var tabSearchService = this;
            var luceneDocument = tabSearchService.mapTabToIndexDocument(tab, tabData);
            tabSearchService.logger.debug('updating tab in index', tab.id, tabData, luceneDocument);
            TabSearchService.Lucene.LuceneJS.addOrUpdateDocuments(TabSearchService.LIVE_TAB_SEARCH_INDEX_NAME, [luceneDocument], SearchService.IndexedWebDocument.DocumentField.ID, function (error) {
                if (!!error) {
                    tabSearchService.logger.error('Error in updating tab in index', error);
                }
            });
        };
        TabSearchService.prototype.removeTabFromIndex = function (tabId, callback) {
            this.metadataIndex.remove(tabId + '');
            if (!TabSearchService.Lucene.LuceneJS.isInitialized()) {
                return;
            }
            this.logger.debug('deleting tab from index', tabId);
            var tabSearchService = this;
            TabSearchService.Lucene.LuceneJS.deleteDocuments(TabSearchService.LIVE_TAB_SEARCH_INDEX_NAME, [{
                    name: SearchService.IndexedWebDocument.DocumentField.ID,
                    value: tabId,
                    matchType: TabSearchService.Lucene.LuceneJS.MatchType.EXACT_EQUAL
                }], function (error) {
                if (!!error) {
                    tabSearchService.logger.error('error in deleting tab from index on tab close', error);
                }
                if (!!callback) {
                    callback(error);
                }
            });
        };
        TabSearchService.prototype.mapTabToIndexDocument = function (tab, tabData) {
            var url = tabData ? tabData.url : tab.url;
            var title = !!tabData ? tabData.title : tab.title;
            var indexedDocument = new SearchService.IndexedWebDocument(tab.id, url, title);
            if (!!tabData) {
                if (!!tabData.description) {
                    indexedDocument.setDescription(tabData.description);
                }
                if (!!tabData.text) {
                    indexedDocument.setText(tabData.text);
                }
            }
            return indexedDocument.getIndexDocument();
        };
        TabSearchService.prototype.searchUsingBackupIndex = function (query, callback) {
            var tabSearchService = this;
            chrome.tabs.query({}, function (allTabs) {
                var matchedDocuments = tabSearchService.metadataIndex.search(query);
                var matchingTabIds = matchedDocuments.map(function (metadataDoc) {
                    return metadataDoc.id;
                });
                callback(null, Common.Util.removeDuplicates(matchingTabIds));
            });
        };
        TabSearchService.prototype.getMatchingTabIds = function (query, callback) {
            if (!this.luceneReady) {
                this.searchUsingBackupIndex(query, callback);
                return;
            }
            var luceneQuery = SearchService.IndexedWebDocument.getSearchFields(query);
            var tabSearchService = this;
            TabSearchService.Lucene.LuceneJS.search(TabSearchService.LIVE_TAB_SEARCH_INDEX_NAME, luceneQuery, [SearchService.IndexedWebDocument.DocumentField.ID], function (error, matches) {
                if (!error) {
                    var matchingTabIds = matches.map(function (match) {
                        return parseInt(match[SearchService.IndexedWebDocument.DocumentField.ID]);
                    });
                    if (matchingTabIds.length > 0) {
                        callback(null, Common.Util.removeDuplicates(matchingTabIds));
                        return;
                    }
                }
                if (!!error) {
                    tabSearchService.logger.error('Error in searching in tabs, trying backup index', error);
                }
                tabSearchService.searchUsingBackupIndex(query, callback);
            });
        };
        TabSearchService.LIVE_TAB_SEARCH_INDEX_NAME = 'tabs';
        TabSearchService.instance = null;
        TabSearchService.Lucene = (window.Lucene);
        return TabSearchService;
    }());
    SearchService.TabSearchService = TabSearchService;
})(SearchService || (SearchService = {}));
//# sourceMappingURL=search-service.js.map