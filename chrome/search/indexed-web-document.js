/// <reference path='../lib/ts/URIjs.d.ts'/>
'use strict';
var SearchService;
(function (SearchService) {
    var IndexedWebDocument = (function () {
        function IndexedWebDocument(id, url, title, description, text) {
            this.id = id;
            this.url = url;
            this.title = title;
            this.description = description;
            this.text = text;
            this.documentBoost = 1.0;
            this.storeUrl = false;
            this.storeTitle = false;
            this.additionalFields = [];
            this.id = id + '';
        }
        IndexedWebDocument.getSearchFields = function (query) {
            var allSearchableFields = this.getSearchableFields();
            return allSearchableFields.map(function (searchableField) {
                return {
                    name: searchableField,
                    value: query,
                    matchType: (window.Lucene).LuceneJS.MatchType.FULL_TEXT
                };
            });
        };
        IndexedWebDocument.prototype.addField = function (fieldName, fieldValue, shouldIndex, shouldStore, shouldAnalzye, boost) {
            if (shouldIndex === void 0) { shouldIndex = false; }
            if (shouldStore === void 0) { shouldStore = false; }
            if (shouldAnalzye === void 0) { shouldAnalzye = false; }
            if (boost === void 0) { boost = 1; }
            this.additionalFields.push({
                name: fieldName,
                value: fieldValue,
                shouldIndex: shouldIndex,
                shouldStore: shouldStore,
                shouldAnalyze: shouldAnalzye,
                boost: boost
            });
        };
        IndexedWebDocument.prototype.setDocumentBoost = function (boost) {
            this.documentBoost = boost;
        };
        IndexedWebDocument.prototype.setDescription = function (description) {
            this.description = description;
        };
        IndexedWebDocument.prototype.setText = function (text) {
            this.text = text;
        };
        IndexedWebDocument.prototype.setStoreUrl = function (storeUrl) {
            this.storeUrl = !!storeUrl;
        };
        IndexedWebDocument.prototype.setStoreTitle = function (storeTitle) {
            this.storeTitle = !!storeTitle;
        };
        IndexedWebDocument.prototype.tokenizeHost = function (host) {
            var parts = host.split('.');
            return parts.join(' ');
        };
        IndexedWebDocument.prototype.getSimplifiedUrl = function (uri) {
            return uri.host() + uri.path();
        };
        IndexedWebDocument.prototype.getIndexFields = function () {
            var parsedURI = new URI(this.url);
            var indexFields = [{
                    name: IndexedWebDocument.DocumentField.ID,
                    value: this.id,
                    shouldIndex: true,
                    shouldStore: true,
                    shouldAnalyze: false
                }, {
                    name: IndexedWebDocument.DocumentField.TITLE,
                    value: this.title || '',
                    shouldIndex: true,
                    shouldStore: this.storeTitle,
                    shouldAnalyze: true
                }, {
                    name: IndexedWebDocument.DocumentField.DOMAIN,
                    // Lucene standard analyzer will not tokenize parts
                    // of domain name by default. This implies that
                    // www.google.com will not match the query `google`.
                    // As a workaround we split the domain name into
                    // tokens before indexing.
                    value: this.tokenizeHost(parsedURI.host()) || '',
                    shouldIndex: true,
                    shouldStore: false,
                    shouldAnalyze: true
                }, {
                    name: IndexedWebDocument.DocumentField.PATH,
                    value: parsedURI.path() || '',
                    shouldIndex: true,
                    shouldStore: false,
                    shouldAnalyze: true
                }, {
                    name: IndexedWebDocument.DocumentField.PROTOCOL,
                    value: parsedURI.protocol() || '',
                    shouldIndex: true,
                    shouldStore: false,
                    shouldAnalyze: false
                }, {
                    name: IndexedWebDocument.DocumentField.URL,
                    value: this.url,
                    shouldIndex: true,
                    shouldStore: this.storeUrl,
                    shouldAnalyze: true
                }, {
                    name: IndexedWebDocument.DocumentField.URL_SIMPLIFIED,
                    value: this.getSimplifiedUrl(parsedURI),
                    shouldIndex: true,
                    shouldStore: false,
                    shouldAnalyze: true
                }];
            if (!!this.description) {
                indexFields.push({
                    name: IndexedWebDocument.DocumentField.DESCRIPTION,
                    value: this.description,
                    shouldIndex: true,
                    shouldStore: false,
                    shouldAnalyze: true
                });
            }
            if (!!this.text) {
                indexFields.push({
                    name: IndexedWebDocument.DocumentField.TEXT,
                    value: this.text,
                    shouldIndex: true,
                    shouldStore: false,
                    shouldAnalyze: true
                });
            }
            indexFields.forEach(function (field) {
                field.boost = IndexedWebDocument.getWebDocumentFieldBoost(field.name);
            });
            this.additionalFields.forEach(function (additionalField) {
                indexFields.push(additionalField);
            });
            return indexFields;
        };
        IndexedWebDocument.prototype.getIndexDocument = function () {
            var indexDocument = {};
            indexDocument[IndexedWebDocument.DocumentProperty.FIELDS] = this.getIndexFields();
            indexDocument[IndexedWebDocument.DocumentProperty.BOOST] = this.documentBoost;
            return indexDocument;
        };
        IndexedWebDocument.getWebDocumentFieldBoost = function (fieldName /*DocumentField*/) {
            switch (fieldName) {
                case IndexedWebDocument.DocumentField.DESCRIPTION:
                    return 0.5;
                case IndexedWebDocument.DocumentField.URL_SIMPLIFIED:
                    return 8;
                case IndexedWebDocument.DocumentField.TITLE:
                    return 32;
                case IndexedWebDocument.DocumentField.DOMAIN:
                    return 512;
                default:
                    return 1;
            }
        };
        IndexedWebDocument.getSearchableFields = function () {
            return [
                IndexedWebDocument.DocumentField.TITLE,
                IndexedWebDocument.DocumentField.DOMAIN,
                IndexedWebDocument.DocumentField.URL_SIMPLIFIED,
                IndexedWebDocument.DocumentField.DESCRIPTION,
                IndexedWebDocument.DocumentField.TEXT
            ];
        };
        IndexedWebDocument.DocumentField = {
            ID: 'id',
            URL: 'url',
            URL_SIMPLIFIED: 'url_simplified',
            TITLE: 'title',
            DOMAIN: 'domain',
            PATH: 'path',
            PROTOCOL: 'protocol',
            DESCRIPTION: 'description',
            TEXT: 'text'
        };
        IndexedWebDocument.DocumentProperty = {
            FIELDS: 'fields',
            BOOST: 'boost'
        };
        return IndexedWebDocument;
    }());
    SearchService.IndexedWebDocument = IndexedWebDocument;
})(SearchService || (SearchService = {}));
//# sourceMappingURL=indexed-web-document.js.map