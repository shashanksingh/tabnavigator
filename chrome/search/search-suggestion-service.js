/// <reference path='../common/common.ts'/>
var SearchService;
(function (SearchService) {
    var SearchSuggestionService = (function () {
        function SearchSuggestionService() {
            this.logger = new Common.Logger('search-suggestion-service');
            this.pendingXHR = null;
            if (SearchSuggestionService.instance) {
                throw new Error('Use SearchSuggestionService.getInstance()');
            }
            this.suggestionsCache = new Common.SpatioTemporalBoundedMap(SearchSuggestionService.MAX_CACHE_ENTRIES, SearchSuggestionService.CACHE_TTL);
        }
        SearchSuggestionService.getInstance = function () {
            if (!SearchSuggestionService.instance) {
                SearchSuggestionService.instance = new SearchSuggestionService();
            }
            return SearchSuggestionService.instance;
        };
        SearchSuggestionService.getUserLocale = function () {
            return window.navigator.userLanguage
                || window.navigator.language
                || SearchSuggestionService.DEFAULT_LOCALE;
        };
        SearchSuggestionService.prototype.getSuggestionAPIURL = function (query) {
            return Common.Util.printf(SearchSuggestionService.SUGGESTION_API_URL_PATTERN, {
                q: encodeURIComponent(query),
                lang: encodeURIComponent(SearchSuggestionService.getUserLocale())
            });
        };
        SearchSuggestionService.prototype.getSuggestions = function (query, callback) {
            if (!!this.pendingXHR) {
                try {
                    this.pendingXHR.abort();
                }
                catch (e) {
                    this.logger.error('Ignored error in aborting pending xhr', e);
                }
            }
            var cachedSuggestions = this.suggestionsCache.get(query);
            if (!!cachedSuggestions) {
                callback(cachedSuggestions.slice(0, SearchSuggestionService.MAX_SUGGESTIONS));
                return;
            }
            var xhr = this.pendingXHR = new XMLHttpRequest();
            var url = this.getSuggestionAPIURL(query);
            xhr.open("GET", url, true);
            xhr.responseType = 'json';
            xhr.send();
            var suggestionService = this;
            xhr.onload = function (event) {
                var response = xhr.response;
                var suggestions = response[1];
                if (!suggestions) {
                    callback(null);
                    return;
                }
                suggestionService.suggestionsCache.set(query, suggestions);
                callback(suggestions.slice(0, SearchSuggestionService.MAX_SUGGESTIONS));
            };
            xhr.onabort = function (event) {
                suggestionService.logger.warn('Error in getting suggestions, XHR aborted', event);
                callback(null);
            };
            xhr.onerror = function (event) {
                suggestionService.logger.error('Error in getting suggestions, XHR error', event);
                callback(null);
            };
        };
        SearchSuggestionService.SUGGESTION_API_URL_PATTERN = 'https://api.bing.com/osjson.aspx?query={q}&language={lang}';
        SearchSuggestionService.DEFAULT_LOCALE = 'en-US';
        SearchSuggestionService.MAX_CACHE_ENTRIES = 1000;
        SearchSuggestionService.CACHE_TTL = 86400 * 7 * 1000;
        SearchSuggestionService.MAX_SUGGESTIONS = 5;
        SearchSuggestionService.instance = null;
        return SearchSuggestionService;
    }());
    SearchService.SearchSuggestionService = SearchSuggestionService;
})(SearchService || (SearchService = {}));
//# sourceMappingURL=search-suggestion-service.js.map