/// <reference path='lib/ts/chrome.d.ts'/>
/// <reference path='lib/ts/filesystem.d.ts'/>
/// <reference path='common/common.ts'/>
/// <reference path='tab-suspension-service.ts'/>

module Preview {
    var Constants = {
        Databases: {
            SCREEN_SHOTS: 'screen-shots'
        },
        Indices: {
            ScreenShotEncodedURL: {
                NAME: 'encodedURL',
                FIELD: 'encodedURL',
                UNIQUE: true
            },
            ScreenShotDomain: {
                NAME: 'domain',
                FIELD: 'domain',
                UNIQUE: false
            }
        },
        DatabaseVersions: {
            SCREEN_SHOTS: 9
        },
        SCREEN_SHOT_TTL: 60 * 60 * 1000,
        FAVICON_TTL: 7 * 86400 * 1000,
        MAX_SCREEN_SHOT_SCREEN_FRACTION: 0.2,
        SCREEN_SHOT_TIMER_MIN_INTERVAL: 2 * 1000,
        SCREEN_SHOT_TIMER_MAX_INTERVAL: 128 * 1000,
    };

    type PreviewRecordCallback = (record: Common.PreviewRecord) => void;

    class BlobUtil {
        public static base64URLToBlob(base64URL): Blob {
            // src: https://code.google.com/p/chromium/issues/detail?id=67587#c57
            var bin = atob(base64URL.split(',')[1]),
                len = bin.length,
                len32 = len >> 2,
                a8 = new Uint8Array(len),
                a32 = new Uint32Array(a8.buffer, 0, len32);

            for(var i=0, j=0; i < len32; i++) {
                a32[i] = bin.charCodeAt(j++) |
                    bin.charCodeAt(j++) << 8 |
                    bin.charCodeAt(j++) << 16 |
                    bin.charCodeAt(j++) << 24;
            }

            var tailLength = len & 3;
            while( tailLength-- ) {
                a8[j] = bin.charCodeAt(j++);
            }
            return new Blob([a8], {'type':'image/webp'});
        }


        public static canvasToBlob(canvas): Blob {
            return this.base64URLToBlob(canvas.toDataURL('image/jpeg'));
        }

        public static urlToBlob(url: string, callback: Common.BlobCallback) {
            if (!url) {
                callback(null);
                return;
            }
            if (url.toLowerCase().indexOf('data:') === 0) {
                callback(this.base64URLToBlob(url));
                return;
            }

            var resizeRequest = new ImageResizeRequest(
                url,
                void 0,
                void 0,
                1,
                callback
            );
            ImageResizeManager.getInstance().resizeImage(resizeRequest);
        }
    }

    class FaviconService {
        private static instance: FaviconService;
        private logger: Common.Logger;

        public static getInstance(): FaviconService {
            if (!this.instance) {
                this.instance = new FaviconService();
            }
            return this.instance;
        }

        constructor() {
            this.logger = new Common.Logger('favicon-service');
        }

        private getFaviconUrl(url: string): string {
            return 'chrome://favicon/' + url;
        }

        public getFaviconBlob(url: string, callback:(blob: Blob)=>void) {
            var origin: string = new (<any>window).URL(url).origin;
            var faviconUrl = this.getFaviconUrl(origin);

            var faviconService = this;

            var xhr = new XMLHttpRequest();
            xhr.onload = function(event) {
                var blob = new Blob([xhr.response], {type: 'image/webp'});
                callback(blob);
            };
            xhr.onerror = function (error) {
                faviconService.logger.error('error in loading favicon data', error);
                callback(null);
            };

            xhr.responseType = 'arraybuffer';
            xhr.open('GET', faviconUrl, true);
            xhr.send();
        }
    }

    class ImageResizeRequest {
        constructor (public url: string,
                     public originalWidth: number,
                     public originalHeight: number,
                     public desiredScale: number,
                     public callback:(blob: Blob)=>void) {

        }
    }

    class ImageResizeManager {
        private static instance: ImageResizeManager;
        private logger: Common.Logger;
        private canvases: HTMLCanvasElement[];
        private image: HTMLImageElement;
        private requestQueue: ImageResizeRequest[];

        public static getInstance(): ImageResizeManager {
            if (!this.instance) {
                this.instance = new ImageResizeManager();
            }
            return this.instance;
        }

        constructor() {
            this.logger = new Common.Logger('image-resize-manager');
            this.canvases = [
                document.createElement('canvas'),
                document.createElement('canvas')
            ];
            this.image = new Image();
            this.requestQueue = [];
        }

        private onRequestProcessed(request: ImageResizeRequest, blob: Blob) {
            try {
                request.callback(blob);
            } catch (e) {
                this.logger.error(e);
            }
            this.processNextFromQueue();
        }

        private imageToBlob(img: any) {
            var canvas = this.canvases[0];
            canvas.width = img.originalWidth;
            canvas.height = img.originalHeight;

            var context = canvas.getContext('2d');
            context.drawImage(img, 0, 0, img.originalWidth, img.originalHeight);
            return BlobUtil.canvasToBlob(canvas);
        }

        private processNextFromQueue() {
            if (this.requestQueue.length === 0) {
                return;
            }

            var imgResizeManager = this,
                requestQueue = this.requestQueue,
                logger = this.logger;

            var currentRequest: ImageResizeRequest = requestQueue[0];

            this.image.onload = function () {
                requestQueue.shift();

                var numSteps = -1 * Math.log(currentRequest.desiredScale)/Math.log(2);
                if (numSteps < 0) {
                    numSteps = 0;
                }
                numSteps = Math.round(numSteps);

                var originalWidth: number = this.naturalWidth,
                    originalHeight: number = this.naturalHeight,
                    scale: number = 1.0,
                    drawingCanvas: HTMLCanvasElement = null;

                // no scale needed
                if (numSteps === 0) {
                    var blob = imgResizeManager.imageToBlob(this);
                    imgResizeManager.onRequestProcessed(currentRequest, blob);
                    return;
                }

                for (var i=0; i<numSteps; ++i) {
                    // if there is no scaling to be done just draw
                    scale = 1/(Math.pow(2, i + 1));

                    drawingCanvas = imgResizeManager.canvases[i%2];
                    var drawingContext = drawingCanvas.getContext('2d');
                    var image = i === 0 ? this : imgResizeManager.canvases[(i+1)%2];

                    drawingCanvas.width = originalWidth * scale;
                    drawingCanvas.height = originalHeight * scale;

                    drawingContext.drawImage(
                        image,
                        0,
                        0,
                        drawingCanvas.width,
                        drawingCanvas.height
                    );
                }

                var blob: Blob = BlobUtil.canvasToBlob(drawingCanvas);
                logger.log(
                    're-sized image from url size',
                    currentRequest.url.length,
                    'to',
                    blob.size
                );
                imgResizeManager.onRequestProcessed(currentRequest, blob);

            };
            this.image.onerror = function (error) {
                requestQueue.shift();
                imgResizeManager.logger.error('ImageResizeManager:: error in loading image', error);
                imgResizeManager.onRequestProcessed(currentRequest, null);
            };
            this.image.src = currentRequest.url;
        }

        public resizeImage(request: ImageResizeRequest) {
            this.requestQueue.push(request);
            if (this.requestQueue.length === 1) {
                this.processNextFromQueue();
            }
        }
    }

    class FileSystemManager {
        public static Directories = {
            THUMBNAILS: 'thumbnails',
            SCREEN_SHOTS: 'screen-shots',
            FAVICONS: 'favicons'
        };

        private static instance: FileSystemManager;
        private logger: Common.Logger;
        private fileSystem: FileSystem;
        private directoryEntries: {[dirName: string]: DirectoryEntry};

        public static getInstance(): FileSystemManager {
            if (!this.instance) {
                this.instance = new FileSystemManager();
            }
            return this.instance;
        }

        constructor() {
            this.logger = new Common.Logger('file-system-manager');
            this.fileSystem = null;
            this.directoryEntries = {};
        }

        private getFileSystem(callback: (fileSystem: FileSystem)=>void) {
            if (this.fileSystem) {
                callback(this.fileSystem);
                return;
            }

            var fsManager = this;
            (<any>window).webkitRequestFileSystem((<any>window).PERSISTENT, 1024 * 1024, function (fs: FileSystem) {
                fsManager.fileSystem = fs;
                callback(fs);
            })
        }

        private getDirectoryEntry(dirName: string, callback: (directoryEntry: DirectoryEntry)=>void) {
            var existingEntry: DirectoryEntry = this.directoryEntries[dirName];
            if (false && !!existingEntry) {
                callback(existingEntry);
                return;
            }

            var fsManager = this;
            this.getFileSystem(function(fs: FileSystem){
                fs.root.getDirectory(dirName, {create: true, exclusive: false}, function (dirEntry: DirectoryEntry){
                    fsManager.directoryEntries[dirName] = dirEntry;
                    callback(dirEntry);
                });
            });
        }

        public persistFile(directoryPath: string, fileName: string, fileContent: Blob, callback:(fileURL: string)=>void) {
            var logger = this.logger;

            this.getDirectoryEntry(directoryPath, function(dirEntry: DirectoryEntry){
                dirEntry.getFile(fileName, {create: true, exclusive: false}, function(fileEntry: FileEntry){
                    fileEntry.createWriter(function(fileWriter: FileWriter){
                        fileWriter.onwriteend = function(e) {
                            callback(fileEntry.toURL());
                        };

                        fileWriter.onerror = function(e) {
                            logger.error('failed to write file to fileSystem', e.toString(), e);
                            callback(null);
                        };

                        fileWriter.write(fileContent);
                    });
                }, function(error){
                    logger.error('failed to get fileEntry', error.toString(), error);
                    callback(null);
                });
            });
        }

        public readFileAsDataURI(fileURL: string, callback: Common.GenericCallback): void {
            (<any>window).webkitResolveLocalFileSystemURL(
                fileURL,
                function(fileEntry: FileEntry) {
                    fileEntry.file(
                        function(file: File) {
                            Common.Util.readFileAsDataURI(
                                file,
                                callback
                            );
                        },
                        callback
                    )
                },
                callback
            );
        }
    }

    class PreviewDatabaseManager {
        private static URL_TO_SCREEN_SHOT_CACHE_MAX_NUM_ENTRIES = 30;
        private static URL_TO_SCREEN_SHOT_CACHE_ENTRY_TTL = 10 * 60 * 1000;

        private static instance: PreviewDatabaseManager;
        private logger: Common.Logger;
        private database: IDBDatabase;
        private onReadyPendingCallbacks: Array<Common.VoidCallback>;
        private urlToScreenShotRecordCache: Common.SpatioTemporalBoundedMap<Common.PreviewRecord>;
        private domainToScreenShotRecordCache: Common.SpatioTemporalBoundedMap<Common.PreviewRecord>;

        public static getInstance(): PreviewDatabaseManager {
            if (!this.instance) {
                this.instance = new PreviewDatabaseManager();
            }
            return this.instance;
        }

        constructor() {
            this.logger = new Common.Logger('preview-database-manager');
            this.onReadyPendingCallbacks = [];

            this.urlToScreenShotRecordCache = new Common.SpatioTemporalBoundedMap<Common.PreviewRecord>(
                PreviewDatabaseManager.URL_TO_SCREEN_SHOT_CACHE_MAX_NUM_ENTRIES,
                PreviewDatabaseManager.URL_TO_SCREEN_SHOT_CACHE_ENTRY_TTL
            );
            this.domainToScreenShotRecordCache = new Common.SpatioTemporalBoundedMap<Common.PreviewRecord>(
                PreviewDatabaseManager.URL_TO_SCREEN_SHOT_CACHE_MAX_NUM_ENTRIES,
                PreviewDatabaseManager.URL_TO_SCREEN_SHOT_CACHE_ENTRY_TTL
            );

            this.initPreviewDatabases();
        }

        private addRecordToCache(previewRecord: Common.PreviewRecord): void {
            this.urlToScreenShotRecordCache.set(previewRecord.url, previewRecord);
            var domain: string = Common.PreviewRecord.getDomainForUrl(previewRecord.url);
            if (domain) {
                this.domainToScreenShotRecordCache.set(domain, previewRecord);
            }
        }

        private getRecordFromCache(url: string): Common.PreviewRecord {
            if (this.urlToScreenShotRecordCache.has(url)) {
                return this.urlToScreenShotRecordCache.get(url);
            }
            var domain = Common.PreviewRecord.getDomainForUrl(url);
            if (domain && this.domainToScreenShotRecordCache.has(domain)) {
                return this.domainToScreenShotRecordCache.get(domain);
            }
            return null;
        }

        private onReady(callback: ()=> void) {
            if (!!this.database) {
                callback();
                return;
            }
            this.onReadyPendingCallbacks.push(callback);
        }

        private initPreviewDatabases() {
            var self = this,
                openRequest = indexedDB.open(
                    Constants.Databases.SCREEN_SHOTS,
                    Constants.DatabaseVersions.SCREEN_SHOTS
                );

            openRequest.onupgradeneeded = function(e: IDBVersionChangeEvent) {

                var target: any = (<any>(e.target));
                var db = target.result;

                var objectStore: IDBObjectStore = null;
                if(!db.objectStoreNames.contains(Constants.Databases.SCREEN_SHOTS)) {
                    objectStore = db.createObjectStore(
                        Constants.Databases.SCREEN_SHOTS,
                        {
                            keyPath: "id",
                            autoIncrement: true
                        }
                    );
                } else {
                    objectStore = target.transaction.objectStore(Constants.Databases.SCREEN_SHOTS);
                }

                // make sure all indices exist
                var indexNames: string[] = Array.prototype.slice.call(objectStore.indexNames, 0);
                Object.keys(Constants.Indices).forEach(function(indexKey: string){
                    var requiredIndexInfo: any = Constants.Indices[indexKey];
                    var requiredIndexName: string = requiredIndexInfo.NAME;
                    var requiredIndexField: string = requiredIndexInfo.FIELD;
                    var isRequiredIndexUnique: boolean = requiredIndexInfo.UNIQUE;

                    if (indexNames.indexOf(requiredIndexName) >= 0) {
                        return;
                    }

                    objectStore.createIndex(
                        requiredIndexName,
                        requiredIndexField,
                        {unique: isRequiredIndexUnique}
                    );
                });
            };

            openRequest.onsuccess = function(e) {
                self.database = (<any>(e.target)).result;


                while (self.onReadyPendingCallbacks.length) {
                    var onReadyCallback = self.onReadyPendingCallbacks.shift();
                    onReadyCallback();
                }

                self.database.onerror = function(e) {
                    self.logger.error(e);
                };
            };
        }

        private getDBTransaction(readWrite: boolean): IDBTransaction {
            var self = this,
                transaction: IDBTransaction =
                    this.database.transaction([Constants.Databases.SCREEN_SHOTS], 'readwrite');


            transaction.onerror = function (event) {
                self.logger.error('error in transaction', event);
            };

            return transaction;
        }

        private getScreenShotStore(): IDBObjectStore {
            var transaction: IDBTransaction = this.getDBTransaction(true);
            return transaction.objectStore(Constants.Databases.SCREEN_SHOTS);
        }

        public addScreenShotURL(url: string,
                                screenShotRecord: Common.PreviewRecord,
                                callback?: Common.BooleanCallback) {

            if (!screenShotRecord.screenShotURL) {
                this.logger.warn('ignored empty screen shot url', screenShotRecord.screenShotURL, url);
                if (!!callback) {
                    callback(false);
                }
                return;
            }
            this.addRecordToCache(screenShotRecord);

            var previewDBManager = this;
            this.onReady(function(){
                var screenShotStore: IDBObjectStore = previewDBManager.getScreenShotStore();
                var encodedUrlIndex: IDBIndex = screenShotStore.index(
                    Constants.Indices.ScreenShotEncodedURL.NAME
                );
                var encodedUrl = Common.PreviewRecord.getEncodedUrl(url);
                var getRequest: IDBRequest = encodedUrlIndex.get(encodedUrl);

                getRequest.onsuccess = function(event) {
                    var savedRecordInDB: any = (<any>(event.target)).result;
                    var setRequest: IDBRequest;

                    if (!!savedRecordInDB) {
                        previewDBManager.logger.debug(
                            'found existing record for url, updating',
                            url,
                            savedRecordInDB
                        );
                        screenShotRecord.updatePersistedRecordScreenShot(savedRecordInDB);
                        setRequest = screenShotStore.put(savedRecordInDB);
                    } else {
                        setRequest = screenShotStore.put(screenShotRecord);
                    }

                    setRequest.onsuccess = function() {
                        if (!!callback) {
                            callback(true);
                        }
                    };
                    setRequest.onerror = function (event) {
                        previewDBManager.logger.error(
                            'error in addScreenShotURL request',
                            setRequest.error,
                            event,
                            url
                        );
                        if (callback) {
                            callback(false);
                        }
                    };
                };
                getRequest.onerror = function (event) {
                    previewDBManager.logger.error('error in getting any existing screen shot', getRequest.error, event);
                    if (callback) {
                        callback(false);
                    }
                };
            });
        }

        private getScreenShotRecordUsingIndex(indexName: string, key: string, callback: PreviewRecordCallback) {
            var previewDBManager = this;
            this.onReady(function(){
                var screenShotStore: IDBObjectStore = previewDBManager.getScreenShotStore(),
                    index: IDBIndex = screenShotStore.index(indexName),
                    request: IDBRequest = index.get(key);

                request.onsuccess = function (event) {
                    var persistedObject = (<any>(event.target)).result;
                    if (!persistedObject) {
                        callback(null);
                    } else {
                        var screenShotRecord = Common.PreviewRecord.fromPersistedRecord(persistedObject);
                        previewDBManager.addRecordToCache(screenShotRecord);
                        callback(screenShotRecord);
                    }
                };

                request.onerror = function (error) {
                    previewDBManager.logger.error(
                        'error in getting screenshot for key in index',
                        error,
                        key,
                        indexName
                    );
                    callback(null);
                };
            });
        }

        public getScreenShotRecord(url: string, callback: PreviewRecordCallback,
                                   exactUrlMatchOnly: boolean = false) {
            var cachedRecord = this.getRecordFromCache(url);
            if (!!cachedRecord) {
                callback(cachedRecord);
                return;
            }

            var previewDBManager = this;
            this.getScreenShotRecordUsingIndex(
                Constants.Indices.ScreenShotEncodedURL.NAME,
                Common.PreviewRecord.getEncodedUrl(url),
                function(previewRecord: Common.PreviewRecord){
                    if (!!previewRecord) {
                        callback(previewRecord);
                        return;
                    }

                    if (!!exactUrlMatchOnly) {
                        callback(null);
                        return;
                    }

                    var domain = Common.PreviewRecord.getDomainForUrl(url);
                    if (!domain) {
                        callback(null);
                        return;
                    }

                    previewDBManager.getScreenShotRecordUsingIndex(
                        Constants.Indices.ScreenShotDomain.NAME,
                        domain,
                        callback
                    );
                }
            );
        }
    }

    export class PreviewManager {
        private static FALLBACK_CACHE_MAX_ENTRIES = 30;
        private static FALLBACK_CACHE_TTL = 10 * 60 * 1000;

        private static instance: PreviewManager;
        private logger: Common.Logger;
        private fallbackPreviewCache: Common.SpatioTemporalBoundedMap<Common.PreviewRecord>;
        // since the chrome.tabs.captureVisibleTab seems to fail arbitrarily with
        // "Failed to capture tab: unknown error" errors, not sure if it helps
        // we re-try for screen shots in a lazy loop every minute or so.
        private activeTabScreenShotTimer: any;


        public static getInstance(): PreviewManager {
            if (!this.instance) {
                this.instance = new PreviewManager();
            }
            return this.instance;
        }

        constructor() {
            this.logger = new Common.Logger('preview-manager');
            this.activeTabScreenShotTimer = null;
            this.fallbackPreviewCache = new Common.SpatioTemporalBoundedMap<Common.PreviewRecord>(
                PreviewManager.FALLBACK_CACHE_MAX_ENTRIES,
                PreviewManager.FALLBACK_CACHE_TTL
            );
            this.setUpTabListeners();
            this.populateInitialPreview();
        }

        public getPreviewRecords(pageURLs: string[],
                                 callback: (previewRecords: Common.PreviewRecord[]) => void) {
            var index = 0,
                previewURLs = [],
                dbManager: PreviewDatabaseManager = PreviewDatabaseManager.getInstance(),
                previewManager = this;

            var getNextPreviewURL = function () {
                if (index >= pageURLs.length) {
                    callback(previewURLs);
                    return;
                }

                var nextURL = pageURLs[index++];
                dbManager.getScreenShotRecord(nextURL, function(record: Common.PreviewRecord) {
                    previewURLs[index - 1] = record || previewManager.fallbackPreviewCache.get(nextURL) || null;
                    getNextPreviewURL();
                });
            };

            getNextPreviewURL();
        }

        public getDataURIForFileSystemURL(filesystemURL: string,
                                          callback: Common.GenericCallback): void {
            FileSystemManager.getInstance().readFileAsDataURI(filesystemURL, callback);
        }

        private setUpTabListeners() {
            var self = this;
            chrome.tabs.onActivated.addListener(function(activeInfo: chrome.tabs.TabActiveInfo){
                self.restartActiveTabScreenShotTimer();
            });
            chrome.tabs.onUpdated.addListener(function(tabId: number, changeInfo: any,
                                                       tab: chrome.tabs.Tab){
                if (!tab.active) {
                    // can take screenshot of active tab only
                    return;
                }

                if (!changeInfo.url && !changeInfo.title) {
                    return;
                }
                self.restartActiveTabScreenShotTimer();
            });
        }

        private encodeURL(url: string): string {
            return btoa(encodeURIComponent(url).replace(/%([0-9A-F]{2})/g, function(match, p1) {
                return String.fromCharCode(parseInt('0x' + p1));
            })).replace(/[/]/g, '_');
        }

        private saveScreenShotBlobToDisk(tab: chrome.tabs.Tab,
                                         fileBlob: Blob,
                                         isThumbnail: boolean,
                                         callback:Common.StringCallback) {
            var fileName = this.encodeURL(tab.url);
            var fsManager = FileSystemManager.getInstance();

            var dirName = isThumbnail ? FileSystemManager.Directories.THUMBNAILS
                : FileSystemManager.Directories.SCREEN_SHOTS;
            fsManager.persistFile(dirName, fileName, fileBlob, callback);
        }

        private getTabPhysicalResolution(tab: chrome.tabs.Tab): Common.Size {
            var devicePixelRatio: number = Common.Util.getDevicePixelRatio();
            var width: number = (<any>tab).width * devicePixelRatio;
            var height: number = (<any>tab).height * devicePixelRatio;

            return new Common.Size(width, height);
        }

        private getFullScreenShotScale(): number {
            // The size of the screenshot dimensions is twice the screen
            // dimensions on retina screens. We care less about that level
            // of quality of the screen shot than we care about the disk space
            // it will take. Hence we save the full screen shot at the
            // logical resolution instead of physical.
            var devicePixelRatio: number = Common.Util.getDevicePixelRatio();
            return 1/devicePixelRatio;
        }

        private getThumbnailScale(tab: chrome.tabs.Tab): number {
            var tabPhysicalResolution: Common.Size
                = this.getTabPhysicalResolution(tab);

            var maxWidth: number
                = Constants.MAX_SCREEN_SHOT_SCREEN_FRACTION * window.screen.availWidth;
            var maxHeight: number
                = Constants.MAX_SCREEN_SHOT_SCREEN_FRACTION * window.screen.availHeight;

            var scale = 1.0;

            if (tabPhysicalResolution.width >= tabPhysicalResolution.height) {
                var width = Math.min(maxWidth, tabPhysicalResolution.width);
                scale = width/tabPhysicalResolution.width;
            } else {
                var height = Math.min(maxHeight, tabPhysicalResolution.height);
                scale = height/tabPhysicalResolution.height;
            }

            scale = Math.min(1.0, scale);
            return scale;
        }

        private saveScreenShotAtScale(tab: chrome.tabs.Tab,
                                      dataURL: string,
                                      scale: number,
                                      isThumbnail: boolean,
                                      callback:Common.StringCallback) {
            var tabPhysicalResolution: Common.Size
                = this.getTabPhysicalResolution(tab);

            var previewManager = this;
            var resizeRequest = new ImageResizeRequest(
                dataURL,
                tabPhysicalResolution.width,
                tabPhysicalResolution.height,
                scale,
                function(fullScreenShotBlob: Blob) {
                    previewManager.saveScreenShotBlobToDisk(
                        tab,
                        fullScreenShotBlob,
                        isThumbnail,
                        callback
                    );
                }
            );
            ImageResizeManager.getInstance().resizeImage(resizeRequest);
        }

        private saveScreenShotDataURLToDisk(tab: chrome.tabs.Tab,
                                            dataURL: string,
                                            callback:Common.StringArrayCallback) {
            var previewManager = this;

            var fullScreenShotScale = this.getFullScreenShotScale();
            this.saveScreenShotAtScale(
                tab,
                dataURL,
                fullScreenShotScale,
                false,
                function(fullScreenShotFileURL: string) {
                    var thumbnailScale: number = previewManager.getThumbnailScale(tab);
                    previewManager.saveScreenShotAtScale(
                        tab,
                        dataURL,
                        thumbnailScale,
                        true,
                        function(thumbnailScreenShotFileURL: string) {
                            if (callback) {
                                callback([fullScreenShotFileURL, thumbnailScreenShotFileURL]);
                            }
                        }
                    );
                }
            );
        }

        private saveFaviconToDisk(tab: chrome.tabs.Tab, callback:(persistedFileURL: string)=>void) {
            var fileName = this.encodeURL(tab.url);
            FaviconService.getInstance().getFaviconBlob(tab.url, function(blob: Blob){
                if (!blob) {
                    callback(null);
                    return;
                }
                var fsManager = FileSystemManager.getInstance();
                fsManager.persistFile(FileSystemManager.Directories.FAVICONS, fileName, blob, callback);
            });
        }

        private updateScreenShotInRecord(tab: chrome.tabs.Tab,
                                         record: Common.PreviewRecord,
                                         callback: Common.BooleanCallback) {

            var previewManager = this,
                dbManager: PreviewDatabaseManager = PreviewDatabaseManager.getInstance();

            var message: Common.PostMessage = new Common.PostMessage(Common.Commands.CAN_TAKE_SCREEN_SHOT, null);
            chrome.tabs.sendMessage(tab.id, message, function(canTakeScreenShot: boolean){
                if (!canTakeScreenShot) {
                    callback(false);
                    return;
                }

                // attempt to avoid "Failed to capture tab: unknown error" errors, not sure if it helps
                setTimeout(function(){
                    chrome.tabs.captureVisibleTab(null, {format: 'jpeg'}, function (dataURL: string) {
                        previewManager.logger.log('captured visible tab', dataURL && dataURL.length);
                        if (!dataURL) {
                            previewManager.logger.warn('empty dataURL from captureVisibleTab');
                            if (callback) {
                                callback(false);
                            }
                            return;
                        }

                        previewManager.saveScreenShotDataURLToDisk(
                            tab,
                            dataURL,
                            function(persistedFileURLs: string[]) {

                                previewManager.logger.log(
                                    'persisted processed screen shot to fileSystem',
                                    persistedFileURLs
                                );

                                var screenShotURL = persistedFileURLs[0];
                                var thumbnailURL = persistedFileURLs[1];

                                record.setScreenShotURL(screenShotURL || null, thumbnailURL || null);
                                dbManager.addScreenShotURL(tab.url, record, callback);
                            }
                        );
                    });
                }, 64);
            });

        }

        private restartActiveTabScreenShotTimer() {
            if (this.activeTabScreenShotTimer !== null) {
                clearTimeout(this.activeTabScreenShotTimer);
                this.activeTabScreenShotTimer = null;
            }

            var previewManager = this;
            var interval = Constants.SCREEN_SHOT_TIMER_MIN_INTERVAL/2;

            var onTimer = function () {
                previewManager.activeTabScreenShotTimer = null;

                previewManager.ensureScreenShotForActiveTab(function(didEnsurePersistedValidScreenShot: boolean){
                    // if there was already a valid persisted screen shot or we could get one and persist
                    // it into the database we stop the loop for this tab. the loop will be re-triggered once
                    // the active tab changes.
                    if (didEnsurePersistedValidScreenShot) {
                        return;
                    }
                    // if another timer was started while we were busy getting the screen shot and persisting it
                    // we stop our own loop
                    if (!!previewManager.activeTabScreenShotTimer) {
                        return;
                    }

                    interval *=  2;
                    if (interval > Constants.SCREEN_SHOT_TIMER_MAX_INTERVAL) {
                        previewManager.logger.warn('Active tab screen shot time gave up after', interval/2);
                        return;
                    }

                    previewManager.activeTabScreenShotTimer = setTimeout(onTimer, interval);
                });
            };

            // try once right away
            onTimer();
        }

        private ensureScreenShotForActiveTab(callback: Common.BooleanCallback) {
            var previewManager = this;

            Common.Util.getCurrentTab(function(tab: chrome.tabs.Tab){
                if (!tab || tab.status !== 'complete') {
                    callback(false);
                    return;
                }

                if (TabNavigator.TabSuspensionService.getInstance().isSuspendedTabURL(tab.url)) {
                    callback(false);
                    return;
                }

                var tabURL: string = tab.url,
                    dbManager: PreviewDatabaseManager = PreviewDatabaseManager.getInstance();

                dbManager.getScreenShotRecord(tabURL, function(record: Common.PreviewRecord){
                    var now = new Date().getTime();
                    if (!!record && now - record.screenShotTimestamp <= Constants.SCREEN_SHOT_TTL) {
                        callback(true);
                        return;
                    }

                    if (!!record && !!record.faviconURL && now - record.faviconTimestamp <= Constants.FAVICON_TTL) {
                        previewManager.updateScreenShotInRecord(tab, record, callback);
                    } else {
                        if (!record) {
                            record = new Common.PreviewRecord(tab.url, null, null, null);
                        }
                        previewManager.saveFaviconToDisk(tab, function(persistedFaviconURL: string){
                            if (!!persistedFaviconURL) {
                                record.setFaviconURL(persistedFaviconURL);
                            }
                            previewManager.updateScreenShotInRecord(tab, record, callback);
                        });
                    }
                }, true);
            });
        }

        private populateInitialPreview(callback?: Common.VoidCallback) {
            var self = this;
            chrome.tabs.query({}, function(tabs: chrome.tabs.Tab[]){
                self.populateInitialPreviewsForTabs(tabs, callback);
            });
        }

        private populateInitialPreviewsForTabs(tabs: chrome.tabs.Tab[], callback?: Common.VoidCallback) {

            if (tabs.length === 0) {
                if (callback) {
                    callback();
                }
                return;
            }

            var previewManager = this,
                logger = this.logger,
                nextTab: chrome.tabs.Tab = tabs.shift(),
                tabURL: string = nextTab.url,
                dbManager: PreviewDatabaseManager = PreviewDatabaseManager.getInstance();

            dbManager.getScreenShotRecord(tabURL, function(record: Common.PreviewRecord){
                if (!!record) {
                    logger.log('found an existing persisted screenshot record for url', tabURL);
                    previewManager.populateInitialPreviewsForTabs(tabs, callback);
                    return;
                }

                logger.log('attempting to extract fallback preview to get a short term screen shot');

                var message: Common.PostMessage = new Common.PostMessage(Common.Commands.GET_PAGE_PREVIEW, null);
                chrome.tabs.sendMessage(nextTab.id, message, function(previewURL: string){
                    logger.log('short term preview URL length', previewURL && previewURL.length);

                    if (!previewURL) {
                        previewManager.populateInitialPreviewsForTabs(tabs, callback);
                        return;
                    }

                    BlobUtil.urlToBlob(previewURL, function(blob: Blob){
                        if (!blob) {
                            previewManager.populateInitialPreviewsForTabs(tabs, callback);
                            return;
                        }

                        previewManager.saveScreenShotBlobToDisk(
                            nextTab,
                            blob,
                            false,
                            function(persistedScreenShotURL: string){
                                previewManager.saveFaviconToDisk(nextTab, function(persistedFaviconURL: string){
                                    if (!persistedFaviconURL) {
                                        persistedFaviconURL = '';
                                    }

                                    var screenShotRecord = new Common.PreviewRecord(
                                        tabURL,
                                        persistedScreenShotURL,
                                        // TODO (shashank): resize this
                                        persistedScreenShotURL,
                                        persistedFaviconURL
                                    );

                                    screenShotRecord.makeShortLived();
                                    previewManager.fallbackPreviewCache.set(tabURL, screenShotRecord);
                                    previewManager.populateInitialPreviewsForTabs(tabs, callback);

                                    logger.log('short term screen shot cached', screenShotRecord);
                                });
                            }
                        );
                    });
                });
            });
        }
    }

}