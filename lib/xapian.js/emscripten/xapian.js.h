#ifndef JSXAPIAN_H
#define JSXAPIAN_H

#include <xapian.h>
#include <string>

using namespace std;

class JSXapianHTMLDocument {
public:
    string url;
    string title;
    string document;
};

class JSXapian {
public:
    JSXapian();
    void addDocument(JSXapianHTMLDocument *document);
};

#endif
