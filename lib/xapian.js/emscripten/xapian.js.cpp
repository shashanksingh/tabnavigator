#include "xapian.js.h"
#include <iostream>

using namespace std;

JSXapian::JSXapian() {
    try {
        string data = "We've reached the end of a paragraph, so index it";
        string query_string = "paregraph";
        
        cout << "Constructing JSXapian" << endl;
        Xapian::WritableDatabase db = Xapian::InMemory::open();
        
        Xapian::TermGenerator indexer;
        Xapian::Stem stemmer("english");
        indexer.set_stemmer(stemmer);
        indexer.set_database(db);
        indexer.set_flags(indexer.FLAG_SPELLING);
        
        Xapian::Document doc;
        
        doc.set_data(data);
        indexer.set_document(doc);
        indexer.index_text(data);
        db.add_document(doc);
        
        db.commit();
        
        Xapian::QueryParser qp;
        qp.set_stemmer(stemmer);
        qp.set_database(db);
        qp.set_stemming_strategy(Xapian::QueryParser::STEM_SOME);
        Xapian::Query query = qp.parse_query(query_string);
        cout << "Parsed query is: " << query.get_description() << endl;
        
        Xapian::Enquire enquire(db);
        enquire.set_query(query);
        
        Xapian::MSet matches = enquire.get_mset(0, 10);
        cout << matches.get_matches_estimated() << " results found.\n";
        cout << "Matches 1-" << matches.size() << ":\n" << endl;
        
        for (Xapian::MSetIterator i = matches.begin(); i != matches.end(); ++i) {
            cout << i.get_rank() + 1 << ": " << i.get_weight() << " docid=" << *i
            << " [" << i.get_document().get_data() << "]\n\n";
        }
        
        cout << "Successfully Constructed JSXapian" << endl;
        
    } catch (const Xapian::Error &e) {
        cout << e.get_description() << endl;
        exit(1);
    }
}

void JSXapian::addDocument(JSXapianHTMLDocument *document) {
    
}

int main(int argc, char** argv) {
    cout<<"in main" << endl;
    JSXapian *x = new JSXapian();
}
