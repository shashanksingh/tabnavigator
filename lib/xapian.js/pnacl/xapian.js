/// <reference path='../../../chrome/common/common.ts'/>
var Xapian;
(function (Xapian) {
    var XapianJS = (function () {
        function XapianJS() {
        }
        XapianJS.openOrCreateIndex = function (indexId, indexType /*XapianJS.IndexType*/, callback) {
            this.initializeBackend(function (error) {
                if (!!error) {
                    callback(error);
                    return;
                }
                var persistent = indexType === XapianJS.IndexType.PERSISTENT;
                XapianJS.loadFileSystem(persistent, function (error) {
                    if (!!error) {
                        callback(error);
                        return;
                    }
                });
            });
        };
        XapianJS.shutdown = function () {
            var pNaClRoot = document.getElementById(XapianJS.PNACL_ROOT_ID);
            if (!pNaClRoot) {
                XapianJS.logger.warn('no PNaCl root found when trying to shut down XapianJs');
                return;
            }
            pNaClRoot.parentElement.removeChild(pNaClRoot);
        };
        XapianJS.initializeBackend = function (callback) {
            if (XapianJS.backendInitialized) {
                callback(XapianJS.backendInitError);
                return;
            }
            XapianJS.pendingInitCallbacks.push(callback);
            XapianJS.onDocumentReady(function () {
                XapianJS.loadPNaClModule(function (error) {
                    XapianJS.backendInitialized = true;
                    XapianJS.backendInitError = !!error ? error : null;
                    XapianJS.executeAllPendingInitCallbacks(error);
                });
            });
        };
        XapianJS.onDocumentReady = function (callback) {
            if (document.readyState === Common.DocumentState.INTERACTIVE
                || document.readyState === Common.DocumentState.COMPLETE) {
                callback();
                return;
            }
            document.addEventListener('DOMContentLoaded', callback);
        };
        XapianJS.loadPNaClModule = function (callback) {
            var pNaClRoot = document.createElement('div');
            pNaClRoot.setAttribute('id', XapianJS.PNACL_ROOT_ID);
            function handleLoadEvent(event) {
                Object.keys(XapianJS.PNaClStateTransitionEvent).forEach(function (key) {
                    var eventName = XapianJS.PNaClStateTransitionEvent[key];
                    pNaClRoot.addEventListener(eventName, handleLoadEvent);
                });
                pNaClRoot.addEventListener('crash', function (e) {
                    XapianJS.onPNaClModuleRuntimeCrash(e);
                }, true);
                var errorMessage;
                switch (event.type) {
                    case XapianJS.PNaClStateTransitionEvent.LOAD:
                        callback(null);
                        break;
                    case XapianJS.PNaClStateTransitionEvent.ABORT:
                        errorMessage = 'PNaCl module load aborted by the user';
                    case XapianJS.PNaClStateTransitionEvent.ERROR:
                        errorMessage = 'Error in loading PNaCl module';
                    case XapianJS.PNaClStateTransitionEvent.CRASH:
                        errorMessage = 'PNaCl module crashed!';
                        errorMessage = XapianJS.getExtendedPNaClErrorMessage(errorMessage);
                        XapianJS.logger.warn(errorMessage, event);
                        var error = new Error(errorMessage);
                        callback(error);
                        break;
                    default:
                        XapianJS.logger.warn('unhandled PNaCl module event', event.type);
                        break;
                }
            }
            Object.keys(XapianJS.PNaClStateTransitionEvent).forEach(function (key) {
                var eName = XapianJS.PNaClStateTransitionEvent[key];
                pNaClRoot.addEventListener(eName, handleLoadEvent, true);
            });
            var pNaClEmbedElement = document.createElement('embed');
            pNaClEmbedElement.setAttribute('width', '0');
            pNaClEmbedElement.setAttribute('height', '0');
            pNaClEmbedElement.setAttribute('type', XapianJS.PNACL_MIME_TYPE);
            pNaClEmbedElement.setAttribute('src', XapianJS.PNACL_MANIFEST_FILE_NAME);
            pNaClRoot.appendChild(pNaClEmbedElement);
            document.body.appendChild(pNaClRoot);
        };
        XapianJS.executeAllPendingInitCallbacks = function (error) {
            var pendingCallback;
            while (pendingCallback = XapianJS.pendingInitCallbacks.shift()) {
                try {
                    pendingCallback(error);
                }
                catch (e) {
                    XapianJS.logger.error('Error in XapianJS initialization callback, ignored', e);
                }
            }
        };
        XapianJS.onPNaClModuleRuntimeCrash = function (event) {
            XapianJS.logger.error('PNaCl module crashed at run time!', event);
        };
        XapianJS.getPNaClRoot = function () {
            return document.getElementById(XapianJS.PNACL_ROOT_ID);
        };
        XapianJS.getPNaClModuleElement = function () {
            var root = this.getPNaClRoot();
            if (!root) {
                return null;
            }
            return root.getElementsByTagName('embed')[0];
        };
        XapianJS.getExtendedPNaClErrorMessage = function (prefix) {
            var pNaClModuleElement = this.getPNaClModuleElement();
            return Common.Util.printf('{prefix}:: lastError: {lastError}, exitStatus: {exitStatus}', {
                prefix: prefix,
                lastError: pNaClModuleElement.lastError,
                exitStatus: pNaClModuleElement.exitStatus
            });
        };
        XapianJS.loadFileSystem = function (persistent, callback) {
            if (persistent) {
                this.ensurePersistentStoragePermission(function (error) {
                    if (!!error) {
                        XapianJS.logger.error('Error in getting persistent storage permission', error);
                        callback(error);
                        return;
                    }
                    XapianJS.initializeFileSystem(persistent, callback);
                });
            }
            else {
                XapianJS.initializeFileSystem(persistent, callback);
            }
        };
        XapianJS.initializeFileSystem = function (persistent, callback) {
            var message = new Common.PostMessage(Common.Commands.xapian.LOAD_FS, {
                persistent: !!persistent
            });
            this.makePNaClRequest(message, function (responseMessage) {
                var responseBody = responseMessage.data;
                var errorMessage = responseBody.error ? responseBody.error : null;
                var error = null;
                if (!!errorMessage) {
                    XapianJS.logger.error('Error in initializing file system', persistent, error);
                    error = new Error(errorMessage);
                }
                callback(error);
            });
        };
        XapianJS.ensurePersistentStoragePermission = function (callback) {
            navigator.webkitPersistentStorage.requestQuota(XapianJS.PERSISTENT_FS_REQUESTED_BYTE_SIZE, function (bytesGranted) {
                callback(null);
            }, function (e) {
                XapianJS.logger.error('Error in allocating persistent storage');
                callback(e);
            });
        };
        XapianJS.makePNaClRequest = function (message, responseCallback) {
            var pNaClRoot = XapianJS.getPNaClRoot();
            var pNaClModule = XapianJS.getPNaClModuleElement();
            var conversationId = message.conversationId;
            pNaClRoot.addEventListener('message', function onPostMessageResponse(event) {
                var responseMessage = event.data;
                if (responseMessage.conversationId !== conversationId) {
                    return;
                }
                pNaClRoot.removeEventListener('message', onPostMessageResponse);
                responseCallback(responseMessage);
            }, true);
            pNaClModule.postMessage(message);
        };
        XapianJS.PNACL_ROOT_ID = '28489f58-1293-4d39-85bd-4035b30b1569';
        XapianJS.PNACL_MANIFEST_FILE_NAME = 'xapian.js.nmf?r=8';
        XapianJS.PNACL_MIME_TYPE = 'application/x-pnacl';
        XapianJS.PERSISTENT_FS_REQUESTED_BYTE_SIZE = 16 * 1024 * 1024;
        XapianJS.PNaClStateTransitionEvent = {
            LOAD: 'load',
            ABORT: 'abort',
            ERROR: 'error',
            CRASH: 'crash'
        };
        XapianJS.backendInitialized = false;
        XapianJS.backendInitError = null;
        XapianJS.logger = new Common.Logger('xapian-js');
        XapianJS.pendingInitCallbacks = [];
        XapianJS.IndexType = {
            IN_MEMORY: 'IN_MEMORY',
            PERSISTENT: 'PERSISTENT'
        };
        return XapianJS;
    })();
    Xapian.XapianJS = XapianJS;
})(Xapian || (Xapian = {}));
//# sourceMappingURL=xapian.js.map