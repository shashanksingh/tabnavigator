#include <string>
#include <iostream>
#include <sstream>
#include <uuid/uuid.h>
#include <sys/mount.h>

#include "ppapi/cpp/instance.h"
#include "ppapi/cpp/module.h"
#include "ppapi/cpp/message_loop.h"
#include "ppapi/cpp/var.h"
#include "ppapi/cpp/var_dictionary.h"
#include "nacl_io/nacl_io.h"

#include <xapian.h>

// When compiling natively on Windows, PostMessage can be #define-d to
// something else.
#ifdef PostMessage
#undef PostMessage
#endif

const char* const kPostMessageFieldNameData = "data";
const char* const kPostMessageFieldNameError = "error";
const char* const kPostMessageFieldNameCommand = "command";
const char* const kPostMessageFieldNameConversationId = "conversationId";
const char* const kPostMessageFieldNamePersistent = "persistent";

const char* const kPostMessageCommandLoadFileSystem = "LOAD_FS";

using namespace std;

static const string GenerateUUID() {
    uuid_t uuid;
    uuid_generate_time_safe(uuid);

    char uuid_buffer[37];
    uuid_unparse_lower(uuid, uuid_buffer);
    return string(uuid_buffer);
}

class IndexableDocument {
public:
    explicit IndexableDocument(const string& url, const string& title, const string& body);
    string url;
    string title;
};

class XapianJS : public pp::Instance {
public:
    explicit XapianJS(PP_Instance instance);
    virtual ~XapianJS() {}

    virtual void HandleMessage(const pp::Var& message);

private:
    const string GetPostMessageConversationId(const pp::VarDictionary& message);
    const pp::VarDictionary GetEmptyResponseForPostMessage(const pp::VarDictionary& post_message);
    void PostErrorResponseForRequest(const pp::Var& message, const string& error_message);
    void PostSuccessResponseForRequest(const pp::VarDictionary& post_message);
    void LoadFileSystem(bool persistent);
};


class XapianJSModule : public pp::Module {

public:
    XapianJSModule() : pp::Module() {}
    virtual ~XapianJSModule() {}

    virtual pp::Instance * CreateInstance(PP_Instance instance) {
        return new XapianJS(instance);
    }
};


namespace pp {
    Module* CreateModule() {
        return new XapianJSModule();
    }
}

void XapianJS::HandleMessage(const pp::Var& message) {
    if (!message.is_dictionary()) {
        PostErrorResponseForRequest(message, "PostMessage message should be a dictionary");
        return;
    }
    
    pp::VarDictionary message_dict(message);

    pp::Var data_dict_var = message_dict.Get(kPostMessageFieldNameData);
    if (!data_dict_var.is_dictionary()) {
        PostErrorResponseForRequest(message, "PostMessage `data` should be a dictionary");
        return;
    }
    
    pp::VarDictionary data_dict(data_dict_var);
    
    pp::Var command_var = message_dict.Get(kPostMessageFieldNameCommand);
    if (!command_var.is_string()) {
        PostErrorResponseForRequest(message_dict, "PostMessage command should be a string");
        return;
    }
    
    string command = command_var.AsString();
    if (!command.compare(kPostMessageCommandLoadFileSystem)) {
        pp::Var persistentVar = data_dict.Get(kPostMessageFieldNamePersistent);
        if (!persistentVar.is_bool()) {
            PostErrorResponseForRequest(message_dict, "PostMessage `data.persistent` must field be a boolean");
            return;
        }

        bool persistent = persistentVar.AsBool();
        LoadFileSystem(persistent);
        PostSuccessResponseForRequest(message_dict);
    } else {
        stringstream error_msg_stream;
        error_msg_stream << "Unknown command" << " " << command;

        PostErrorResponseForRequest(message_dict, error_msg_stream.str());
    }
}

const string XapianJS::GetPostMessageConversationId(const pp::VarDictionary& message) {
    pp::Var conversation_id_var = message.Get(kPostMessageFieldNameConversationId);
    string conversation_id;

    if (conversation_id_var.is_string()) {
        conversation_id = conversation_id_var.AsString();
    } else {
        conversation_id = GenerateUUID();
    }
    return conversation_id;
}

const pp::VarDictionary XapianJS::GetEmptyResponseForPostMessage(const pp::VarDictionary& post_message) {
    string conversation_id = GetPostMessageConversationId(post_message);

    pp::VarDictionary response = pp::VarDictionary();
    response.Set(kPostMessageFieldNameConversationId, conversation_id);
    return response;
}

void XapianJS::PostErrorResponseForRequest(const pp::Var& post_message,
                                 const string& error_message) {

    pp::VarDictionary response;
    if (!post_message.is_dictionary()) {
        response = pp::VarDictionary();
    } else {
        response = GetEmptyResponseForPostMessage(pp::VarDictionary(post_message));
    }
    
    pp::VarDictionary responseData;
    response.Set(kPostMessageFieldNameData, responseData);
    
    responseData.Set(kPostMessageFieldNameError, error_message);

    PostMessage(response);
}

void XapianJS::PostSuccessResponseForRequest(const pp::VarDictionary& post_message) {
    pp::VarDictionary response = GetEmptyResponseForPostMessage(post_message);
    
    pp::VarDictionary responseData;
    response.Set(kPostMessageFieldNameData, responseData);
    
    PostMessage(response);
}

void XapianJS::LoadFileSystem(bool persistent) {
    cout << "Loading FileSystem" << " " << persistent << endl;
    
    nacl_io_init_ppapi(pp::Instance::pp_instance(),
                       pp::Module::Get()->get_browser_interface());
    
    // By default, nacl_io mounts / to pass through to the original NaCl
    // filesystem (which doesn't do much). Let's remount it to a memfs
    // filesystem.
    umount("/");
    mount("", "/", "memfs", 0, "");
    
    mount("",
          "/persistent",
          "html5fs",
          0,
          "type=PERSISTENT,expected_size=16777216");
    
    cout << "Loaded FileSystem" << " " << persistent << endl;
}

//void XapianJS::AddDocumentToIndex(


XapianJS::XapianJS(PP_Instance instance)
: pp::Instance(instance) {

    /*try {
        string data = "We've reached the end of a paragraph, so index 2 it";
        string query_string = "paragraph";

        cout << "Constructing JSXapian" << endl;

        Xapian::WritableDatabase db = Xapian::InMemory::open();

        Xapian::TermGenerator indexer;
        Xapian::Stem stemmer("english");
        indexer.set_stemmer(stemmer);
        indexer.set_database(db);
        //indexer.set_flags(indexer.FLAG_SPELLING);

        Xapian::Document doc;

        doc.set_data(data);
        indexer.set_document(doc);
        indexer.index_text(data);
        db.add_document(doc);

        db.commit();

        Xapian::QueryParser qp;
        qp.set_stemmer(stemmer);
        qp.set_database(db);
        qp.set_stemming_strategy(Xapian::QueryParser::STEM_SOME);
        Xapian::Query query = qp.parse_query(query_string);
        cout << "Parsed query is: " << query.get_description() << endl;

        Xapian::Enquire enquire(db);
        enquire.set_query(query);

        Xapian::MSet matches = enquire.get_mset(0, 10);
        cout << matches.get_matches_estimated() << " results found.\n";
        cout << "Matches 1-" << matches.size() << ":\n" << endl;

        for (Xapian::MSetIterator i = matches.begin(); i != matches.end(); ++i) {
            cout << i.get_rank() + 1 << ": " << i.get_weight() << " docid=" << *i
            << " [" << i.get_document().get_data() << "]\n\n";
        }

        cout << "Successfully2 Constructed JSXapian" << endl;

    } catch (const Xapian::Error &e) {
        cout << e.get_description() << endl;
        exit(1);
    }*/
}
                                  
IndexableDocument::IndexableDocument(const string& url,
                                     const string& title,
                                     const string& body) {
    
}
