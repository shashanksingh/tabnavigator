/// <reference path='../../../chrome/common/common.ts'/>
module Xapian {
    export class XapianJS {
        private static PNACL_ROOT_ID = '28489f58-1293-4d39-85bd-4035b30b1569';
        private static PNACL_MANIFEST_FILE_NAME = 'xapian.js.nmf?r=8';
        private static PNACL_MIME_TYPE = 'application/x-pnacl';

        private static PERSISTENT_FS_REQUESTED_BYTE_SIZE = 16 * 1024 * 1024;

        private static PNaClStateTransitionEvent = {
            LOAD: 'load',
            ABORT: 'abort',
            ERROR: 'error',
            CRASH: 'crash'
        };

        private static backendInitialized: boolean = false;
        private static backendInitError: Error = null;

        private static logger: Common.Logger = new Common.Logger('xapian-js');
        private static pendingInitCallbacks: Common.ErrorCallback[] = [];

        public static IndexType = {
            IN_MEMORY: 'IN_MEMORY',
            PERSISTENT: 'PERSISTENT'
        };

        public static openOrCreateIndex(indexId: string,
                                        indexType: string /*XapianJS.IndexType*/,
                                        callback: Common.ErrorCallback) {

            this.initializeBackend(function(error: Error){
                if (!!error) {
                    callback(error);
                    return;
                }

                var persistent: boolean = indexType === XapianJS.IndexType.PERSISTENT;
                XapianJS.loadFileSystem(
                    persistent,
                    function(error){
                        if (!!error) {
                            callback(error);
                            return;
                        }
                    }
                );
            });
        }

        public static shutdown() {
            var pNaClRoot: HTMLElement = document.getElementById(XapianJS.PNACL_ROOT_ID);
            if (!pNaClRoot) {
                XapianJS.logger.warn('no PNaCl root found when trying to shut down XapianJs');
                return;
            }
            pNaClRoot.parentElement.removeChild(pNaClRoot);
        }

        private static initializeBackend(callback: Common.ErrorCallback) {
            if (XapianJS.backendInitialized) {
                callback(XapianJS.backendInitError);
                return;
            }

            XapianJS.pendingInitCallbacks.push(callback);

            XapianJS.onDocumentReady(function(){
                XapianJS.loadPNaClModule(function(error: Error){
                    XapianJS.backendInitialized = true;
                    XapianJS.backendInitError = !!error ? error : null;

                    XapianJS.executeAllPendingInitCallbacks(error);
                });
            });

        }

        private static onDocumentReady(callback: Common.VoidCallback) {
            if (document.readyState === Common.DocumentState.INTERACTIVE
                || document.readyState === Common.DocumentState.COMPLETE) {
                callback();
                return;
            }
            document.addEventListener('DOMContentLoaded', callback);
        }

        private static loadPNaClModule(callback: Common.ErrorCallback) {
            var pNaClRoot: HTMLElement = document.createElement('div');
            pNaClRoot.setAttribute('id', XapianJS.PNACL_ROOT_ID);

            function handleLoadEvent(event) {
                Object.keys(XapianJS.PNaClStateTransitionEvent).forEach(function(key: string){
                    var eventName: string = XapianJS.PNaClStateTransitionEvent[key];
                    pNaClRoot.addEventListener(eventName, handleLoadEvent);
                });

                pNaClRoot.addEventListener('crash', function(e){
                    XapianJS.onPNaClModuleRuntimeCrash(<Event>e);
                }, true);

                var errorMessage;

                switch (event.type) {
                    case XapianJS.PNaClStateTransitionEvent.LOAD:
                        callback(null);
                        break;

                    case XapianJS.PNaClStateTransitionEvent.ABORT:
                        errorMessage = 'PNaCl module load aborted by the user';
                    case XapianJS.PNaClStateTransitionEvent.ERROR:
                        errorMessage = 'Error in loading PNaCl module';
                    case XapianJS.PNaClStateTransitionEvent.CRASH:
                        errorMessage = 'PNaCl module crashed!';

                        errorMessage = XapianJS.getExtendedPNaClErrorMessage(errorMessage);
                        XapianJS.logger.warn(errorMessage, event);

                        var error = new Error(errorMessage);
                        callback(error);
                        break;

                    default:
                        XapianJS.logger.warn('unhandled PNaCl module event', event.type);
                        break;
                }
            }

            Object.keys(XapianJS.PNaClStateTransitionEvent).forEach(function(key: string){
                var eName: string = XapianJS.PNaClStateTransitionEvent[key];
                pNaClRoot.addEventListener(eName, handleLoadEvent, true);
            });

            var pNaClEmbedElement: HTMLEmbedElement = document.createElement('embed');
            pNaClEmbedElement.setAttribute('width', '0');
            pNaClEmbedElement.setAttribute('height', '0');
            pNaClEmbedElement.setAttribute('type', XapianJS.PNACL_MIME_TYPE);
            pNaClEmbedElement.setAttribute('src', XapianJS.PNACL_MANIFEST_FILE_NAME);

            pNaClRoot.appendChild(pNaClEmbedElement);

            document.body.appendChild(pNaClRoot);

        }

        private static executeAllPendingInitCallbacks(error: Error) {
            var pendingCallback: Common.ErrorCallback;

            while (pendingCallback = XapianJS.pendingInitCallbacks.shift()) {
                try {
                    pendingCallback(error);
                } catch(e) {
                    XapianJS.logger.error('Error in XapianJS initialization callback, ignored', e);
                }
            }
        }

        private static onPNaClModuleRuntimeCrash(event: Event) {
            XapianJS.logger.error('PNaCl module crashed at run time!', event);
        }

        private static getPNaClRoot(): HTMLElement {
            return document.getElementById(XapianJS.PNACL_ROOT_ID);
        }

        private static getPNaClModuleElement(): HTMLElement {
            var root: HTMLElement = this.getPNaClRoot();
            if (!root) {
                return null;
            }
            return root.getElementsByTagName('embed')[0];
        }

        private static getExtendedPNaClErrorMessage(prefix: string) {
            var pNaClModuleElement: HTMLElement = this.getPNaClModuleElement();
            return Common.Util.printf(
                '{prefix}:: lastError: {lastError}, exitStatus: {exitStatus}',
                {
                    prefix: prefix,
                    lastError: (<any>pNaClModuleElement).lastError,
                    exitStatus: (<any>pNaClModuleElement).exitStatus
                }
            );
        }

        private static loadFileSystem(persistent: boolean, callback: Common.ErrorCallback) {
            if (persistent) {
                this.ensurePersistentStoragePermission(function(error){
                    if (!!error) {
                        XapianJS.logger.error('Error in getting persistent storage permission', error);
                        callback(error);
                        return;
                    }

                    XapianJS.initializeFileSystem(persistent, callback);
                });
            } else {
                XapianJS.initializeFileSystem(persistent, callback);
            }
        }

        private static initializeFileSystem(persistent: boolean, callback: Common.ErrorCallback) {
            var message = new Common.PostMessage(
                Common.Commands.xapian.LOAD_FS,
                {
                    persistent: !!persistent
                }
            );
            this.makePNaClRequest(message, function(responseMessage: Common.PostMessage){
                var responseBody = responseMessage.data;
                var errorMessage: string = responseBody.error ? responseBody.error : null;
                var error: Error = null;

                if (!!errorMessage) {
                    XapianJS.logger.error('Error in initializing file system', persistent, error);
                    error = new Error(errorMessage);
                }
                callback(error);
            });

        }

        private static ensurePersistentStoragePermission(callback: Common.ErrorCallback) {
            (<any>navigator).webkitPersistentStorage.requestQuota(
                XapianJS.PERSISTENT_FS_REQUESTED_BYTE_SIZE,
                function(bytesGranted: number) {
                    callback(null);
                },
                function(e) {
                    XapianJS.logger.error('Error in allocating persistent storage');
                    callback(e);
                });
        }

        private static makePNaClRequest(message: Common.PostMessage,
                                        responseCallback: Common.PostMessageCallback) {

            var pNaClRoot: HTMLElement = XapianJS.getPNaClRoot();
            var pNaClModule: HTMLElement = XapianJS.getPNaClModuleElement();

            var conversationId = message.conversationId;
            pNaClRoot.addEventListener('message', function onPostMessageResponse(event: MessageEvent){
                var responseMessage: Common.PostMessage = event.data;
                if (responseMessage.conversationId !== conversationId) {
                    return;
                }

                pNaClRoot.removeEventListener('message', onPostMessageResponse);
                responseCallback(responseMessage);
            }, true);

            (<any>pNaClModule).postMessage(message);
        }
    }
}