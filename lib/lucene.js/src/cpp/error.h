#ifndef LUCENEJS_ERROR_H
#define LUCENEJS_ERROR_H

#include <string>


namespace lucenejs {
    class Error;
};

class lucenejs::Error {
public:

    enum ErrorCode {
        kErrorNone = 0,
        kInvalidState,
        kInvalidInput,
        kLuceneException,
        kGenericException,
        kGenericError
    };

    ErrorCode GetCode();
    std::string GetMessage();
    void Reset();
    void Set(ErrorCode code, std::string message);
    bool IsSuccess();

private:
    ErrorCode code;
    std::string message;
};

#endif