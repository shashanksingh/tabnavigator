#ifndef LUCENEJS_H
#define LUCENEJS_H

#include "request_processor.h"

namespace lucenejs {
    class LuceneJS;
    class LuceneJSModule;
}

class lucenejs::LuceneJS : public pp::Instance {
public:
    explicit LuceneJS(PP_Instance instance);
    virtual ~LuceneJS() {}
    
    virtual void HandleMessage(const pp::Var& message);
    
private:
    RequestProcessor request_processor;
};


class lucenejs::LuceneJSModule : public pp::Module {
    
public:
    LuceneJSModule() : pp::Module() {}
    virtual ~LuceneJSModule() {}
    
    virtual pp::Instance * CreateInstance(PP_Instance instance) {
        return new LuceneJS(instance);
    }
};

#endif