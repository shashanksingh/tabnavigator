#ifndef LUCENEJS_REQUEST_PROCESSOR_H
#define LUCENEJS_REQUEST_PROCESSOR_H

#include "ppapi/cpp/var.h"
#include "ppapi/cpp/var_dictionary.h"
#include "ppapi/utility/threading/simple_thread.h"
#include "ppapi/utility/completion_callback_factory.h"
#include "ppapi/cpp/instance.h"

#include "lucenejs_index.h"

namespace lucenejs {
    class RequestProcessor;
}

class lucenejs::RequestProcessor {
public:
    RequestProcessor(const pp::Instance& instance);
    virtual ~RequestProcessor();
    
    void ProcessRequest(const pp::Var& message);
    
private:
    
    pp::Instance instance;
    bool persistent_file_system_mounted;
    std::map<std::string, lucenejs::LuceneJSIndex*> index_id_to_index;
    
    pp::CompletionCallbackFactory<RequestProcessor> callback_factory;
    pp::SimpleThread processing_thread;
    void HandleMessageOnBackgroundThread(uint32_t result, const pp::Var& message);
    
    const std::string GetPostMessageConversationId(const pp::VarDictionary& message);
    const pp::VarDictionary GetEmptyResponseForPostMessage(const pp::VarDictionary& post_message);
    void PostErrorResponseForRequest(const pp::Var& post_message,
                                     const std::string& error_message);
    
    void PostSuccessResponseForRequest(const pp::VarDictionary& post_message,
                                       const pp::VarDictionary& response_data);
    
    void PostSuccessResponseForRequest(const pp::VarDictionary& post_message);
    
    void PostMessageToJSFromMainThread(int32_t result, const pp::Var& post_message);
    void PostMessageToJSFromBackgroundThread(const pp::Var& post_message);
    void PostMessageToJS(const pp::Var& post_message);
    
    template <typename T>
    bool ExtractField(const pp::VarDictionary& dict,
                      const std::string field_name,
                      const pp::VarDictionary& message_dict,
                      T& field_value);
    
    bool ExtractField(const pp::VarDictionary& dict,
                      const std::string field_name,
                      const pp::VarDictionary& message_dict,
                      std::string &field_value);
    
    bool ExtractField(const pp::VarDictionary& dict,
                      const std::string field_name,
                      const pp::VarDictionary& message_dict,
                      bool &field_value);
    
    bool ExtractField(const pp::VarDictionary& dict,
                      const std::string field_name,
                      const pp::VarDictionary& message_dict,
                      pp::VarDictionary &field_value);
    
    bool ExtractField(const pp::VarDictionary& dict,
                      const std::string field_name,
                      const pp::VarDictionary& message_dict,
                      pp::VarArray &field_value);
    
    LuceneJSIndex* ExtractExistingIndexFromRequest(pp::VarDictionary& message_dict,
                                                   pp::VarDictionary& data_dict);
    
    
    void HandleGetOrCreateIndexRequest(pp::VarDictionary& message_dict,
                                       pp::VarDictionary& data_dict);
    void HandleIndexDocumentRequest(pp::VarDictionary& message_dict,
                                    pp::VarDictionary& data_dict);
    void HandleSearchRequest(pp::VarDictionary& message_dict,
                             pp::VarDictionary& data_dict);
    void HandleDeleteRequest(pp::VarDictionary& message_dict,
                             pp::VarDictionary& data_dict);
    void HandleAddOrUpdateDocumentsRequest(pp::VarDictionary& message_dict,
                                           pp::VarDictionary& data_dict);
    void HandleOptimizeIndexRequest(pp::VarDictionary& message_dict,
                                    pp::VarDictionary& data_dict);
    void HandleIndexStatisticsRequest(pp::VarDictionary& message_dict,
                                      pp::VarDictionary& data_dict);
    
    void MountPersistentFileSystem(lucenejs::Error& error);
    void CreateIndexIfNeeded(const std::string& index_id,
                             bool persistent,
                             lucenejs::Error& error);
};
#endif