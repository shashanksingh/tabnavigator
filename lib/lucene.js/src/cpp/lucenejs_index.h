#ifndef LUCENEJSINDEX_H
#define LUCENEJSINDEX_H

#include "error.h"

#include "LuceneHeaders.h"
#include "ppapi/cpp/var_array.h"
#include "ppapi/cpp/var_dictionary.h"

namespace lucenejs {
    class LuceneJSIndex;
}

class lucenejs::LuceneJSIndex {
    
public:
    explicit LuceneJSIndex(const std::string& index_id,
                           const std::string& persistent_index_root_directory,
                           bool persistent,
                           lucenejs::Error& error);
    virtual ~LuceneJSIndex();
    
    void Optimize(lucenejs::Error& error);
    void AddDocuments(const pp::VarArray& document_list, lucenejs::Error& error);
    pp::VarArray Search(const pp::VarArray& query,
                        const std::set<std::string>& returned_doc_fields,
                        lucenejs::Error& error);
    void Delete(const pp::VarArray& query,
                lucenejs::Error& error);
    void AddOrUpdateDocuments(const pp::VarArray& document_list,
                              const std::string& doc_id_field_name,
                              lucenejs::Error& error);
    void GetIndexStatistics(pp::VarDictionary& statistics,
                            lucenejs::Error& error);
    
private:
    std::string index_id;
    
    Lucene::DirectoryPtr index_directory;
    
    Lucene::IndexWriterPtr index_writer;
    Lucene::SearcherPtr searcher;
    
    bool searcher_refresh_pending;
    Lucene::SearcherPtr GetFreshSearcher();
};
#endif