#ifndef LUCENEJSLOGGER_H
#define LUCENEJSLOGGER_H

namespace lucenejs {
    class Logger;
}

class lucenejs::Logger {
    
public:
    explicit Logger();
    virtual ~Logger();
    
private:
};
#endif