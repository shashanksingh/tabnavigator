#include <string>
#include <iostream>


#include "ppapi/cpp/instance.h"
#include "ppapi/cpp/module.h"
#include "ppapi/cpp/message_loop.h"
#include "ppapi/cpp/var.h"

#include "LuceneHeaders.h"
#include "request_processor.h"

// When compiling natively on Windows, PostMessage can be #define-d to
// something else.
#ifdef PostMessage
#undef PostMessage
#endif

class LuceneJS : public pp::Instance {
public:
    explicit LuceneJS(PP_Instance instance);
    virtual ~LuceneJS() {}

    virtual void HandleMessage(const pp::Var& message);

private:
    lucenejs::RequestProcessor request_processor;
};


class LuceneJSModule : public pp::Module {

public:
    LuceneJSModule() : pp::Module() {}
    virtual ~LuceneJSModule() {}

    virtual pp::Instance * CreateInstance(PP_Instance instance) {
        return new LuceneJS(instance);
    }
};


namespace pp {
    Module* CreateModule() {
        return new LuceneJSModule();
    }
}

void LuceneJS::HandleMessage(const pp::Var& message) {
    this->request_processor.ProcessRequest(message);
}

LuceneJS::LuceneJS(PP_Instance instance)
: pp::Instance(instance),
  request_processor(*this){
}

