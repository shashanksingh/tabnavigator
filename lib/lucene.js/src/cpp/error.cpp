#include "error.h"


lucenejs::Error::ErrorCode lucenejs::Error::GetCode() {
    return this->code;
}

std::string lucenejs::Error::GetMessage() {
    return this->message;
}

void lucenejs::Error::Reset() {
    this->code = kErrorNone;
    this->message = "";
}

void lucenejs::Error::Set(lucenejs::Error::ErrorCode code, std::string message) {
    this->code = code;
    this->message = message;
}

bool lucenejs::Error::IsSuccess() {
    return this->code == kErrorNone;
}