#include "request_processor.h"

#include <iostream>
#include <sstream>
#include <sys/mount.h>
#include <sys/stat.h>
#include <map>
#include <type_traits>

#include "ppapi/cpp/completion_callback.h"
#include "ppapi/cpp/var_dictionary.h"
#include "nacl_io/nacl_io.h"
#include <boost/locale.hpp>

#include "error.h"
#include "util.h"

using namespace std;

const char* const kPersistentIndicesRootMountPoint = "/persistent";
const char* const kPersistentIndicesRootDirectoryPath = "/persistent/indices";
const int kPersistentIndicesRootDirectoryMode = 0777;

const char* const kPostMessageFieldNameData = "data";
const char* const kPostMessageFieldNameError = "error";
const char* const kPostMessageFieldNameCommand = "command";
const char* const kPostMessageFieldNameConversationId = "conversationId";

const char* const kPostMessageFieldNameIndexId = "indexId";
const char* const kPostMessageFieldNamePersistent = "persistent";
const char* const kPostMessageFieldNameDocuments = "documents";

const char* const kPostMessageFieldNameQuery = "query";
const char* const kPostMessageFieldNameQueryResultFields = "queryResultFields";
const char* const kPostMessageFieldNameQueryResults = "queryResults";

const char* const kPostMessageFieldNameDocumentIdFieldName = "docIdFieldName";

const char* const kPostMessageCommandGetOrCreateIndex = "GET_OR_CREATE_INDEX";
const char* const kPostMessageCommandIndexDocument = "INDEX_DOCUMENT";
const char* const kPostMessageCommandSearch = "SEARCH";
const char* const kPostMessageCommandDelete = "DELETE";
const char* const kPostMessageCommandAddOrUpdateDocuments = "ADD_OR_UPDATE_DOCUMENTS";
const char* const kPostMessageCommandOptimizeIndex = "OPTIMIZE_INDEX";
const char* const kPostMessageCommandGetIndexStatistics = "GET_INDEX_STATISTICS";

static bool indexExists(std::map<string, lucenejs::LuceneJSIndex*> index_id_to_index,
                        string index_id) {
    
    return lucenejs::Util::MapHasKey<string, lucenejs::LuceneJSIndex*>(index_id_to_index,
                                                                       index_id);
}

static void init_icu_localization() {
    boost::locale::localization_backend_manager backend_mgr
        = boost::locale::localization_backend_manager::global();
    
    backend_mgr.select("icu");
    
    boost::locale::generator gen(backend_mgr);
    boost::locale::localization_backend_manager::global(backend_mgr);
}

lucenejs::RequestProcessor::RequestProcessor(const pp::Instance& instance)
:  instance(instance),
   persistent_file_system_mounted(false),
   index_id_to_index(),
   processing_thread(pp::InstanceHandle(instance.pp_instance())),
   callback_factory(this)
{
    init_icu_localization();
    this->processing_thread.Start();
}

lucenejs::RequestProcessor::~RequestProcessor() {
    this->processing_thread.Join();
}

void lucenejs::RequestProcessor::ProcessRequest(const pp::Var& message) {
    const pp::CompletionCallback callback =
        this->callback_factory.NewCallback(&RequestProcessor::HandleMessageOnBackgroundThread,
                                           message);
    
    this->processing_thread.message_loop().PostWork(callback);
}

void lucenejs::RequestProcessor::HandleMessageOnBackgroundThread(uint32_t result,
                                                       const pp::Var& message) {
    if (result != PP_OK) {
        return;
    }
    
    if (!message.is_dictionary()) {
        PostErrorResponseForRequest(message, "PostMessage message should be a dictionary");
        return;
    }
    
    pp::VarDictionary message_dict(message);
    
    pp::Var data_dict_var = message_dict.Get(kPostMessageFieldNameData);
    if (!data_dict_var.is_dictionary()) {
        PostErrorResponseForRequest(message,
                                    "PostMessage `data` should be a dictionary");
        return;
    }
    
    pp::VarDictionary data_dict(data_dict_var);
    
    pp::Var command_var = message_dict.Get(kPostMessageFieldNameCommand);
    if (!command_var.is_string()) {
        PostErrorResponseForRequest(message_dict,
                                    "PostMessage command should be a string");
        return;
    }
    
    string command = command_var.AsString();
    if (!command.compare(kPostMessageCommandGetOrCreateIndex)) {
        HandleGetOrCreateIndexRequest(message_dict, data_dict);
    } else if (!command.compare(kPostMessageCommandIndexDocument)) {
        HandleIndexDocumentRequest(message_dict, data_dict);
    } else if (!command.compare(kPostMessageCommandSearch)) {
        HandleSearchRequest(message_dict, data_dict);
    } else if (!command.compare(kPostMessageCommandDelete)) {
        HandleDeleteRequest(message_dict, data_dict);
    } else if (!command.compare(kPostMessageCommandAddOrUpdateDocuments)) {
        HandleAddOrUpdateDocumentsRequest(message_dict, data_dict);
    } else if (!command.compare(kPostMessageCommandOptimizeIndex)) {
        HandleOptimizeIndexRequest(message_dict, data_dict);
    } else if (!command.compare(kPostMessageCommandGetIndexStatistics)) {
        HandleIndexStatisticsRequest(message_dict, data_dict);
    } else {
        stringstream error_msg_stream;
        error_msg_stream << "Unknown command" << " " << command;
        
        PostErrorResponseForRequest(message_dict, error_msg_stream.str());
    }
}

const string
lucenejs::RequestProcessor::GetPostMessageConversationId(const pp::VarDictionary& message) {
    pp::Var conversation_id_var = message.Get(kPostMessageFieldNameConversationId);
    string conversation_id;
    
    if (conversation_id_var.is_string()) {
        conversation_id = conversation_id_var.AsString();
    } else {
        conversation_id = lucenejs::Util::GenerateUUID();
    }
    return conversation_id;
}

const pp::VarDictionary
lucenejs::RequestProcessor::GetEmptyResponseForPostMessage(const pp::VarDictionary& post_message) {
    string conversation_id = GetPostMessageConversationId(post_message);
    
    pp::VarDictionary response = pp::VarDictionary();
    response.Set(kPostMessageFieldNameConversationId, conversation_id);
    return response;
}

void
lucenejs::RequestProcessor::PostErrorResponseForRequest(const pp::Var& post_message,
                                                        const string& error_message) {
    
    pp::VarDictionary response;
    if (!post_message.is_dictionary()) {
        response = pp::VarDictionary();
    } else {
        response = GetEmptyResponseForPostMessage(pp::VarDictionary(post_message));
    }
    
    pp::VarDictionary responseData;
    response.Set(kPostMessageFieldNameData, responseData);
    
    responseData.Set(kPostMessageFieldNameError, error_message);
    
    PostMessageToJS(response);
}

void
lucenejs::RequestProcessor::PostSuccessResponseForRequest(const pp::VarDictionary& post_message) {
    pp::VarDictionary response_data;
    PostSuccessResponseForRequest(post_message, response_data);
}

void
lucenejs::RequestProcessor::PostSuccessResponseForRequest(const pp::VarDictionary& post_message,
                                                          const pp::VarDictionary& response_data) {
    pp::VarDictionary response = GetEmptyResponseForPostMessage(post_message);
    response.Set(kPostMessageFieldNameData, response_data);
    
    PostMessageToJS(response);
}

void
lucenejs::RequestProcessor::PostMessageToJSFromBackgroundThread(const pp::Var& post_message) {
    const pp::CompletionCallback callback =
        this->callback_factory.NewCallback(&RequestProcessor::PostMessageToJSFromMainThread,
                                           post_message);
    pp::Module::Get()->core()->CallOnMainThread(0, callback, 0);
}

void
lucenejs::RequestProcessor::PostMessageToJSFromMainThread(int32_t result,
                                                     const pp::Var& post_message) {
    this->instance.PostMessage(post_message);
}

void
lucenejs::RequestProcessor::PostMessageToJS(const pp::Var& post_message) {
    if (pp::Module::Get()->core()->IsMainThread()) {
        PostMessageToJSFromMainThread(0, post_message);
    } else {
        PostMessageToJSFromBackgroundThread(post_message);
    }
}

template <typename T>
bool lucenejs::RequestProcessor::ExtractField(const pp::VarDictionary& dict,
                                              const std::string field_name,
                                              const pp::VarDictionary& message_dict,
                                              T& field_value) {
    return false;
}

bool lucenejs::RequestProcessor::ExtractField(const pp::VarDictionary& dict,
                                              const std::string field_name,
                                              const pp::VarDictionary& message_dict,
                                              string &field_value) {
    pp::Var var = dict.Get(field_name);
    if (!var.is_string()) {
        stringstream ss;
        ss << "`" << field_name << "` should be a string";
        PostErrorResponseForRequest(message_dict, ss.str());
        return false;
    }
    
    field_value = var.AsString();
    return true;
}

bool lucenejs::RequestProcessor::ExtractField(const pp::VarDictionary& dict,
                                              const std::string field_name,
                                              const pp::VarDictionary& message_dict,
                                              bool &field_value) {
    pp::Var var = dict.Get(field_name);
    if (!var.is_bool()) {
        stringstream ss;
        ss << "`" << field_name << "` should be a boolean";
        PostErrorResponseForRequest(message_dict, ss.str());
        return false;
    }
    field_value = var.AsBool();
    return true;
}

bool lucenejs::RequestProcessor::ExtractField(const pp::VarDictionary& dict,
                                              const std::string field_name,
                                              const pp::VarDictionary& message_dict,
                                              pp::VarDictionary &field_value) {
    pp::Var var = dict.Get(field_name);
    if (!var.is_dictionary()) {
        stringstream ss;
        ss << "`" << field_name << "` should be a dictionary";
        PostErrorResponseForRequest(message_dict, ss.str());
        return false;
    }
    field_value = (pp::VarDictionary)var;
    return true;
}

bool lucenejs::RequestProcessor::ExtractField(const pp::VarDictionary& dict,
                                              const std::string field_name,
                                              const pp::VarDictionary& message_dict,
                                              pp::VarArray &field_value) {
    pp::Var var = dict.Get(field_name);
    if (!var.is_array()) {
        stringstream ss;
        ss << "`" << field_name << "` should be an array";
        PostErrorResponseForRequest(message_dict, ss.str());
        return false;
    }
    field_value = (pp::VarArray)var;
    return true;
}

lucenejs::LuceneJSIndex*
lucenejs::RequestProcessor::ExtractExistingIndexFromRequest(pp::VarDictionary& message_dict,
                                                            pp::VarDictionary& data_dict) {
    
    string index_id;
    bool field_found = ExtractField(data_dict,
                                    kPostMessageFieldNameIndexId,
                                    message_dict,
                                    index_id);
    if (!field_found) {
        return NULL;
    }
    
    if (!indexExists(this->index_id_to_index, index_id)) {
        stringstream ss;
        ss << "Index with id " << index_id << " does not exist.";
        PostErrorResponseForRequest(message_dict, ss.str());
        return NULL;
    }
    
    LuceneJSIndex* index = this->index_id_to_index.at(index_id);
    return index;
}

void
lucenejs::RequestProcessor::HandleGetOrCreateIndexRequest(pp::VarDictionary& message_dict,
                                                          pp::VarDictionary& data_dict) {
    
    
    string index_id;
    bool field_found = ExtractField(data_dict,
                                    kPostMessageFieldNameIndexId,
                                    message_dict,
                                    index_id);
    if (!field_found) {
        return;
    }
    
    bool persistent;
    field_found = ExtractField(data_dict,
                               kPostMessageFieldNamePersistent,
                               message_dict,
                               persistent);
    if (!field_found) {
        return;
    }
    
    lucenejs::Error error;
    CreateIndexIfNeeded(index_id, persistent, error);
    if (!error.IsSuccess()) {
        PostErrorResponseForRequest(message_dict, error.GetMessage());
        return;
    }
    PostSuccessResponseForRequest(message_dict);
}

void
lucenejs::RequestProcessor::HandleIndexDocumentRequest(pp::VarDictionary& message_dict,
                                                       pp::VarDictionary& data_dict) {
    
    pp::VarArray documents;
    bool field_found = ExtractField(data_dict,
                                    kPostMessageFieldNameDocuments,
                                    message_dict,
                                    documents);
    if (!field_found) {
        return;
    }
    
    LuceneJSIndex* index = ExtractExistingIndexFromRequest(message_dict, data_dict);
    if (!index) {
        return;
    }
    
    lucenejs::Error error;
    index->AddDocuments(documents, error);
    if (!error.IsSuccess()) {
        PostErrorResponseForRequest(message_dict, error.GetMessage());
        return;
    }
    
    PostSuccessResponseForRequest(message_dict);
}

void
lucenejs::RequestProcessor::HandleSearchRequest(pp::VarDictionary& message_dict,
                                                pp::VarDictionary& data_dict) {
    
    
    pp::VarArray query;
    bool field_found = ExtractField(data_dict,
                                    kPostMessageFieldNameQuery,
                                    message_dict,
                                    query);
    if (!field_found) {
        return;
    }
    
    pp::VarArray query_result_fields_var;
    field_found = ExtractField(data_dict,
                               kPostMessageFieldNameQueryResultFields,
                               message_dict,
                               query_result_fields_var);
    if (!field_found) {
        return;
    }
    
    
    uint32_t num_query_result_fields = query_result_fields_var.GetLength();
    if (num_query_result_fields == 0) {
        stringstream ss;
        ss << "At least one query result field required.";
        PostErrorResponseForRequest(message_dict, ss.str());
        return;
    }
    
    set<string> query_result_fields;
    for (uint32_t i=0; i<num_query_result_fields; ++i) {
        pp::Var query_result_field_var = query_result_fields_var.Get(i);
        if (!query_result_field_var.is_string()) {
            stringstream ss;
            ss << "Query result field must be a string";
            PostErrorResponseForRequest(message_dict, ss.str());
            return;
        }
        
        query_result_fields.insert(query_result_field_var.AsString());
    }
    
    LuceneJSIndex* index = ExtractExistingIndexFromRequest(message_dict, data_dict);
    if (!index) {
        return;
    }
    
    lucenejs::Error error;
    pp::VarArray search_results = index->Search(query, query_result_fields, error);
    if (!error.IsSuccess()) {
        PostErrorResponseForRequest(message_dict, error.GetMessage());
        return;
    }
    
    pp::VarDictionary response_data;
    response_data.Set(kPostMessageFieldNameQueryResults, search_results);
    
    PostSuccessResponseForRequest(message_dict, response_data);
}

void
lucenejs::RequestProcessor::HandleDeleteRequest(pp::VarDictionary& message_dict,
                                                pp::VarDictionary& data_dict) {
    pp::VarArray query;
    bool field_found = ExtractField(data_dict,
                               kPostMessageFieldNameQuery,
                               message_dict,
                               query);
    if (!field_found) {
        return;
    }
    
    LuceneJSIndex* index = ExtractExistingIndexFromRequest(message_dict, data_dict);
    if (!index) {
        return;
    }
    
    lucenejs::Error error;
    index->Delete(query, error);
    if (!error.IsSuccess()) {
        PostErrorResponseForRequest(message_dict, error.GetMessage());
        return;
    }
    
    PostSuccessResponseForRequest(message_dict);
}

void
lucenejs::RequestProcessor::HandleAddOrUpdateDocumentsRequest(pp::VarDictionary& message_dict,
                                                              pp::VarDictionary& data_dict) {
    
    pp::VarArray documents;
    bool field_found = ExtractField(data_dict,
                                    kPostMessageFieldNameDocuments,
                                    message_dict,
                                    documents);
    if (!field_found) {
        return;
    }
    
    string doc_id_field_name;
    field_found = ExtractField(data_dict,
                               kPostMessageFieldNameDocumentIdFieldName,
                               message_dict,
                               doc_id_field_name);
    if (!field_found) {
        return;
    }
    
    LuceneJSIndex* index = ExtractExistingIndexFromRequest(message_dict, data_dict);
    if (!index) {
        return;
    }
    
    lucenejs::Error error;
    index->AddOrUpdateDocuments(documents, doc_id_field_name, error);
    if (!error.IsSuccess()) {
        PostErrorResponseForRequest(message_dict, error.GetMessage());
        return;
    }
    
    PostSuccessResponseForRequest(message_dict);
}

void
lucenejs::RequestProcessor::HandleOptimizeIndexRequest(pp::VarDictionary& message_dict,
                                                       pp::VarDictionary& data_dict) {
    LuceneJSIndex* index = ExtractExistingIndexFromRequest(message_dict, data_dict);
    if (!index) {
        return;
    }
    
    lucenejs::Error error;
    index->Optimize(error);
    if (!error.IsSuccess()) {
        PostErrorResponseForRequest(message_dict, error.GetMessage());
        return;
    }
    
    PostSuccessResponseForRequest(message_dict);
}

void
lucenejs::RequestProcessor::HandleIndexStatisticsRequest(pp::VarDictionary& message_dict,
                                                         pp::VarDictionary& data_dict) {
    
    LuceneJSIndex* index = ExtractExistingIndexFromRequest(message_dict, data_dict);
    if (!index) {
        return;
    }
    
    pp::VarDictionary response;
    lucenejs::Error error;
    index->GetIndexStatistics(response, error);
    if (!error.IsSuccess()) {
        PostErrorResponseForRequest(message_dict, error.GetMessage());
        return;
    }
    PostSuccessResponseForRequest(message_dict, response);
}


void lucenejs::RequestProcessor::MountPersistentFileSystem(lucenejs::Error& error) {
    error.Reset();
    
    if (this->persistent_file_system_mounted) {
        return;
    }
    
    int err_code;
    // TODO: how to handle any error here? this method does not seem
    // to support error handling.
    nacl_io_init_ppapi(this->instance.pp_instance(),
                       pp::Module::Get()->get_browser_interface());
    
    err_code = mount("",
                     kPersistentIndicesRootMountPoint,
                     "html5fs",
                     0,
                     "type=PERSISTENT,expected_size=16777216");
    
    if (err_code) {
        stringstream ss;
        ss << "Error in mounting persistent FS: " << err_code;
        error.Set(lucenejs::Error::kGenericException, ss.str());
        return;
    }
    
    err_code = mkdir(kPersistentIndicesRootDirectoryPath,
                     kPersistentIndicesRootDirectoryMode);
    if (err_code) {
        stringstream ss;
        ss << "Error in making directory for persistent FS: " << err_code;
        error.Set(lucenejs::Error::kGenericException, ss.str());
        return;
    }
    
    this->persistent_file_system_mounted = true;
    return;
}

void lucenejs::RequestProcessor::CreateIndexIfNeeded(const string& index_id,
                                                    bool persistent,
                                                    lucenejs::Error& error) {
    if (!this->persistent_file_system_mounted) {
        MountPersistentFileSystem(error);
        if (!error.IsSuccess()) {
            return;
        }
    }
    
    if (indexExists(this->index_id_to_index, index_id)) {
        return;
    }
    
    lucenejs::LuceneJSIndex* index
        = new lucenejs::LuceneJSIndex(index_id,
                                  kPersistentIndicesRootDirectoryPath,
                                  persistent,
                                  error);
    
    if (!error.IsSuccess()) {
        return;
    }
    
    this->index_id_to_index.insert(map<string, lucenejs::LuceneJSIndex*>::value_type(index_id, index));
    
    return;
}