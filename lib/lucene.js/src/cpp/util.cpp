#include "util.h"
#include <uuid/uuid.h>
#include <sys/time.h>

const std::string lucenejs::Util::GenerateUUID() {
    uuid_t uuid;
    uuid_generate_time_safe(uuid);
    
    char uuid_buffer[37];
    uuid_unparse_lower(uuid, uuid_buffer);
    return std::string(uuid_buffer);
}

const long lucenejs::Util::GetCurrentTime() {
    struct timeval tp;
    gettimeofday(&tp, NULL);
    long int ms = tp.tv_sec * 1000 + tp.tv_usec / 1000;
    return ms;
}