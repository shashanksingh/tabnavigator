#ifndef LUCENEJS_UTIL_H
#define LUCENEJS_UTIL_H

#include <string>
#include <map>
#include <set>

namespace lucenejs {
    class Util;
};

class lucenejs::Util {
public:
    template<typename K, typename V>
    static bool MapHasKey(const std::map<K, V>& haystack, const K& needle) {
        typename std::map<K, V>::const_iterator it = haystack.find(needle);
        return it != haystack.end();
    }
    
    template<typename K>
    static bool SetHasValue(const std::set<K>& haystack, const K& needle) {
        typename std::set<K>::const_iterator it = haystack.find(needle);
        return it != haystack.end();
    }
    
    static const std::string GenerateUUID();
    static const long GetCurrentTime();
};

#endif