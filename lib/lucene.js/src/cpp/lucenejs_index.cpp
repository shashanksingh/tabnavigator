#include "lucenejs_index.h"

#include "FileUtils.h"
#include "StringUtils.h"
#include "SingleInstanceLockFactory.h"
#include "Field.h"
#include "FuzzyQuery.h"
#include "NumericField.h"

#include <iostream>
#include <sstream>
#include <boost/locale.hpp>
#include <boost/filesystem.hpp>

#include "ppapi/cpp/var_dictionary.h"

#include "error.h"
#include "util.h"

using namespace std;
namespace L = Lucene;

const char* const kPathSeparator = "/";
const int32_t kMaxFieldLength = 1024 * 1024;

const uint32_t kDefaultMaxPhraseSlop = 4;
const uint32_t kDefaultMaxSearchResults = 5;
const double kDefaultFuzzySearchMinSimilary = 0.2;

const char* const kDocumentFieldsKey = "fields";
const char* const kDocumentBoostKey = "boost";

const char* const kDocumentFieldName = "name";
const char* const kDocumentFieldValue = "value";
const char* const kDocumentFieldBoost = "boost";

const char* const kDocumentFieldShouldStore = "shouldStore";
const char* const kDocumentFieldShouldIndex = "shouldIndex";
const char* const kDocumentFieldShouldAnalyze = "shouldAnalyze";
const char* const kDocumentFieldShouldStoreNorms = "shouldStoreNorms";

const char* const kQueryPropertyMatchType = "matchType";

const char* const kQueryMatchTypeFullText = "FULL_TEXT";
const char* const kQueryMatchTypeExactEqual = "EXACT_EQUAL";
const char* const kQueryMatchTypeRange = "RANGE";

const char* const kStatisticsFieldNumAllDocs = "docs_count";
const char* const kStatisticsFieldNumLiveDocs = "docs_count_live";
const char* const kStatisticsFieldNumDeletedDocs = "docs_count_deleted";
const char* const kStatisticsFieldNumUniqueTerms = "term_count_unique";

// A subclass of Lucene's query parser to expose certain
// protected methods as public as a workaround to having
// to re-implement them
class LuceneJSQueryParser: public L::QueryParser {
public:
    static L::String escape(const L::String& s) {
        return s;
        return L::QueryParser::escape(s);
    }
};


static L::String stdStringToLucenceString(const string& std_string) {
    return L::StringUtils::toUnicode(std_string);
}

static L::StandardAnalyzerPtr getDefaultAnalyzer() {
    return L::newLucene<L::StandardAnalyzer>(L::LuceneVersion::LUCENE_CURRENT);
}

static L::FieldablePtr parseField(const pp::Var& doc_field_var,
                                  lucenejs::Error& error) {
    
    L::FieldablePtr rv;
    
    if (!doc_field_var.is_dictionary()) {
        error.Set(lucenejs::Error::kInvalidInput,
                  "Field must be an object");
        return rv;
    }
    
    pp::VarDictionary doc_field_dict = (pp::VarDictionary)doc_field_var;
    
    pp::Var name_var = doc_field_dict.Get(kDocumentFieldName);
    if (!name_var.is_string()) {
        error.Set(lucenejs::Error::kInvalidInput,
                  "Field's name must be a string");
        return rv;
    }
    
    string name = name_var.AsString();
    if (name.empty()) {
        error.Set(lucenejs::Error::kInvalidInput,
                  "Field's name must not be empty");
        return rv;
    }
    
    double boost = 1.0;
    pp::Var boost_var = doc_field_dict.Get(kDocumentFieldBoost);
    if (boost_var.is_number()) {
        boost = boost_var.AsDouble();
    }
    
    bool should_store = false;
    pp::Var should_store_var = doc_field_dict.Get(kDocumentFieldShouldStore);
    if (should_store_var.is_bool()) {
        should_store = should_store_var.AsBool();
    } else if (!should_store_var.is_undefined()) {
        error.Set(lucenejs::Error::kInvalidInput,
                  "Field's `shouldStore` must be a boolean if specified");
        return rv;
    }
    
    bool should_index = false;
    pp::Var should_index_var = doc_field_dict.Get(kDocumentFieldShouldIndex);
    if (should_index_var.is_bool()) {
        should_index = should_index_var.AsBool();
    } else if (!should_index_var.is_undefined()) {
        error.Set(lucenejs::Error::kInvalidInput,
                  "Field's `shouldIndex` must be a boolean if specified");
        return rv;
    }
    
    bool should_analyze = false;
    pp::Var should_analyze_var = doc_field_dict.Get(kDocumentFieldShouldAnalyze);
    if (should_analyze_var.is_bool()) {
        should_analyze = should_analyze_var.AsBool();
    } else if (!should_analyze_var.is_undefined()) {
        error.Set(lucenejs::Error::kInvalidInput,
                  "Field's `shouldAnalyze` must be a boolean if specified");
        return rv;
    }
    
    bool should_store_norms = false;
    pp::Var should_store_norms_var = doc_field_dict.Get(kDocumentFieldShouldStoreNorms);
    if (should_store_norms_var.is_bool()) {
        should_store_norms = should_store_norms_var.AsBool();
    } else if (!should_store_norms_var.is_undefined()) {
        error.Set(lucenejs::Error::kInvalidInput,
                  "Field's `shouldStoreNorms` must be a boolean if specified");
        return rv;
    }
    
    // Note: LuceneCPP will throw if argument combination does not make sense.
    // Since we want to avoid exception handling we need to duplicate that check
    // here.
    if (!should_index && !should_store) {
        error.Set(lucenejs::Error::kInvalidInput,
                  "Field's `shouldIndex` and `shouldStore` mayn't be both false");
        return rv;
    }
    if (!should_index && should_store_norms) {
        error.Set(lucenejs::Error::kInvalidInput,
                  "Can't store norms of a field that is not indexed");
        return rv;
    }
    
    // TODO: support TermVector storage configuration
    L::Field::Store store = should_store ? L::Field::STORE_YES : L::Field::STORE_NO;
    L::Field::Index index = L::Field::INDEX_NO;
    if (should_index) {
        if (should_analyze) {
            index = should_store_norms
                ? L::Field::INDEX_ANALYZED : L::Field::INDEX_ANALYZED_NO_NORMS;
        } else {
            index = L::Field::INDEX_NOT_ANALYZED;
        }
    }
    
    pp::Var value_var = doc_field_dict.Get(kDocumentFieldValue);
    if (value_var.is_string()) {
        string value = value_var.AsString();
        rv = L::newLucene<L::Field>(stdStringToLucenceString(name),
                                    stdStringToLucenceString(value),
                                    store,
                                    index);
        
    } else if (value_var.is_number()) {
        L::NumericFieldPtr field =
            L::newLucene<L::NumericField>(stdStringToLucenceString(name),
                                          store,
                                          index);
        if (value_var.is_int()) {
            field->setIntValue(value_var.AsInt());
        } else {
            // pp::Var doesn't provide long type support
            // we treat longs as doubles too
            field->setDoubleValue(value_var.AsDouble());
        }
        
        rv = field;
        
    } else {
        stringstream ss;
        ss << "Unsupported field value type for field " << name;
        error.Set(lucenejs::Error::kInvalidInput, ss.str());
        return rv;
    }
    
    rv->setBoost(boost);
    
    return rv;
}

static pp::VarDictionary mapDocumentToDictionary(L::DocumentPtr doc,
                                                 set<string> field_names) {
    
    pp::VarDictionary rv;
    
    L::Collection<L::FieldablePtr> fields = doc->getFields();
    L::Collection<L::FieldablePtr>::iterator it = fields.begin();
    for (; it != fields.end(); ++it) {
        string field_name = L::StringUtils::toUTF8((*it)->name());
        if (lucenejs::Util::SetHasValue(field_names, field_name)) {
            rv.Set(field_name, L::StringUtils::toUTF8((*it)->stringValue()));
        }
    }
    return rv;
}


static L::QueryPtr parseFieldQuery(string field_name,
                                   string match_type,
                                   pp::VarDictionary query_field,
                                   pp::Var field_value_var,
                                   L::AnalyzerPtr analyzer,
                                   lucenejs::Error& error) {
    
    L::QueryPtr rv;
    L::String field_name_unicode = stdStringToLucenceString(field_name);
    
    if (!match_type.compare(kQueryMatchTypeFullText)) {
        if (field_value_var.is_string()) {
            string field_value = field_value_var.AsString();
            L::String field_value_unicode = stdStringToLucenceString(field_value);
            field_value_unicode = LuceneJSQueryParser::escape(field_value_unicode);
            
            L::QueryParserPtr parser
                = L::newLucene<L::QueryParser>(L::LuceneVersion::LUCENE_CURRENT,
                                               field_name_unicode,
                                               analyzer);
            
            parser->setPhraseSlop(kDefaultMaxPhraseSlop);
            return parser->parse(field_value_unicode);
            
        } else {
            stringstream ss;
            ss << "Unsupported field value type for matchType "
                << match_type << " for field " << field_name
                << " expected string";
            error.Set(lucenejs::Error::kInvalidInput, ss.str());
            return rv;
        }
    }
    
    if (!match_type.compare(kQueryMatchTypeExactEqual)) {
        if (field_value_var.is_string()) {
            string field_value = field_value_var.AsString();
            L::String field_value_unicode = stdStringToLucenceString(field_value);
            field_value_unicode = LuceneJSQueryParser::escape(field_value_unicode);
            
            L::TermPtr term = L::newLucene<L::Term>(field_name_unicode,
                                                    field_value_unicode);
            L::TermQueryPtr term_query = L::newLucene<L::TermQuery>(term);
            
            return term_query;
            
        } else if (field_value_var.is_int()) {
            int32_t field_value = field_value_var.AsInt();
            return L::NumericRangeQuery::newIntRange(field_name_unicode,
                                                     field_value,
                                                     field_value,
                                                     true,
                                                     true);
        } else if (field_value_var.is_double()) {
            double field_value = field_value_var.AsDouble();
            return L::NumericRangeQuery::newDoubleRange(field_name_unicode,
                                                        field_value,
                                                        field_value,
                                                        true,
                                                        true);
        } else {
            stringstream ss;
            ss << "Unsupported field value type for matchType "
            << match_type << " for field " << field_name;
            error.Set(lucenejs::Error::kInvalidInput, ss.str());
            return rv;
        }
    }
    
    if (!match_type.compare(kQueryMatchTypeRange)) {
        if (!field_value_var.is_array()) {
            stringstream ss;
            ss << "Unsupported field value type for matchType "
                << match_type << " & field " << field_name;
            error.Set(lucenejs::Error::kInvalidInput, ss.str());
            return rv;
        }
        
        pp::VarArray range = (pp::VarArray)field_value_var;
        if (range.GetLength() != 2) {
            stringstream ss;
            ss << "Query range array size must be 2 for matchType "
                << match_type << " & field " << field_name;
            error.Set(lucenejs::Error::kInvalidInput, ss.str());
            return rv;
        }
        
        pp::Var range_min = range.Get(0);
        pp::Var range_max = range.Get(1);
        
        bool both_strings = range_min.is_string() && range_max.is_string();
        bool both_ints = range_min.is_int() && range_max.is_int();
        bool both_doubles = range_min.is_double() && range_max.is_double();
        if (!both_strings && !both_ints && !both_doubles) {
            stringstream ss;
            ss << "Query range values must be either both "
                  "string, both integers or both doubles for matchType "
                << match_type << " & field " << field_name;
            error.Set(lucenejs::Error::kInvalidInput, ss.str());
            return rv;
        }
        
        if (both_strings) {
            string range_min_string = range_min.AsString();
            string range_max_string = range_max.AsString();
            
            L::String range_min_unicode = stdStringToLucenceString(range_min_string);
            L::String range_max_unicode = stdStringToLucenceString(range_max_string);
            
            return L::newLucene<L::TermRangeQuery>(field_name_unicode,
                                                   range_min_unicode,
                                                   range_max_unicode,
                                                   true,
                                                   true);
        }
        
        if (both_ints) {
            return L::NumericRangeQuery::newIntRange(field_name_unicode,
                                                     range_min.AsInt(),
                                                     range_max.AsInt(),
                                                     true,
                                                     true);
        }
        
        if (both_doubles) {
            return L::NumericRangeQuery::newDoubleRange(field_name_unicode,
                                                        range_min.AsDouble(),
                                                        range_max.AsDouble(),
                                                        true,
                                                        true);
        }
    }
    
    
    stringstream ss;
    ss << "Unsupported matchType "
        << match_type << " for field " << field_name;
    error.Set(lucenejs::Error::kInvalidInput, ss.str());
    return rv;
}

static void
parseRequestDocuments(const pp::VarArray& document_list,
                      vector<L::DocumentPtr>& documents,
                      lucenejs::Error& error) {
    
    uint32_t num_documents = document_list.GetLength();
    
    for (uint32_t i=0; i<num_documents; ++i) {
        pp::Var document_var = document_list.Get(i);
        if (!document_var.is_dictionary()) {
            error.Set(lucenejs::Error::kInvalidInput,
                      "Each document must be an object");
            return;
        }
        
        pp::VarDictionary document = (pp::VarDictionary)document_var;
        double document_boost = 1.0;
        pp::Var document_boost_var = document.Get(kDocumentBoostKey);
        if (document_boost_var.is_number()) {
            document_boost = document_boost_var.AsDouble();
        }
        
        pp::Var document_fields_var = document.Get(kDocumentFieldsKey);
        if (!document_fields_var.is_array()) {
            error.Set(lucenejs::Error::kInvalidInput,
                      "document.fields must be an array, one entry per field");
            return;
        }
        pp::VarArray document_fields = (pp::VarArray)document_fields_var;
        
        uint32_t num_fields = document_fields.GetLength();
        if (num_fields == 0) {
            error.Set(lucenejs::Error::kInvalidInput,
                      "Document may not be empty");
            return;
        }
        
        L::DocumentPtr doc = L::newLucene<L::Document>();
        doc->setBoost(document_boost);
        
        
        for (uint32_t j=0; j<num_fields; ++j) {
            pp::Var doc_field_var = document_fields.Get(j);
            L::FieldablePtr field = parseField(doc_field_var, error);
            if (!error.IsSuccess()) {
                return;
            }
            
            cout << "adding field to document" << endl;
            doc->add(field);
            cout << "added field to document" << endl;
        }
        
        documents.push_back(doc);
    }
}

lucenejs::LuceneJSIndex::LuceneJSIndex(const string& index_id,
                                       const string& persistent_index_root_directory,
                                       bool persistent,
                                       lucenejs::Error& error) {
    try {

        this->searcher_refresh_pending = true;
        this->index_id = index_id;

        L::StandardAnalyzerPtr analyzer = getDefaultAnalyzer();

        if (persistent) {
            string index_path = persistent_index_root_directory + kPathSeparator + index_id;
            L::String dir_path = stdStringToLucenceString(index_path);

            L::LockFactoryPtr lock_factory
                = L::newLucene<L::SingleInstanceLockFactory>();
            this->index_directory = L::FSDirectory::open(dir_path, lock_factory);

        } else {
            this->index_directory = L::newLucene<L::RAMDirectory>();
        }

        // create a new index if one does not already exist,
        // open the existing one to append if an index already exists.
        this->index_writer = L::newLucene<L::IndexWriter>(index_directory,
                                                          analyzer,
                                                          kMaxFieldLength);
        
    } catch (const L::LuceneException& le) {
        stringstream ss;
        ss << "Lucene exception in creating a new index: "
            << L::StringUtils::toUTF8(le.getError())
            << "(" << le.getType() << ")";
        
        error.Set(lucenejs::Error::kLuceneException, ss.str());
        return;
    } catch (const std::exception& ex) {
        stringstream ss;
        ss << "Exception in creating a new index: " << ex.what();
        error.Set(lucenejs::Error::kGenericException, ss.str());
        return;
    } catch(...) {
        error.Set(lucenejs::Error::kGenericError, "Error in creating a new index");
        return;
    }
}

L::SearcherPtr lucenejs::LuceneJSIndex::GetFreshSearcher() {
    if (!this->searcher_refresh_pending) {
        return this->searcher;
    }
    
    this->searcher_refresh_pending = false;
    if (this->searcher) {
        this->searcher->close();
    }
    
    this->searcher = L::newLucene<L::IndexSearcher>(this->index_directory, true);
    return this->searcher;
}

lucenejs::LuceneJSIndex::~LuceneJSIndex() {
    if (this->index_writer) {
        this->index_writer->close();
    }
    if (this->searcher) {
        this->searcher->close();
    }
}

void lucenejs::LuceneJSIndex::Optimize(lucenejs::Error& error) {
    error.Reset();
    
    if (this->index_writer) {
        this->index_writer->optimize();
        this->searcher_refresh_pending = true;
    }
}

void lucenejs::LuceneJSIndex::AddDocuments(const pp::VarArray& document_list,
                                          lucenejs::Error& error) {
    try {
        error.Reset();
        if (!this->index_writer) {
            error.Set(lucenejs::Error::kInvalidState,
                      "Can't add document without an index_writer present");
            return;
        }
    
        uint32_t num_documents = document_list.GetLength();
        if (num_documents == 0) {
            return;
        }
        
        vector<L::DocumentPtr> documents;
        parseRequestDocuments(document_list, documents, error);
        if (!error.IsSuccess()) {
            return;
        }
        
        for(vector<L::DocumentPtr>::iterator it = documents.begin();
            it != documents.end();
            ++it) {
            L::DocumentPtr doc = *it;
            this->index_writer->addDocument(doc);
        }
        
        this->index_writer->commit();
        this->searcher_refresh_pending = true;
        
    } catch (const L::LuceneException& le) {
        stringstream ss;
        ss << "Lucene exception in indexing: "
            << L::StringUtils::toUTF8(le.getError())
            << "(" << le.getType() << ")";
        
        error.Set(lucenejs::Error::kLuceneException, ss.str());
        return;
    } catch (const std::exception& ex) {
        stringstream ss;
        ss << "Exception in indexing: " << ex.what();
        error.Set(lucenejs::Error::kGenericException, ss.str());
        return;
    } catch(...) {
        error.Set(lucenejs::Error::kGenericError, "Error in indexing");
        return;
    }
}

static L::QueryPtr parseQuery(const pp::VarArray& query,
                              const char* const default_match_type,
                              lucenejs::Error& error) {
    
    error.Reset();
    L::BooleanQueryPtr final_query = L::newLucene<L::BooleanQuery>();
    
    try {
        L::StandardAnalyzerPtr analyzer = getDefaultAnalyzer();
        
        uint32_t num_fields = query.GetLength();
        if (num_fields == 0) {
            error.Set(lucenejs::Error::kInvalidInput,
                      "Query may not be empty");
            return final_query;
        }
        
        for (uint32_t i=0; i<num_fields; ++i) {
            pp::Var query_field_var = query.Get(i);
            if (!query_field_var.is_dictionary()) {
                error.Set(lucenejs::Error::kInvalidInput,
                          "Query field must be a dictionary");
                return final_query;
            }
            
            pp::VarDictionary query_field = (pp::VarDictionary)query_field_var;
            
            pp::Var field_name_var = query_field.Get(kDocumentFieldName);
            if (!field_name_var.is_string()) {
                error.Set(lucenejs::Error::kInvalidInput,
                          "Field name must be a string");
                return final_query;
            }
            
            string field_name = field_name_var.AsString();
            if (field_name.empty()) {
                error.Set(lucenejs::Error::kInvalidInput,
                          "Field name may not be empty");
                return final_query;
            }
            
            pp::Var match_type_var = query_field.Get(kQueryPropertyMatchType);
            string match_type = default_match_type;
            if (match_type_var.is_string()) {
                match_type = match_type_var.AsString();
            }
            
            pp::Var field_value_var = query_field.Get(kDocumentFieldValue);
            
            L::QueryPtr query = parseFieldQuery(field_name,
                                                match_type,
                                                query_field,
                                                field_value_var,
                                                analyzer,
                                                error);
            if (!error.IsSuccess()) {
                return final_query;
            }
            
            // OR all fields
            final_query->add(query, L::BooleanClause::SHOULD);
        }
        
        return final_query;
        
    } catch (const L::LuceneException& le) {
        stringstream ss;
        ss << "Lucene exception in parsing query: "
            << L::StringUtils::toUTF8(le.getError())
            << "(" << le.getType() << ")";
        
        error.Set(lucenejs::Error::kLuceneException, ss.str());
        return final_query;
    } catch (const std::exception& ex) {
        stringstream ss;
        ss << "Exception in parsing query: " << ex.what();
        error.Set(lucenejs::Error::kGenericException, ss.str());
        return final_query;
    } catch(...) {
        error.Set(lucenejs::Error::kGenericError, "Error in parsing query");
        return final_query;
    }
    
    return final_query;
}

pp::VarArray
lucenejs::LuceneJSIndex::Search(const pp::VarArray& query,
                                const set<string>& returned_doc_fields,
                                lucenejs::Error& error) {
    
    error.Reset();
    pp::VarArray results;
    
    try {
        
        L::QueryPtr final_query = parseQuery(query,
                                             kQueryMatchTypeFullText,
                                             error);
        if (!error.IsSuccess()) {
            return results;
        }
    
        L::SearcherPtr fresh_searcher = this->GetFreshSearcher();
        
        long search_start_time = lucenejs::Util::GetCurrentTime();
        
        L::TopDocsPtr top_docs = fresh_searcher->search(final_query,
                                                        kDefaultMaxSearchResults);
        L::Collection<L::ScoreDocPtr> hits = top_docs->scoreDocs;
        
        long search_end_time = lucenejs::Util::GetCurrentTime();
        long search_time_taken = search_end_time - search_start_time;
        cout << "Searching `" << L::StringUtils::toUTF8(final_query->toString())
            << "` took " << search_time_taken << " ms" << endl;
    
        uint32_t num_hits = hits.size();
        cout << "num hits " << num_hits << endl;
    
        results.SetLength(num_hits);
    
        for (uint32_t i=0; i<num_hits; ++i) {
            L::DocumentPtr doc = fresh_searcher->doc(hits[i]->doc);
            pp::VarDictionary doc_dict = mapDocumentToDictionary(doc, returned_doc_fields);
            results.Set(i, doc_dict);
        }
    
    } catch (const L::LuceneException& le) {
        stringstream ss;
        ss << "Lucene exception in search: "
            << L::StringUtils::toUTF8(le.getError())
            << "(" << le.getType() << ")";
        
        error.Set(lucenejs::Error::kLuceneException, ss.str());
        return results;
    } catch (const std::exception& ex) {
        stringstream ss;
        ss << "Exception in search: " << ex.what();
        error.Set(lucenejs::Error::kGenericException, ss.str());
        return results;
    } catch(...) {
        error.Set(lucenejs::Error::kGenericError, "Error in search");
        return results;
    }
    return results;
}


void lucenejs::LuceneJSIndex::Delete(const pp::VarArray& query,
                                     lucenejs::Error& error) {
    
    error.Reset();
    
    try {
        
        L::QueryPtr final_query = parseQuery(query,
                                             kQueryMatchTypeExactEqual,
                                             error);
        if (!error.IsSuccess()) {
            return;
        }
        
        this->index_writer->deleteDocuments(final_query);
        this->index_writer->commit();
        this->searcher_refresh_pending = true;
        
    } catch (const L::LuceneException& le) {
        stringstream ss;
        ss << "Lucene exception in deletion: "
            << L::StringUtils::toUTF8(le.getError())
            << "(" << le.getType() << ")";
        
        error.Set(lucenejs::Error::kLuceneException, ss.str());
        return;
    } catch (const std::exception& ex) {
        stringstream ss;
        ss << "Exception in deletion: " << ex.what();
        error.Set(lucenejs::Error::kGenericException, ss.str());
        return;
    } catch(...) {
        error.Set(lucenejs::Error::kGenericError, "Error in deletion");
        return;
    }
}

void lucenejs::LuceneJSIndex::AddOrUpdateDocuments(const pp::VarArray& document_list,
                                                   const std::string& doc_id_field_name,
                                                   lucenejs::Error& error) {
    error.Reset();
    
    try {
        
        if (!this->index_writer) {
            error.Set(lucenejs::Error::kInvalidState,
                      "Can't upsert document without an index_writer present");
            return;
        }
        
        uint32_t num_documents = document_list.GetLength();
        if (num_documents == 0) {
            return;
        }
        
        vector<L::DocumentPtr> documents;
        parseRequestDocuments(document_list, documents, error);
        if (!error.IsSuccess()) {
            return;
        }
        
        L::String id_field_name_unicode
            = stdStringToLucenceString(doc_id_field_name);
        L::StandardAnalyzerPtr analyzer = getDefaultAnalyzer();
        L::TermPtr id_term;
        
        for(vector<L::DocumentPtr>::iterator it = documents.begin();
            it != documents.end();
            ++it) {
            
            L::DocumentPtr doc = *it;
            L::String id_value = doc->get(id_field_name_unicode);
            
            cout << "upserting ["
            << L::StringUtils::toUTF8(id_field_name_unicode)
            << "=" << L::StringUtils::toUTF8(id_value)
            << "]" << endl;
            
            
            id_term = L::newLucene<L::Term>(id_field_name_unicode, id_value);
            this->index_writer->updateDocument(id_term, doc, analyzer);
        }
        
        this->index_writer->commit();
        this->searcher_refresh_pending = true;
        
    }  catch (const L::LuceneException& le) {
        stringstream ss;
        ss << "Lucene exception in UPSERT: "
        << L::StringUtils::toUTF8(le.getError())
        << "(" << le.getType() << ")";
        
        error.Set(lucenejs::Error::kLuceneException, ss.str());
        return;
    } catch (const std::exception& ex) {
        stringstream ss;
        ss << "Exception in UPSERT: " << ex.what();
        error.Set(lucenejs::Error::kGenericException, ss.str());
        return;
    } catch(...) {
        error.Set(lucenejs::Error::kGenericError, "Error in UPSERT");
        return;
    }
}

void lucenejs::LuceneJSIndex::GetIndexStatistics(pp::VarDictionary& statistics,
                                                 lucenejs::Error& error) {
    error.Reset();
    
    try {
        L::IndexReaderPtr index_reader
            = L::IndexReader::open(this->index_directory, true);
        int32_t all_docs = index_reader->maxDoc();
        int32_t deleted_docs = index_reader->numDeletedDocs();
        int32_t live_docs = index_reader->numDocs();
        long unique_term_count = index_reader->getUniqueTermCount();
        
        statistics.Set(kStatisticsFieldNumAllDocs, all_docs);
        statistics.Set(kStatisticsFieldNumLiveDocs, live_docs);
        statistics.Set(kStatisticsFieldNumDeletedDocs, deleted_docs);
        statistics.Set(kStatisticsFieldNumUniqueTerms, (double)unique_term_count);
        
    } catch (const L::LuceneException& le) {
        stringstream ss;
        ss << "Lucene exception in GetIndexStatistics: "
        << L::StringUtils::toUTF8(le.getError())
        << "(" << le.getType() << ")";
        
        error.Set(lucenejs::Error::kLuceneException, ss.str());
        return;
    } catch (const std::exception& ex) {
        stringstream ss;
        ss << "Exception in GetIndexStatistics: " << ex.what();
        error.Set(lucenejs::Error::kGenericException, ss.str());
        return;
    } catch(...) {
        error.Set(lucenejs::Error::kGenericError, "Error in GetIndexStatistics");
        return;
    }
}