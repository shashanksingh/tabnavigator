#include "stdio.h"
#include "sys/stat.h"
#include "sys/statvfs.h"

int crash()
{
    int* i = 0;
    fprintf( stderr, "Craching now." );
    return (*i = 1);
}


/* <AZ> Deliberately cause a crash. */
int statvfs(const char *path, struct statvfs *buf)
{
    fprintf( stderr, "statvfs() is only a stub." );
    return crash();
}


int fstatvfs(int fd, struct statvfs *buf)
{
    fprintf( stderr, "fstatvfs() is only a stub." );
    return crash();
}

int fchmodat(int fd, const char *path, mode_t mode, int flag) {
    fprintf( stderr, "fchmodat() is only a stub." );
    return crash();
}
