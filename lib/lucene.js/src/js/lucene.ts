/// <reference path='../../../../chrome/common/common.ts'/>

module Lucene {
    export class LuceneJS {

        private static PERSISTENT_FS_REQUESTED_BYTE_SIZE = 16 * 1024 * 1024;
        private static logger: Common.Logger = new Common.Logger('lucene-js');


        public static IndexType = {
            IN_MEMORY: 'IN_MEMORY',
            PERSISTENT: 'PERSISTENT'
        };

        public static MatchType = {
            EXACT_EQUAL: 'EXACT_EQUAL',
            FULL_TEXT: 'FULL_TEXT',
            RANGE: 'RANGE'
        };

        public static isInitialized() {
            return PNaClModuleManager.isInitialized();
        }

        public static openOrCreateIndex(indexId: string,
                                        indexType: string /*LuceneJS.IndexType*/,
                                        callback: Common.GenericCallback) {

            PNaClModuleManager.initialize(function(error: Error){
                if (!!error) {
                    callback(error);
                    return;
                }

                var persistent: boolean = indexType === LuceneJS.IndexType.PERSISTENT;
                if (persistent) {
                    LuceneJS.ensurePersistentStoragePermission(function(error: Error){
                        if (!!error) {
                            callback(error);
                            return;
                        }

                        LuceneJS.ensureIndexExists(indexId, persistent, callback);
                    });
                } else {
                    LuceneJS.ensureIndexExists(indexId, persistent, callback);
                }
            });
        }

        public static addDocumentsToIndex(indexId: string,
                                         documents:any[],
                                         callback: Common.GenericCallback) {

            var message = new Common.PostMessage(
                Common.Commands.lucenejs.INDEX_DOCUMENT,
                {
                    indexId: indexId,
                    documents: documents
                }
            );

            this.makeRequest(message, callback);
        }

        public static search(indexId: string,
                             query: any[],
                             queryResultFields: string[],
                             callback: Common.GenericCallback) {

            if (query.length === 0) {
                this.logger.warn('Received empty query, returning empty results.');
                callback(null, []);
                return;
            }

            // lucene does not like fields with empty query on them,
            // query parser will throw an exception for such requests
            query = query.filter(function(queryField) {
                return !!queryField.value;
            });

            if (query.length === 0) {
                this.logger.warn('Query became empty after filtering fields with empty value.');
                callback(null, []);
                return;
            }

            var message = new Common.PostMessage(
                Common.Commands.lucenejs.SEARCH,
                {
                    indexId: indexId,
                    query: query,
                    queryResultFields: queryResultFields
                }
            );

            this.makeRequest(message, callback);
        }

        public static deleteDocuments(indexId: string,
                                      deletionQuery: any[],
                                      callback: Common.GenericCallback) {

            var message = new Common.PostMessage(
                Common.Commands.lucenejs.DELETE,
                {
                    indexId: indexId,
                    query: deletionQuery
                }
            );

            this.makeRequest(message, callback);
        }

        public static addOrUpdateDocuments(indexId: string,
                                           documents:any[],
                                           documentIdFieldName: string,
                                           callback: Common.GenericCallback) {

            var message = new Common.PostMessage(
                Common.Commands.lucenejs.ADD_OR_UPDATE_DOCUMENTS,
                {
                    indexId: indexId,
                    documents: documents,
                    docIdFieldName: documentIdFieldName
                }
            );

            this.makeRequest(message, callback);
        }

        public static optimizeIndex(indexId: string,
                                    callback: Common.GenericCallback) {
            var message = new Common.PostMessage(
                Common.Commands.lucenejs.OPTIMIZE_INDEX,
                {
                    indexId: indexId
                }
            );

            this.makeRequest(message, callback);
        }

        public static getIndexStatistics(indexId: string,
                                    callback: Common.GenericCallback) {
            var message = new Common.PostMessage(
                Common.Commands.lucenejs.GET_INDEX_STATISTICS,
                {
                    indexId: indexId
                }
            );

            this.makeRequest(message, callback);
        }

        private static makeRequest(message: Common.PostMessage,
                                   callback: Common.GenericCallback) {

            if (!PNaClModuleManager.isInitialized()) {
                callback(new Error('LuceneJS is not initialized'));
                return;
            }

            PNaClModuleManager.makePNaClRequest(message, function(responseMessage: Common.PostMessage){
                var responseBody = responseMessage.data;
                var errorMessage: string = responseBody.error ? responseBody.error : null;

                if (!!errorMessage) {
                    var error: Error = new Error(errorMessage);
                    callback(error);
                    return;
                }

                callback(null, responseBody.queryResults);
            });
        }

        private static ensureIndexExists(indexId: string, persistent: boolean,
                                         callback: Common.GenericCallback) {
            var message = new Common.PostMessage(
                Common.Commands.lucenejs.GET_OR_CREATE_INDEX,
                {
                    indexId: indexId,
                    persistent: !!persistent
                }
            );

            PNaClModuleManager.makePNaClRequest(message, function(responseMessage: Common.PostMessage){
                var responseBody = responseMessage.data;
                var errorMessage: string = responseBody.error ? responseBody.error : null;
                var error: Error = null;

                if (!!errorMessage) {
                    LuceneJS.logger.error('Error in initializing index', indexId, persistent, error);
                    error = new Error(errorMessage);
                }
                callback(error);
            });

        }

        private static ensurePersistentStoragePermission(callback: Common.GenericCallback) {
            (<any>navigator).webkitPersistentStorage.requestQuota(
                LuceneJS.PERSISTENT_FS_REQUESTED_BYTE_SIZE,
                function(bytesGranted: number) {
                    callback(null);
                },
                function(e) {
                    LuceneJS.logger.error('Error in allocating persistent storage');
                    callback(e);
                });
        }
    }

    class PNaClModuleManager {
        private static PNACL_ROOT_ID = '28489f58-1293-4d39-85bd-4035b30b1569';
        private static PNACL_MANIFEST_FILE_NAME = document.location.protocol.indexOf('http') === 0 ? '../src/lucene.js.nmf?r=13' : 'lib/js/lucenejs/lucene.js.nmf?r=13';
        private static PNACL_MIME_TYPE = 'application/x-pnacl';

        private static PNaClStateTransitionEvent = {
            LOAD: 'load',
            ABORT: 'abort',
            ERROR: 'error',
            CRASH: 'crash'
        };

        private static logger: Common.Logger = new Common.Logger('pnacl-module-manager');

        private static backendInitialized: boolean = false;
        private static backendInitError: Error = null;
        private static pendingInitCallbacks: Common.GenericCallback[] = [];

        public static shutdown() {
            var pNaClRoot: HTMLElement = document.getElementById(PNaClModuleManager.PNACL_ROOT_ID);
            if (!pNaClRoot) {
                PNaClModuleManager.logger.warn('no PNaCl root found when trying to shut down LuceneJS');
                return;
            }
            pNaClRoot.parentElement.removeChild(pNaClRoot);
        }

        public static initialize(callback: Common.GenericCallback) {
            if (PNaClModuleManager.backendInitialized) {
                callback(PNaClModuleManager.backendInitError);
                return;
            }

            PNaClModuleManager.pendingInitCallbacks.push(callback);

            PNaClModuleManager.onDocumentReady(function(){
                PNaClModuleManager.loadPNaClModule(function(error: Error){
                    PNaClModuleManager.backendInitialized = true;
                    PNaClModuleManager.backendInitError = !!error ? error : null;

                    PNaClModuleManager.executeAllPendingInitCallbacks(error);
                });
            });

        }

        public static isInitialized(): boolean {
            return !!this.backendInitialized && !this.backendInitError;
        }

        public static makePNaClRequest(message: Common.PostMessage,
                                       responseCallback: Common.PostMessageCallback) {

            var pNaClRoot: HTMLElement = PNaClModuleManager.getPNaClRoot();
            var pNaClModule: HTMLElement = PNaClModuleManager.getPNaClModuleElement();

            var conversationId = message.conversationId;
            pNaClRoot.addEventListener('message', function onPostMessageResponse(event: MessageEvent){
                var responseMessage: Common.PostMessage = event.data;
                if (responseMessage.conversationId !== conversationId) {
                    return;
                }

                pNaClRoot.removeEventListener('message', onPostMessageResponse);
                responseCallback(responseMessage);
            }, true);

            (<any>pNaClModule).postMessage(message);
        }

        private static onDocumentReady(callback: Common.VoidCallback) {
            if (document.readyState === Common.DocumentState.INTERACTIVE
                || document.readyState === Common.DocumentState.COMPLETE) {
                callback();
                return;
            }
            document.addEventListener('DOMContentLoaded', callback);
        }

        private static loadPNaClModule(callback: Common.GenericCallback) {
            var pNaClRoot: HTMLElement = document.getElementById(PNaClModuleManager.PNACL_ROOT_ID);
            if (pNaClRoot) {
                pNaClRoot.parentNode.removeChild(pNaClRoot);
            }

            pNaClRoot = document.createElement('div');
            pNaClRoot.setAttribute('id', PNaClModuleManager.PNACL_ROOT_ID);

            function handleLoadEvent(event) {
                Object.keys(PNaClModuleManager.PNaClStateTransitionEvent).forEach(function(key: string){
                    var eventName: string = PNaClModuleManager.PNaClStateTransitionEvent[key];
                    pNaClRoot.addEventListener(eventName, handleLoadEvent);
                });

                pNaClRoot.addEventListener('crash', function(e){
                    PNaClModuleManager.onPNaClModuleRuntimeCrash(<Event>e);
                }, true);

                var errorMessage;

                switch (event.type) {
                    case PNaClModuleManager.PNaClStateTransitionEvent.LOAD:
                        callback(null);
                        break;

                    case PNaClModuleManager.PNaClStateTransitionEvent.ABORT:
                        errorMessage = 'PNaCl module load aborted by the user';
                    case PNaClModuleManager.PNaClStateTransitionEvent.ERROR:
                        errorMessage = 'Error in loading PNaCl module';
                    case PNaClModuleManager.PNaClStateTransitionEvent.CRASH:
                        errorMessage = 'PNaCl module crashed!';

                        errorMessage = PNaClModuleManager.getExtendedPNaClErrorMessage(errorMessage);
                        PNaClModuleManager.logger.warn(errorMessage, event);

                        var error = new Error(errorMessage);
                        callback(error);
                        break;

                    default:
                        PNaClModuleManager.logger.warn('unhandled PNaCl module event', event.type);
                        break;
                }
            }

            Object.keys(PNaClModuleManager.PNaClStateTransitionEvent).forEach(function(key: string){
                var eName: string = PNaClModuleManager.PNaClStateTransitionEvent[key];
                pNaClRoot.addEventListener(eName, handleLoadEvent, true);
            });

            var pNaClEmbedElement: HTMLEmbedElement = document.createElement('embed');
            pNaClEmbedElement.setAttribute('width', '0');
            pNaClEmbedElement.setAttribute('height', '0');
            pNaClEmbedElement.setAttribute('type', PNaClModuleManager.PNACL_MIME_TYPE);
            pNaClEmbedElement.setAttribute('src', PNaClModuleManager.PNACL_MANIFEST_FILE_NAME);

            pNaClRoot.appendChild(pNaClEmbedElement);

            document.body.appendChild(pNaClRoot);

        }

        private static executeAllPendingInitCallbacks(error: Error) {
            var pendingCallback: Common.GenericCallback;

            while (pendingCallback = PNaClModuleManager.pendingInitCallbacks.shift()) {
                try {
                    pendingCallback(error);
                } catch(e) {
                    PNaClModuleManager.logger.error('Error in LuceneJS initialization callback, ignored', e);
                }
            }
        }

        private static onPNaClModuleRuntimeCrash(event: Event) {
            PNaClModuleManager.logger.error('PNaCl module crashed at run time!', event);
            PNaClModuleManager.loadPNaClModule(function (error: Error) {
                if (!!error) {
                    PNaClModuleManager.logger.error('Error in trying to recover PNaCl after crash', error);
                } else {
                    PNaClModuleManager.logger.log('Successfully recovered PNaCl module after runtime crash');
                }
            });
        }

        private static getPNaClRoot(): HTMLElement {
            return document.getElementById(PNaClModuleManager.PNACL_ROOT_ID);
        }

        private static getPNaClModuleElement(): HTMLElement {
            var root: HTMLElement = this.getPNaClRoot();
            if (!root) {
                return null;
            }
            return root.getElementsByTagName('embed')[0];
        }

        private static getExtendedPNaClErrorMessage(prefix: string) {
            var pNaClModuleElement: HTMLElement = PNaClModuleManager.getPNaClModuleElement();
            return Common.Util.printf(
                '{prefix}:: lastError: {lastError}, exitStatus: {exitStatus}',
                {
                    prefix: prefix,
                    lastError: (<any>pNaClModuleElement).lastError,
                    exitStatus: (<any>pNaClModuleElement).exitStatus
                }
            );
        }

    }
}