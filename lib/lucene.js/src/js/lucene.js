/// <reference path='../../../../chrome/common/common.ts'/>
var Lucene;
(function (Lucene) {
    var LuceneJS = (function () {
        function LuceneJS() {
        }
        LuceneJS.isInitialized = function () {
            return PNaClModuleManager.isInitialized();
        };
        LuceneJS.openOrCreateIndex = function (indexId, indexType /*LuceneJS.IndexType*/, callback) {
            PNaClModuleManager.initialize(function (error) {
                if (!!error) {
                    callback(error);
                    return;
                }
                var persistent = indexType === LuceneJS.IndexType.PERSISTENT;
                if (persistent) {
                    LuceneJS.ensurePersistentStoragePermission(function (error) {
                        if (!!error) {
                            callback(error);
                            return;
                        }
                        LuceneJS.ensureIndexExists(indexId, persistent, callback);
                    });
                }
                else {
                    LuceneJS.ensureIndexExists(indexId, persistent, callback);
                }
            });
        };
        LuceneJS.addDocumentsToIndex = function (indexId, documents, callback) {
            var message = new Common.PostMessage(Common.Commands.lucenejs.INDEX_DOCUMENT, {
                indexId: indexId,
                documents: documents
            });
            this.makeRequest(message, callback);
        };
        LuceneJS.search = function (indexId, query, queryResultFields, callback) {
            if (query.length === 0) {
                this.logger.warn('Received empty query, returning empty results.');
                callback(null, []);
                return;
            }
            // lucene does not like fields with empty query on them,
            // query parser will throw an exception for such requests
            query = query.filter(function (queryField) {
                return !!queryField.value;
            });
            if (query.length === 0) {
                this.logger.warn('Query became empty after filtering fields with empty value.');
                callback(null, []);
                return;
            }
            var message = new Common.PostMessage(Common.Commands.lucenejs.SEARCH, {
                indexId: indexId,
                query: query,
                queryResultFields: queryResultFields
            });
            this.makeRequest(message, callback);
        };
        LuceneJS.deleteDocuments = function (indexId, deletionQuery, callback) {
            var message = new Common.PostMessage(Common.Commands.lucenejs.DELETE, {
                indexId: indexId,
                query: deletionQuery
            });
            this.makeRequest(message, callback);
        };
        LuceneJS.addOrUpdateDocuments = function (indexId, documents, documentIdFieldName, callback) {
            var message = new Common.PostMessage(Common.Commands.lucenejs.ADD_OR_UPDATE_DOCUMENTS, {
                indexId: indexId,
                documents: documents,
                docIdFieldName: documentIdFieldName
            });
            this.makeRequest(message, callback);
        };
        LuceneJS.optimizeIndex = function (indexId, callback) {
            var message = new Common.PostMessage(Common.Commands.lucenejs.OPTIMIZE_INDEX, {
                indexId: indexId
            });
            this.makeRequest(message, callback);
        };
        LuceneJS.getIndexStatistics = function (indexId, callback) {
            var message = new Common.PostMessage(Common.Commands.lucenejs.GET_INDEX_STATISTICS, {
                indexId: indexId
            });
            this.makeRequest(message, callback);
        };
        LuceneJS.makeRequest = function (message, callback) {
            if (!PNaClModuleManager.isInitialized()) {
                callback(new Error('LuceneJS is not initialized'));
                return;
            }
            PNaClModuleManager.makePNaClRequest(message, function (responseMessage) {
                var responseBody = responseMessage.data;
                var errorMessage = responseBody.error ? responseBody.error : null;
                if (!!errorMessage) {
                    var error = new Error(errorMessage);
                    callback(error);
                    return;
                }
                callback(null, responseBody.queryResults);
            });
        };
        LuceneJS.ensureIndexExists = function (indexId, persistent, callback) {
            var message = new Common.PostMessage(Common.Commands.lucenejs.GET_OR_CREATE_INDEX, {
                indexId: indexId,
                persistent: !!persistent
            });
            PNaClModuleManager.makePNaClRequest(message, function (responseMessage) {
                var responseBody = responseMessage.data;
                var errorMessage = responseBody.error ? responseBody.error : null;
                var error = null;
                if (!!errorMessage) {
                    LuceneJS.logger.error('Error in initializing index', indexId, persistent, error);
                    error = new Error(errorMessage);
                }
                callback(error);
            });
        };
        LuceneJS.ensurePersistentStoragePermission = function (callback) {
            navigator.webkitPersistentStorage.requestQuota(LuceneJS.PERSISTENT_FS_REQUESTED_BYTE_SIZE, function (bytesGranted) {
                callback(null);
            }, function (e) {
                LuceneJS.logger.error('Error in allocating persistent storage');
                callback(e);
            });
        };
        LuceneJS.PERSISTENT_FS_REQUESTED_BYTE_SIZE = 16 * 1024 * 1024;
        LuceneJS.logger = new Common.Logger('lucene-js');
        LuceneJS.IndexType = {
            IN_MEMORY: 'IN_MEMORY',
            PERSISTENT: 'PERSISTENT'
        };
        LuceneJS.MatchType = {
            EXACT_EQUAL: 'EXACT_EQUAL',
            FULL_TEXT: 'FULL_TEXT',
            RANGE: 'RANGE'
        };
        return LuceneJS;
    })();
    Lucene.LuceneJS = LuceneJS;
    var PNaClModuleManager = (function () {
        function PNaClModuleManager() {
        }
        PNaClModuleManager.shutdown = function () {
            var pNaClRoot = document.getElementById(PNaClModuleManager.PNACL_ROOT_ID);
            if (!pNaClRoot) {
                PNaClModuleManager.logger.warn('no PNaCl root found when trying to shut down LuceneJS');
                return;
            }
            pNaClRoot.parentElement.removeChild(pNaClRoot);
        };
        PNaClModuleManager.initialize = function (callback) {
            if (PNaClModuleManager.backendInitialized) {
                callback(PNaClModuleManager.backendInitError);
                return;
            }
            PNaClModuleManager.pendingInitCallbacks.push(callback);
            PNaClModuleManager.onDocumentReady(function () {
                PNaClModuleManager.loadPNaClModule(function (error) {
                    PNaClModuleManager.backendInitialized = true;
                    PNaClModuleManager.backendInitError = !!error ? error : null;
                    PNaClModuleManager.executeAllPendingInitCallbacks(error);
                });
            });
        };
        PNaClModuleManager.isInitialized = function () {
            return !!this.backendInitialized && !this.backendInitError;
        };
        PNaClModuleManager.makePNaClRequest = function (message, responseCallback) {
            var pNaClRoot = PNaClModuleManager.getPNaClRoot();
            var pNaClModule = PNaClModuleManager.getPNaClModuleElement();
            var conversationId = message.conversationId;
            pNaClRoot.addEventListener('message', function onPostMessageResponse(event) {
                var responseMessage = event.data;
                if (responseMessage.conversationId !== conversationId) {
                    return;
                }
                pNaClRoot.removeEventListener('message', onPostMessageResponse);
                responseCallback(responseMessage);
            }, true);
            pNaClModule.postMessage(message);
        };
        PNaClModuleManager.onDocumentReady = function (callback) {
            if (document.readyState === Common.DocumentState.INTERACTIVE
                || document.readyState === Common.DocumentState.COMPLETE) {
                callback();
                return;
            }
            document.addEventListener('DOMContentLoaded', callback);
        };
        PNaClModuleManager.loadPNaClModule = function (callback) {
            var pNaClRoot = document.getElementById(PNaClModuleManager.PNACL_ROOT_ID);
            if (pNaClRoot) {
                pNaClRoot.parentNode.removeChild(pNaClRoot);
            }
            pNaClRoot = document.createElement('div');
            pNaClRoot.setAttribute('id', PNaClModuleManager.PNACL_ROOT_ID);
            function handleLoadEvent(event) {
                Object.keys(PNaClModuleManager.PNaClStateTransitionEvent).forEach(function (key) {
                    var eventName = PNaClModuleManager.PNaClStateTransitionEvent[key];
                    pNaClRoot.addEventListener(eventName, handleLoadEvent);
                });
                pNaClRoot.addEventListener('crash', function (e) {
                    PNaClModuleManager.onPNaClModuleRuntimeCrash(e);
                }, true);
                var errorMessage;
                switch (event.type) {
                    case PNaClModuleManager.PNaClStateTransitionEvent.LOAD:
                        callback(null);
                        break;
                    case PNaClModuleManager.PNaClStateTransitionEvent.ABORT:
                        errorMessage = 'PNaCl module load aborted by the user';
                    case PNaClModuleManager.PNaClStateTransitionEvent.ERROR:
                        errorMessage = 'Error in loading PNaCl module';
                    case PNaClModuleManager.PNaClStateTransitionEvent.CRASH:
                        errorMessage = 'PNaCl module crashed!';
                        errorMessage = PNaClModuleManager.getExtendedPNaClErrorMessage(errorMessage);
                        PNaClModuleManager.logger.warn(errorMessage, event);
                        var error = new Error(errorMessage);
                        callback(error);
                        break;
                    default:
                        PNaClModuleManager.logger.warn('unhandled PNaCl module event', event.type);
                        break;
                }
            }
            Object.keys(PNaClModuleManager.PNaClStateTransitionEvent).forEach(function (key) {
                var eName = PNaClModuleManager.PNaClStateTransitionEvent[key];
                pNaClRoot.addEventListener(eName, handleLoadEvent, true);
            });
            var pNaClEmbedElement = document.createElement('embed');
            pNaClEmbedElement.setAttribute('width', '0');
            pNaClEmbedElement.setAttribute('height', '0');
            pNaClEmbedElement.setAttribute('type', PNaClModuleManager.PNACL_MIME_TYPE);
            pNaClEmbedElement.setAttribute('src', PNaClModuleManager.PNACL_MANIFEST_FILE_NAME);
            pNaClRoot.appendChild(pNaClEmbedElement);
            document.body.appendChild(pNaClRoot);
        };
        PNaClModuleManager.executeAllPendingInitCallbacks = function (error) {
            var pendingCallback;
            while (pendingCallback = PNaClModuleManager.pendingInitCallbacks.shift()) {
                try {
                    pendingCallback(error);
                }
                catch (e) {
                    PNaClModuleManager.logger.error('Error in LuceneJS initialization callback, ignored', e);
                }
            }
        };
        PNaClModuleManager.onPNaClModuleRuntimeCrash = function (event) {
            PNaClModuleManager.logger.error('PNaCl module crashed at run time!', event);
            PNaClModuleManager.loadPNaClModule(function (error) {
                if (!!error) {
                    PNaClModuleManager.logger.error('Error in trying to recover PNaCl after crash', error);
                }
                else {
                    PNaClModuleManager.logger.log('Successfully recovered PNaCl module after runtime crash');
                }
            });
        };
        PNaClModuleManager.getPNaClRoot = function () {
            return document.getElementById(PNaClModuleManager.PNACL_ROOT_ID);
        };
        PNaClModuleManager.getPNaClModuleElement = function () {
            var root = this.getPNaClRoot();
            if (!root) {
                return null;
            }
            return root.getElementsByTagName('embed')[0];
        };
        PNaClModuleManager.getExtendedPNaClErrorMessage = function (prefix) {
            var pNaClModuleElement = PNaClModuleManager.getPNaClModuleElement();
            return Common.Util.printf('{prefix}:: lastError: {lastError}, exitStatus: {exitStatus}', {
                prefix: prefix,
                lastError: pNaClModuleElement.lastError,
                exitStatus: pNaClModuleElement.exitStatus
            });
        };
        PNaClModuleManager.PNACL_ROOT_ID = '28489f58-1293-4d39-85bd-4035b30b1569';
        PNaClModuleManager.PNACL_MANIFEST_FILE_NAME = document.location.protocol.indexOf('http') === 0 ? '../src/lucene.js.nmf?r=13' : 'lib/js/lucenejs/lucene.js.nmf?r=13';
        PNaClModuleManager.PNACL_MIME_TYPE = 'application/x-pnacl';
        PNaClModuleManager.PNaClStateTransitionEvent = {
            LOAD: 'load',
            ABORT: 'abort',
            ERROR: 'error',
            CRASH: 'crash'
        };
        PNaClModuleManager.logger = new Common.Logger('pnacl-module-manager');
        PNaClModuleManager.backendInitialized = false;
        PNaClModuleManager.backendInitError = null;
        PNaClModuleManager.pendingInitCallbacks = [];
        return PNaClModuleManager;
    })();
})(Lucene || (Lucene = {}));
//# sourceMappingURL=lucene.js.map